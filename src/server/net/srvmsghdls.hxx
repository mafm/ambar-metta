/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_SERVER_NET_MSGHDLS_H__
#define __AMBARMETTA_SERVER_NET_MSGHDLS_H__


/** \file srvmsghdls.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains all the server message handlers, which decode the data in
 * the message and relay the message to the corresponding manager.
 */


#include "server/srvconfig.hxx"

#include "common/net/msgbase.hxx"


NAMESPACE_START(AmbarMetta);


//---------------------------------------------------------------
// Connections
//---------------------------------------------------------------

class MsgHdlConnect : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlLogin : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlJoin : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlNewUser : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlNewChar : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlDelChar : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};


//---------------------------------------------------------------
// Console
//---------------------------------------------------------------

class MsgHdlChat : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlCommand : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};


//---------------------------------------------------------------
// Contact list
//---------------------------------------------------------------

class MsgHdlContactAdd : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlContactDel : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};


//---------------------------------------------------------------
// Entities
//---------------------------------------------------------------

class MsgHdlEntityMove : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};


//---------------------------------------------------------------
// Inventory
//---------------------------------------------------------------

class MsgHdlInventoryGet : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};

class MsgHdlInventoryDrop : public MsgHdlBase
{
public:
	virtual MsgType getMsgType() const;
	virtual void handleMsg(MsgBase& msg, Netlink* netlink);
};


NAMESPACE_END(AmbarMetta);
#endif
