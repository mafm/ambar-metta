/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvnetworkmgr.hxx"

#include "common/net/msgs.hxx"
#include "common/configmgr.hxx"

#include "server/login/srvloginmgr.hxx"
#include "server/net/srvmsghdls.hxx"
#include "server/world/srvworldmgr.hxx"

#include <cstdlib>
#include <fcntl.h>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvNetworkMgr
 ******************************************************************************/
template <> SrvNetworkMgr* Singleton<SrvNetworkMgr>::INSTANCE = 0;

SrvNetworkMgr::SrvNetworkMgr() :
	mSocketLayer(&mNetlink), mMaxPlayers(0)
{
	registerMsgHdls();

	// Start listening on the network
	std::string address = ConfigMgr::instance().getConfigVar("Server.Network.Address", "-");
	int port = atoi(ConfigMgr::instance().getConfigVar("Server.Network.Port", "-1").c_str());
	int maxPlayers = atoi(ConfigMgr::instance().getConfigVar("Server.Network.MaxPlayers", "-1").c_str());
	if (address == "-" || port == -1 || maxPlayers == -1) {
		logERROR("Unable to load network-related values from cfg file");
	} else if (!startListening(address.c_str(), port, maxPlayers)) {
		logERROR("Unable to start listening: %s:%d, maxplayers=%d",
			 address.c_str(), port, maxPlayers);
	}
}

void SrvNetworkMgr::finalize()
{
	while (!mConnList.empty()) {
		mConnList.front()->disconnect();
		delete mConnList.front();
		mConnList.pop_front();
	}

	mSocketLayer.disconnect();
}

void SrvNetworkMgr::registerMsgHdls()
{
#define	REGHDL(msg, hdl) mMsgHdlFactory.registerMsgWithHdl(new msg, new hdl);

	// connection messages
	REGHDL(MsgConnect, MsgHdlConnect);
	REGHDL(MsgLogin, MsgHdlLogin);
	REGHDL(MsgNewUser, MsgHdlNewUser);
	REGHDL(MsgNewChar, MsgHdlNewChar);
	REGHDL(MsgDelChar, MsgHdlDelChar);
	REGHDL(MsgJoin, MsgHdlJoin);

	// console
	REGHDL(MsgChat, MsgHdlChat);
	REGHDL(MsgCommand, MsgHdlCommand);

	// contact list
	REGHDL(MsgContactAdd, MsgHdlContactAdd);
	REGHDL(MsgContactDel, MsgHdlContactDel);

	// entities
	REGHDL(MsgEntityMove, MsgHdlEntityMove);

	// inventory
	REGHDL(MsgInventoryGet, MsgHdlInventoryGet);
	REGHDL(MsgInventoryDrop, MsgHdlInventoryDrop);

#undef	REGHDL
}

bool SrvNetworkMgr::isConnected()
{
	return mSocketLayer.isConnected();
}

bool SrvNetworkMgr::startListening(const char* host, int port, int maxPlayers)
{
	mMaxPlayers = maxPlayers;

	if (mSocketLayer.isConnected()) {
		logWARNING("Already listening, refusing to connect again");
		return false;
	} else {
		// the third parameter is the number of connections waiting
		// (backlog)
		bool resultGame = mSocketLayer.listenForClients(host, port, 8);
		bool resultPing = mPingServer.listenForClients(host, port-1, 8);
		if (!resultGame || !resultPing) {
			logERROR("Couldn't set up Game or Ping listeners");
		}
		return resultGame && resultPing;
	}
}

void SrvNetworkMgr::disconnectPlayer(Netlink& netlink)
{
	// mafm: do not call this if we detect the disconnection trying to
	// process the incoming data, it's only to be used when the decision
	// comes from other managers.

	for (auto it = mConnList.begin(); it != mConnList.end(); ++it) {
		if (**it == netlink) {
			logINFO("Adding player for queue to disconnect: %s:%d",
				netlink.getIP(), netlink.getPort());
			mConnListToDelete.push_front(&netlink);
			return;
		}
	}

	// not found
	logERROR("Couldn't find netlink when trying to disconnect player: %s:%d",
		 netlink.getIP(), netlink.getPort());
}

void SrvNetworkMgr::processIncomingMsgs()
{
	if (!mSocketLayer.isConnected()) {
		// we just return without printing anything because otherwise it
		// floods the client log until we get connected for the first
		// time
		return;
	}

	// attend ping requests
	mPingServer.acceptIncoming();
	mPingServer.processPingRequests();

	// delete pending connections
	while (!mConnListToDelete.empty()) {
		Netlink* netlink = *(mConnListToDelete.begin());
		mConnListToDelete.pop_front();
		for (auto it = mConnList.begin(); it != mConnList.end(); ++it) {
			if (**it == *netlink) {
				logINFO("Disconnecting player: %s:%d",
					netlink->getIP(), netlink->getPort());
				mConnList.erase(it);
				netlink->disconnect();
				delete netlink;
				break;
			}
		}
	}

	// to accept incoming connections, just 1 for loop so we autoprotect
	// from floods
	int socket = 0, port = 0; 
	std::string ip;
	bool result = mSocketLayer.acceptIncoming(socket, ip, port);
	if (result) {
		if (mConnList.size() >= mMaxPlayers) {
			logWARNING("MaxPlayers=%d reached, rejecting connection: %d (IP %s, port %d)",
				   mMaxPlayers, socket, ip.c_str(), port);
			close(socket);
		} else {
			logDEBUG("Accepting incoming connection: %d (IP: %s, port %d)",
				 socket, ip.c_str(), port);
			fcntl(socket, F_SETFL, O_NONBLOCK);
			Netlink* netlink = new Netlink(socket, ip.c_str(), port);
			mConnList.push_back(netlink);
		}
        }

	// loops through all the connections to see if they send anything new
	// (and if the connection was closed, which is done with the result of
	// the recv() system call).
	for (auto it = mConnList.begin(); it != mConnList.end(); ++it) {
		// it will try to send queued messages
		(**it).processOutgoingMsgs();

		// we read data and there may be some messages available (so we
		// should treat the messages appropriately)
		bool resultProcessing = (**it).processIncomingMsgs(mMsgHdlFactory);
		if (!resultProcessing) {
			logDEBUG("Connection closed, invoking disconnection: %d", (*it)->getSocket());
			SrvLoginMgr::instance().removeConnection(*it);
			delete *it;
			it = mConnList.erase(it);
		}
	}
}

void SrvNetworkMgr::sendToConnection(MsgBase& msg, Netlink* netlink)
{
	int result = netlink->sendMsg(msg);
	if (!result) {
		logERROR("Message '%s' for connection %d (IP='%s') too big (%u)",
			 msg.getType().getName(),
			 netlink->getSocket(),
			 netlink->getIP(),
			 msg.getLength());
		return;
	}
}

void SrvNetworkMgr::sendToPlayer(MsgBase& msg, const SrvLoginData* player)
{
	int result = player->getNetlink()->sendMsg(msg);
	if (!result) {
		logERROR("Message '%s' for player '%s' (IP='%s') too big (%u)",
			 msg.getType().getName(),
			 player->getPlayerName(),
			 player->getIP(),
			 msg.getLength());
		return;
	}
}

void SrvNetworkMgr::sendToAllConnections(MsgBase& msg)
{
	std::vector<SrvLoginData*> allConnections;
	SrvLoginMgr::instance().getAllConnections(allConnections);

	for (size_t i = 0; i < allConnections.size(); ++i) {
		sendToPlayer(msg, allConnections[i]);
	}
}

void SrvNetworkMgr::sendToAllPlayers(MsgBase& msg)
{
	std::vector<SrvLoginData*> allPlayers;
	SrvLoginMgr::instance().getAllConnectionsPlaying(allPlayers);

	for (size_t i = 0; i < allPlayers.size(); ++i) {
		sendToPlayer(msg, allPlayers[i]);
	}
}

void SrvNetworkMgr::sendToPlayerList(MsgBase& msg, std::vector<SrvLoginData*>& playerList)
{
	for (size_t i = 0; i < playerList.size(); ++i) {
		sendToPlayer(msg, playerList[i]);
	}
}

void SrvNetworkMgr::sendToAllButPlayer(MsgBase& msg, const SrvLoginData* player)
{
	std::vector<SrvLoginData*> allPlayers;
	SrvLoginMgr::instance().getAllConnectionsPlaying(allPlayers);

	for (size_t i = 0; i < allPlayers.size(); ++i) {
		if (player != allPlayers[i]) {
			sendToPlayer(msg, allPlayers[i]);
		}
	}
}


NAMESPACE_END(AmbarMetta);
