/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvmsghdls.hxx"

#include "common/net/msgs.hxx"

#include "server/console/srvcommand.hxx"
#include "server/console/srvconsolemgr.hxx"
#include "server/entity/srventityplayer.hxx"
#include "server/login/srvloginmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"
#include "server/world/srvworldcontactmgr.hxx"
#include "server/world/srvworldmgr.hxx"


NAMESPACE_START(AmbarMetta);


//---------------------------------------------------------------
// Connections
//---------------------------------------------------------------

MsgType MsgHdlConnect::getMsgType() const
{
	return MsgConnect::_type;
}

void MsgHdlConnect::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	// MsgConnect* msg = dynamic_cast<MsgConnect*>(&baseMsg);
	SrvLoginMgr::instance().addConnection(netlink);
}


MsgType MsgHdlLogin::getMsgType() const
{
	return MsgLogin::_type;
}

void MsgHdlLogin::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgLogin* msg = dynamic_cast<MsgLogin*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	SrvLoginMgr::instance().login(player,
				      msg->username,
				      msg->pw_md5sum);
}


MsgType MsgHdlJoin::getMsgType() const
{
	return MsgJoin::_type;
}

void MsgHdlJoin::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgJoin* msg = dynamic_cast<MsgJoin*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	SrvLoginMgr::instance().joinGame(player, msg->charname);
}


MsgType MsgHdlNewUser::getMsgType() const
{
	return MsgNewUser::_type;
}

void MsgHdlNewUser::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgNewUser* msg = dynamic_cast<MsgNewUser*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	SrvLoginMgr::instance().createUser(player,
					   msg->username,
					   msg->pw_md5sum,
					   msg->email,
					   msg->realname);
}


MsgType MsgHdlNewChar::getMsgType() const
{
	return MsgNewChar::_type;
}

void MsgHdlNewChar::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgNewChar* msg = dynamic_cast<MsgNewChar*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	SrvLoginMgr::instance().createCharacter(player,
						msg->charname,
						msg->race, msg->gender,
						msg->playerClass,
						msg->ab_choice_con,
						msg->ab_choice_str,
						msg->ab_choice_dex,
						msg->ab_choice_int,
						msg->ab_choice_wis,
						msg->ab_choice_cha);
}


MsgType MsgHdlDelChar::getMsgType() const
{
	return MsgDelChar::_type;
}

void MsgHdlDelChar::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgDelChar* msg = dynamic_cast<MsgDelChar*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	SrvLoginMgr::instance().deleteCharacter(player,
						msg->charname);
}


//---------------------------------------------------------------
// Console
//---------------------------------------------------------------

MsgType MsgHdlChat::getMsgType() const
{
	return MsgChat::_type;
}

void MsgHdlChat::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgChat* msg = dynamic_cast<MsgChat*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	if (!player->isPlaying()) {
		logERROR("Chat message with invalid player: '%s: %s'",
			 player->getPlayerName(),
			 msg->text.c_str());
	} else if (msg->text.empty()) {
		logWARNING("Empty chat message from '%s', ignoring",
			   player->getPlayerName());
	} else {
		SrvConsoleMgr::instance().processChat(*msg, *player);
	}
}


MsgType MsgHdlCommand::getMsgType() const
{
	return MsgCommand::_type;
}

void MsgHdlCommand::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgCommand* msg = dynamic_cast<MsgCommand*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	if (!player->isPlaying()) {
		logERROR("Command message with invalid player: '%s: %s'",
			 player->getPlayerName(),
			 msg->command.c_str());
	} else if (msg->command.empty()) {
		logWARNING("Empty command in message from '%s', ignoring",
			   player->getPlayerName());
	} else {
		SrvConsoleMgr::instance().processCommand(*msg, *player);
	}
}


//---------------------------------------------------------------
// Contact list
//---------------------------------------------------------------

MsgType MsgHdlContactAdd::getMsgType() const
{
	return MsgContactAdd::_type;
}

void MsgHdlContactAdd::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgContactAdd* msg = dynamic_cast<MsgContactAdd*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	SrvWorldContactMgr::addContact(*player,
				       msg->charname, msg->type, msg->comment);
}


MsgType MsgHdlContactDel::getMsgType() const
{
	return MsgContactDel::_type;
}

void MsgHdlContactDel::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgContactDel* msg = dynamic_cast<MsgContactDel*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	SrvWorldContactMgr::removeContact(*player, msg->charname);
}


//---------------------------------------------------------------
// Entities
//---------------------------------------------------------------

MsgType MsgHdlEntityMove::getMsgType() const
{
	return MsgEntityMove::_type;
}

void MsgHdlEntityMove::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgEntityMove* msg = dynamic_cast<MsgEntityMove*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	if (!player->isPlaying()) {
		logWARNING("Player '%s' (IP: %s) sent EntityMove message but not playing, ignoring",
			   player->getUserName(), player->getIP());
		return;
	} else {
		player->getPlayerEntity()->updateMovementFromClient(msg);
	}
}


//---------------------------------------------------------------
// Inventory
//---------------------------------------------------------------

MsgType MsgHdlInventoryGet::getMsgType() const
{
	return MsgInventoryGet::_type;
}

void MsgHdlInventoryGet::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgInventoryGet* msg = dynamic_cast<MsgInventoryGet*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	if (!player->isPlaying()) {
		logWARNING("User '%s' (IP: %s) sent EntityMove message but not playing, ignoring",
			   player->getUserName(), player->getIP());
		return;
	} else {
		SrvWorldMgr::instance().playerGetItem(player->getPlayerEntity(),
						      msg->itemID);
	}
}

MsgType MsgHdlInventoryDrop::getMsgType() const
{
	return MsgInventoryDrop::_type;
}

void MsgHdlInventoryDrop::handleMsg(MsgBase& baseMsg, Netlink* netlink)
{
	MsgInventoryDrop* msg = dynamic_cast<MsgInventoryDrop*>(&baseMsg);
	SrvLoginData* player = SrvLoginMgr::instance().findPlayer(netlink);

	// sanity check
	if (!player) {
		logWARNING("Message of type '%s' from unregistered connection '%s', disconnecting",
			   msg->getType().getName(), netlink->getIP());
		SrvNetworkMgr::instance().disconnectPlayer(*netlink);
		return;
	}

	if (!player->isPlaying()) {
		logWARNING("User '%s' (IP: %s) sent EntityMove message but not playing, ignoring",
			   player->getUserName(), player->getIP());
		return;
	} else {
		SrvWorldMgr::instance().playerDropItem(player->getPlayerEntity(),
						       msg->itemID);
	}
}


NAMESPACE_END(AmbarMetta);
