/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvdbmgr.hxx"

#include "common/configmgr.hxx"

#ifdef HAVE_POSTGRESQL
#include "server/db/srvdbconnectorpostgresql.hxx"
#endif
#if (!defined HAVE_POSTGRESQL)
#error "You must choose at least one SQL database type"
#endif

#include <cstdlib>
#include <cstring>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvDBResult
 ******************************************************************************/
void SrvDBResult::getValue(size_t row, const char* columnName, std::string& value) const
{
	if (row > getNumberOfRows()) {
		logERROR("Asked for row '%d', nresults='%d'",
		       row, getNumberOfRows());
		value = "<row out of bounds>";
		return;
	}

	// Only useful if the column was used and the value initialized
	size_t nColumns = getNumberOfColumns();
	for (size_t column = 0; column < nColumns; ++column) {
		if (std::string(columnName) == getColumnName(column)) {
			value = getValue(row, column);
			return;
		}
	}
	logERROR("Column '%s' not found, numcolumns=%u",
	       columnName, nColumns);
	value = "<column not found>";
}

void SrvDBResult::getValue(size_t row, const char* columnName, int& value) const
{
	std::string str = "<initialized>";
	getValue(row, columnName, str);
	value = atoi(str.c_str());
}

void SrvDBResult::getValue(size_t row, const char* columnName, float& value) const
{
	std::string str = "<initialized>";
	getValue(row, columnName, str);
	value = atof(str.c_str());
}


/*******************************************************************************
 * SrvDBQuery
 ******************************************************************************/
SrvDBQuery::SrvDBQuery() :
	_result(0)
{
}

SrvDBQuery::~SrvDBQuery()
{
	if (_result)
		delete _result;
}

void SrvDBQuery::setTables(const std::string& tables)
{
	_tables = tables;
}

void SrvDBQuery::setTables(const char* tables)
{
	_tables = tables;
}

void SrvDBQuery::setCondition(const std::string& cond)
{
	_cond = cond;
}

void SrvDBQuery::setCondition(const char* cond)
{
	_cond = cond;
}

void SrvDBQuery::setOrder(const std::string& order)
{
	_order = order;
}

void SrvDBQuery::setOrder(const char* order)
{
	_order = order;
}

void SrvDBQuery::addColumnWithValue(const char* name, const char* value, bool escape)
{
	std::string val;
	if (escape) {
		std::string value_escaped = "<initialized>";
		SrvDBMgr::instance().escape(value_escaped, value);
		// variable values have to be quoted, too
		val = strFmt("'%s'", value_escaped.c_str());
	} else {
		val = value;
	}

	_fieldPair.push_back(NameValuePair(std::string(name), val));
}

void SrvDBQuery::addColumnWithValue(const char* name, const std::string& value, bool escape)
{
	addColumnWithValue(name, value.c_str(), escape);
}

void SrvDBQuery::addColumnWithoutValue(const char* name)
{
	addColumnWithValue(name, "<uninitialized>", false);
}

size_t SrvDBQuery::getNumberOfColumns() const
{
	return _fieldPair.size();
}

void SrvDBQuery::getColumnName(size_t column, std::string& name) const
{
	// Only retrieve it if the column was used
	if (column < _fieldPair.size()) {
		name = _fieldPair[column].name;
	} else {
		logERROR("Asked for column %zu, numcolumns=%zu",
		       column, _fieldPair.size());
		name = "<column out of range>";
	}
}

void SrvDBQuery::getColumnValue(const char* columnName, std::string& value) const
{
	// Only useful if the column was used and the value initialized
	for (size_t column = 0; column < _fieldPair.size(); ++column) {
		if (_fieldPair[column].name == columnName) {
			value = _fieldPair[column].value;
			return;
		}
	}
	logERROR("Column '%s' not found, numcolumns=%zu",
	       columnName, _fieldPair.size());
	value = "<column not found>";
}

void SrvDBQuery::getColumnValue(size_t column, std::string& value) const
{
	// Only retrieve it if the column was used
	if (column < _fieldPair.size()) {
		value = _fieldPair[column].value;
	} else {
		logERROR("Asked for column %zu, numcolumns=%zu",
		       column, _fieldPair.size());
		value = "<column out of range>";
	}
}

const SrvDBResult* SrvDBQuery::getResult() const
{
	return _result;
}

void SrvDBQuery::setResult(SrvDBResult* result)
{
	_result = result;
}


/*******************************************************************************
 * SrvDBMgr
 ******************************************************************************/
template <> SrvDBMgr* Singleton<SrvDBMgr>::INSTANCE = 0;

SrvDBMgr::SrvDBMgr() :
	_connector(0)
{
	std::string dbtype = ConfigMgr::instance().getConfigVar("Server.Database.Type", "");
	std::string host = ConfigMgr::instance().getConfigVar("Server.Database.Host", "");
	std::string port = ConfigMgr::instance().getConfigVar("Server.Database.Port", "");
	std::string dbname = ConfigMgr::instance().getConfigVar("Server.Database.DatabaseName", "");
	std::string dbuser = ConfigMgr::instance().getConfigVar("Server.Database.Username", "");
	std::string dbpass = ConfigMgr::instance().getConfigVar("Server.Database.Password", "");
	if (dbtype.empty() || host.empty() || port.empty()
	    || dbname.empty() || dbuser.empty() || dbpass.empty()) {
		logERROR("Couldn't read necessary config values for the DB");
	}

#ifdef HAVE_POSTGRESQL
	if (dbtype == "postgresql")
		_connector = new SrvDBConnectorPostgresql();
#endif

	// do connect
	_connector->connectToDB(host.c_str(), port.c_str(),
				dbname.c_str(),
				dbuser.c_str(), dbpass.c_str());
}

SrvDBMgr::~SrvDBMgr()
{
}

void SrvDBMgr::finalize()
{
	delete _connector;
}

void SrvDBMgr::escape(std::string& out, const std::string& in) const
{
	_connector->escapeData(out, in.c_str(), in.size());
}

std::string SrvDBMgr::escape(const std::string& in) const
{
	std::string out;
	_connector->escapeData(out, in.c_str(), in.size());
	return out;
}

bool SrvDBMgr::execute(const char* cmd) const
{
	SrvDBResult* res = _connector->executeQuery(cmd);
	if (res) {
		delete res;
		return true;
	} else {
		return false;
	}
}

bool SrvDBMgr::queryInsert(const SrvDBQuery* query) const
{
	// auxiliar
	std::string colname = "<initialized>";
	std::string value = "<initialized>";

	// start to build the query
	size_t numcolumns = query->getNumberOfColumns();
	std::string qry = "INSERT INTO " + query->_tables + " (";
	for (size_t column = 0; column < numcolumns; ++column) {
		query->getColumnName(column, colname);
		if (0 != column) qry += ",";
		qry += colname;
	}
	qry += ") VALUES (";
	for (size_t column = 0; column < numcolumns; ++column) {
		query->getColumnValue(column, value);
		if (0 != column)
			qry += ",";
		qry += value;
	}
	qry += ")";

	// final processing
	SrvDBResult* res = _connector->executeQuery(qry.c_str());
	if (!res) {
		return false;
	} else {
		bool result = (res->getNumberOfAffectedRows() > 0);
		delete res;
		return result;
	}
}

int SrvDBMgr::queryUpdate(const SrvDBQuery* query) const
{
	// auxiliar
	std::string colname = "<initialized>";
	std::string value = "<initialized>";

	// start to build the query
	std::string qry = "UPDATE " + query->_tables + " SET ";
	for (size_t column = 0; column < query->getNumberOfColumns(); ++column) {
		query->getColumnName(column, colname);
		query->getColumnValue(column, value);
		if (0 != column) qry += ",";
		qry += colname + "=" + value;
	}

	// condition?
	if (query->_cond.size() > 0)
		qry += " WHERE " + query->_cond;

	// final processing
	SrvDBResult* res = _connector->executeQuery(qry.c_str());
	if (!res) {
		return -1;
	} else {
		int affected = res->getNumberOfAffectedRows();
		delete res;
		return affected;
	}
}

int SrvDBMgr::queryDelete(const SrvDBQuery* query) const
{
	// starting to build the query
	std::string qry = "DELETE FROM " + query->_tables;

	// condition ?
	if (query->_cond.size() > 0)
		qry += " WHERE " + query->_cond;

	// final processing
	SrvDBResult* res = _connector->executeQuery(qry.c_str());
	if (!res) {
		return -1;
	} else {
		int affected = res->getNumberOfAffectedRows();
		delete res;
		return affected;
	}
}

int SrvDBMgr::querySelect(SrvDBQuery* query) const
{
	// auxiliar
	std::string colname = "<initialized>";

	// starting to build the query
	std::string qry = "SELECT ";
	for (size_t column = 0; column < query->getNumberOfColumns(); ++column) {
		query->getColumnName(column, colname);
		if (0 != column) qry += ",";
		qry += colname;
	}
	qry += " FROM " + query->_tables;

	// condition?
	if (query->_cond.size() > 0)
		qry += " WHERE " + query->_cond;

	// order?
	if (query->_order.size() > 0)
		qry += " ORDER BY " + query->_order;

	// final processing
	SrvDBResult* result = _connector->executeQuery(qry.c_str());
	if (!result) {
		return -1;
	} else {
		query->setResult(result);
		return result->getNumberOfRows();
	}
}

bool SrvDBMgr::queryMatch(const SrvDBQuery* query) const
{
	int numResults = queryMatchNumber(query);
	if (numResults <= 0) {
		return false;
	} else {
		return true;
	}
}

int SrvDBMgr::queryMatchNumber(const SrvDBQuery* query) const
{
	// base
	std::string qry = "SELECT count(*) FROM " + query->_tables;

	// add condition
	if (query->_cond.size() > 0)
		qry += " WHERE " + query->_cond;

	// execute the query itself
	SrvDBResult* result = _connector->executeQuery(qry.c_str());
	if (!result) {
		return -1;
	} else {
		int count = 0;
		result->getValue(0, "count", count);
		delete result;
		return count;
	}
}


NAMESPACE_END(AmbarMetta);
