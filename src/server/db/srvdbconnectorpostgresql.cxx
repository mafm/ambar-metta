/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_POSTGRESQL

#include "srvdbconnectorpostgresql.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvDBPostgresqlResult
 ******************************************************************************/
SrvDBPostgresqlResult::SrvDBPostgresqlResult(PGresult* result)
{
	_result = result;
	_nRows = PQntuples(_result);
	_nFields = PQnfields(_result);
	_nAffected = atoi(PQcmdTuples(_result));
}

SrvDBPostgresqlResult::~SrvDBPostgresqlResult()
{
	PQclear(_result);
}

const char* SrvDBPostgresqlResult::getValue(size_t row, size_t column) const
{
	if (row > _nRows || column > _nFields) {
		logERROR("DB PostgreSQL: getValue out of range (row '%zu', column '%zu')",
		       row, column);
		return 0;
	} else {
		return PQgetvalue(_result, row, column);
	}
}

const char* SrvDBPostgresqlResult::getColumnName(size_t column) const
{
	if (column > _nFields) {
		logERROR("DB PostgreSQL: getColumnName out of range (column '%zu')",
		       column);
		return 0;
	} else {
		return PQfname(_result, column);
	}
}

size_t SrvDBPostgresqlResult::getNumberOfRows() const
{
	return _nRows;
}

size_t SrvDBPostgresqlResult::getNumberOfColumns() const
{
	return _nFields;
}

size_t SrvDBPostgresqlResult::getNumberOfAffectedRows() const
{
	return _nAffected;
}


/*******************************************************************************
 * SrvDBConnectorPostgresql
 ******************************************************************************/
SrvDBConnectorPostgresql::SrvDBConnectorPostgresql() :
	_conn(0)
{
}

SrvDBConnectorPostgresql::~SrvDBConnectorPostgresql()
{
	PQfinish(_conn);
}

bool SrvDBConnectorPostgresql::connectToDB(const char* host,
					   const char* port,
					   const char* dbname,
					   const char* dbuser,
					   const char* dbpass)
{
	std::string connstring = strFmt("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=1",
					host, port, dbname, dbuser, dbpass);

	logINFO("Connecting to PostgreSQL");
	_conn = PQconnectdb(connstring.c_str());
	if (PQstatus(_conn) == CONNECTION_BAD) {
		logERROR("DB failed: '%s'", PQerrorMessage(_conn));
		return false;
	} else {
		return true;
	}
}

SrvDBResult* SrvDBConnectorPostgresql::executeQuery(const char* sqlcmd) const
{
	PGresult* res = PQexec(_conn, sqlcmd);

	// check to see that the backend connection was successfully made
	if (! (PQresultStatus(res) == PGRES_COMMAND_OK
	       || PQresultStatus(res) == PGRES_TUPLES_OK)) {
		logERROR("DB failed: '%s'", PQresultErrorMessage(res));
		return 0;
	} else {
		return new SrvDBPostgresqlResult(res);
	}
}

void SrvDBConnectorPostgresql::escapeData(std::string& out, const char* data, size_t length) const
{
	char escData[(length*2) + 1];
	escData[0] = '\0';
	PQescapeString(escData, data, length);
	out = escData;
}


NAMESPACE_END(AmbarMetta);


#endif
