/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvconsolemgr.hxx"

#include "common/configmgr.hxx"
#include "common/net/msgs.hxx"

#include "server/console/srvcommand.hxx"
#include "server/login/srvloginmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"
#include "server/world/srvworldmgr.hxx"

#include <vector>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvConsoleMgr
 ******************************************************************************/
template <> SrvConsoleMgr* Singleton<SrvConsoleMgr>::INSTANCE = 0;

SrvConsoleMgr::SrvConsoleMgr()
{
	_radiusSay = atof(ConfigMgr::instance().getConfigVar("Server.Chat.SayRadius", "0").c_str());
	if (_radiusSay == 0.0f) {
		logERROR("Couldn't get ChatSayRadius from the config file");
	}
}

void SrvConsoleMgr::processChat(const MsgChat& msg, const SrvLoginData& player)
{
	// mafm: disabled the chat censor, some people (as me :P) don't like it
	// so maybe won't be even used, but anyway it's useless now

	logDEBUG("Console message: '<%s> %s' [to '%s']",
		 player.getPlayerName(),
		 msg.text.c_str(),
		 msg.target.c_str());

	// Find out who the message came from (don't trust the client to tell us
	// the right name)
	MsgChat repmsg;
	repmsg.origin = player.getPlayerName();
	repmsg.text = msg.text;
	if (msg.target.empty()) {
		// target empty, treat as /say command
		std::vector<SrvLoginData*> nearbyPlayers;
		SrvWorldMgr::instance().getNearbyPlayers(player,
							 _radiusSay,
							 nearbyPlayers);
		repmsg.type = MsgChat::CHAT;
		SrvNetworkMgr::instance().sendToPlayerList(repmsg,
							   nearbyPlayers);
	} else {
		// target especified, treat as private message
		SrvLoginData* target = SrvLoginMgr::instance().findPlayer(msg.target.c_str());
		repmsg.type = MsgChat::PM;
		if (target && target->isPlaying()) {
			repmsg.target = msg.target;
			SrvNetworkMgr::instance().sendToPlayer(repmsg,
							       target);
		} else {
			// can't find player, bouncing
			repmsg.target = repmsg.origin;
			repmsg.text += " [player '" + msg.target + "' not found]";
			SrvNetworkMgr::instance().sendToPlayer(repmsg, &player);
		}
	}
}

void SrvConsoleMgr::processCommand(const MsgCommand& msg, const SrvLoginData& player)
{
	logINFO("Command: '<%s %d> %s'",
		player.getPlayerName(),	player.getPermissionLevel(),
		msg.command.c_str());

	// execute
	CommandOutput out;
	SrvCommandMgr::instance().execute(msg.command.c_str(),
					  player.getPermissionLevel(),
					  out);
	// send reply message with result
	MsgChat replymsg;
	replymsg.origin = "Server";
	replymsg.type = MsgChat::SYSTEM;
	replymsg.text = out.getString();
	SrvNetworkMgr::instance().sendToPlayer(replymsg, &player);
}


NAMESPACE_END(AmbarMetta);
