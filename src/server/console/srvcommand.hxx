/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_SERVER_COMMAND_H__
#define __AMBARMETTA_SERVER_COMMAND_H__


/** \file srvcommand.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file has the implementation of commands and command manager in the
 * server.
 */


#include "server/srvconfig.hxx"

#include "common/patterns/singleton.hxx"
#include "common/command.hxx"


NAMESPACE_START(AmbarMetta);


/** Command manager class for the server.
 *
 * The registerCommands method is defined so it registers the commands specific
 * to the srv.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandMgr : public CommandMgr, public Singleton<SrvCommandMgr>
{
private:
	/** Singleton friend access */
	friend class Singleton<SrvCommandMgr>;


	/** Default constructor */
	SrvCommandMgr();
	/** Destructor */
	~SrvCommandMgr();


	/** @see CommandMgr::registerCommands() */
	virtual void registerCommands();
};


NAMESPACE_END(AmbarMetta);
#endif
