/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvcommand.hxx"

#include "common/net/msgs.hxx"
#include "common/logger.hxx"

#include "server/srvmain.hxx"
#include "server/login/srvloginmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"
#include "server/world/srvworldtimemgr.hxx"
#include "server/world/srvworldmgr.hxx"


NAMESPACE_START(AmbarMetta);


/** Quit, stop the server
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandQuit : public Command
{
public:
	SrvCommandQuit() :
		Command(PermissionLevel::Admin,
			"quit",
			"Stop the server") {
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() > 0) {
			out.appendLine("This command doesn't accept arguments, ignoring");
		}

		MsgChat msg;
		msg.origin = "Server";
		msg.text = "Server is shutting down by admin request";
		msg.type = MsgChat::SYSTEM;
		SrvNetworkMgr::instance().sendToAllConnections(msg);

		out.appendLine("Server is shutting down by admin request");
		SrvMain::instance().quit();
	}
};


/** Show some statistics and info about the game
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandShowStats : public Command
{
public:
	SrvCommandShowStats() :
		Command(PermissionLevel::Admin,
			"show_stats",
			"Show some statistics and info about the game") {
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() > 0) {
			out.appendLine("This command doesn't accept arguments, ignoring");
		}

		std::string uptime = SrvMain::instance().getUptime();
		std::string gameTime = strFmt("%u", SrvWorldTimeMgr::instance().getGameTime());
		int numAccts = SrvLoginMgr::instance().getNumberOfAccounts();
		int numChars = SrvLoginMgr::instance().getNumberOfCharacters();
		int numPlayers = SrvLoginMgr::instance().getNumberOfConnectionsPlaying();
		out.appendLine(strFmt("Server uptime: %s", uptime.c_str()));
		out.appendLine(strFmt("Game time: %s", gameTime.c_str()));
		out.appendLine(strFmt("Number of accounts: %d", numAccts));
		out.appendLine(strFmt("Number of characters: %d", numChars));
		out.appendLine(strFmt("Number of players: %d", numPlayers));
	}
};


/** Change the time in the server
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandChangeTime : public Command
{
public:
	SrvCommandChangeTime() :
		Command(PermissionLevel::Admin,
			"changetime",
			"Change time by the given minutes") {
		_argNames.push_back("+-minutes");
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() != 1) {
			out.appendLine("This command needs exactly one argument, aborting");
			return;
		}

		int minutes = atoi(args[0].c_str());
		std::string text = strFmt("Time changed by %d minutes", minutes);
		out.appendLine(text.c_str());
		SrvWorldTimeMgr::instance().changeTime(minutes);

		MsgChat msg;
		msg.origin = "Server";
		msg.type = MsgChat::SYSTEM;
		msg.text = text;
		SrvNetworkMgr::instance().sendToAllPlayers(msg);
	}
};


/** Load an area.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandLoadArea : public Command
{
public:
	SrvCommandLoadArea() :
		Command(PermissionLevel::Admin,
			"load_area",
			"Load an area") {
		_argNames.push_back("area");
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() != 1) {
			out.appendLine("This command needs exactly one argument, aborting");
			return;
		}

		if (!SrvWorldMgr::instance().loadArea(args[0])) {
			out.appendLine(strFmt("ERROR: loading area '%s' (already exists?)", args[0].c_str()));
		} else {
			out.appendLine(strFmt("Area loaded successfully: %s", args[0].c_str()));
		}
	}
};


/** Load objects.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandLoadObjects : public Command
{
public:
	SrvCommandLoadObjects() :
		Command(PermissionLevel::Admin,
			"load_objects",
			"Load objects for the given area") {
		_argNames.push_back("area");
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() != 1) {
			out.appendLine("This command needs exactly one argument, aborting");
			return;
		}

		if (!SrvWorldMgr::instance().loadObjectsFromDB(args[0])) {
			out.appendLine("ERROR: loading objects from DB (already loaded?)");
		} else {
			out.appendLine("Objects loaded successfully from DB");
		}
	}
};


/** Load creatures.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandLoadCreatures : public Command
{
public:
	SrvCommandLoadCreatures() :
		Command(PermissionLevel::Admin,
			"load_creatures",
			"Load creatures for the given area") {
		_argNames.push_back("area");
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() != 1) {
			out.appendLine("This command needs exactly one argument, aborting");
			return;
		}

		if (!SrvWorldMgr::instance().loadCreaturesFromDB(args[0])) {
			out.appendLine("ERROR: loading creatures from DB (already loaded?)");
		} else {
			out.appendLine("Creatures loaded successfully from DB");
		}
	}
};


/** Who, to list the players in the server.  When the client sends a /who
 * command it's redirected to this one too.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandWho : public Command
{
public:
	SrvCommandWho() :
		Command(PermissionLevel::Player,
			"who",
			"Show the current players") {
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() > 0) {
			out.appendLine("This command doesn't accept arguments, ignoring");
		}

		// printing 6 names per line, truncated to 12 characters
		unsigned int namesPerLine = 6;
		unsigned int namesCurLine = 0;

		// get the raw list and sort it
		std::list<std::string> nameList;
		SrvLoginMgr::instance().getPlayerNameList(nameList);
		nameList.sort();

		// header
		out.appendLine(strFmt("Total number of players: %zu", nameList.size()));

		std::string line = "  ";
		for (auto it = nameList.begin(); it != nameList.end(); ++it) {
			// max of 12 chars per name, fill if doesn't reach it
			line += (*it).substr(0, 12);
			for (int i = 12-(*it).size(); i >= 0; i--) {
				line += " ";
			}
			++namesCurLine;
			if (namesCurLine >= namesPerLine) {
				out.appendLine(strFmt("  %s", line.c_str()));
				namesCurLine = 0;
				line.clear();
			}
		}
		out.appendLine(line.c_str());
	}
};


/** Modify server's log message level.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvCommandSetLogLevel : public Command
{
public:
	SrvCommandSetLogLevel() :
		Command(PermissionLevel::Admin, "loglevel", "Modify the Log Message level"){
		_argNames.push_back("DEBUG|INFO|WARNING|ERROR|FATAL");
	}

	virtual void execute(std::vector<std::string>& args, CommandOutput& out) const {
		if (args.size() != 1) {
			out.appendLine("This command needs exactly one argument ( which must be DBG, NTC, WN or ERR ), aborting");
			return;
		}

		if (!LogMgr::instance().setLogMsgLevel(args[0].c_str())) {
			out.appendLine(strFmt("ERROR: modifying log level '%s' (wrong parameter?)", args[0].c_str()));
		} else {
			out.appendLine(strFmt("Log level %s succesfully set", args[0].c_str()));
		}
	}
};


/*******************************************************************************
 * SrvCommandMgr
 ******************************************************************************/
template <> SrvCommandMgr* Singleton<SrvCommandMgr>::INSTANCE = 0;

SrvCommandMgr::SrvCommandMgr()
{
	registerCommands();
}

SrvCommandMgr::~SrvCommandMgr()
{
}

void SrvCommandMgr::registerCommands()
{
	// mafm: The procedure is quite simple, for each command that we want to
	// support we have to create an instance and add it to the manager, the
	// base class takes care of the rest (including the deletion of the
	// instances in the destructor).  The data for each command is
	// especified in the constructor, and the argument names are a
	// convenience way to show some understandable info in the help (such as
	// "pm target ...", indicating that you have to especify the target
	// player and then whatever you want to tell her/him).

	// admin commands
	addCommand(new SrvCommandQuit());
	addCommand(new SrvCommandShowStats());
	addCommand(new SrvCommandChangeTime());
	addCommand(new SrvCommandLoadArea());
	addCommand(new SrvCommandLoadObjects());
	addCommand(new SrvCommandLoadCreatures());
	addCommand(new SrvCommandSetLogLevel());

	// player commands
	addCommand(new SrvCommandWho());
}


NAMESPACE_END(AmbarMetta);
