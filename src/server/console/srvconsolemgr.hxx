/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_SERVER_CONSOLE_MGR_H__
#define __AMBARMETTA_SERVER_CONSOLE_MGR_H__


/** \file srvconsolemgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains the manager for the server console.
 */


#include "server/srvconfig.hxx"

#include "common/patterns/singleton.hxx"


NAMESPACE_START(AmbarMetta);


class SrvLoginData;
class MsgChat;
class MsgCommand;


/** Class controling the console, deciding what to do with the incoming
 * messages.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvConsoleMgr : public Singleton<SrvConsoleMgr>
{
public:
	/** Process chat message */
	void processChat(const MsgChat& msg, const SrvLoginData& player);
	/** Process a command */
	void processCommand(const MsgCommand& msg, const SrvLoginData& player);

private:
	/** Singleton friend access */
	friend class Singleton<SrvConsoleMgr>;


	/// Radius for say command
	float _radiusSay;


	/** Default constructor */
	SrvConsoleMgr();
};


NAMESPACE_END(AmbarMetta);
#endif
