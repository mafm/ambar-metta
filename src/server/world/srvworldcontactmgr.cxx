/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvworldcontactmgr.hxx"

#include "common/net/msgs.hxx"

#include "server/db/srvdbmgr.hxx"
#include "server/login/srvloginmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvWorldContactMgr
 ******************************************************************************/
void SrvWorldContactMgr::playerStatusChange(const SrvLoginData& player,
					    bool connected)
{
	// when connected, notify player about her contacts
	if (connected) {
		notifyPlayerOfContacts(player);
	}

	// tell other people the new status of this contact
	notifyOtherPlayers(player, connected);
}

void SrvWorldContactMgr::addContact(const SrvLoginData& player,
				    std::string& otherCharname,
				    char otherTypeChar,
				    std::string& otherComment)
{
	logINFO("Player '%s' requested to add this contact: '%s', type %c, comment '%s'",
		player.getPlayerName(),
		otherCharname.c_str(), otherTypeChar, otherComment.c_str());

	// steps
	// 1- check if player already has this contact
	// 2- get the data of the contact player (cid, last login)
	// 3- insert into the database
	// 4- notify about the status of the recently added contact

	std::string charname = player.getPlayerName();
	std::string cid = player.getPlayerID();
	otherCharname = SrvDBMgr::instance().escape(otherCharname);
	otherComment = SrvDBMgr::instance().escape(otherComment);
	std::string otherType = strFmt("%c", otherTypeChar);

	// 1- check if player already has this contact
	{
		SrvDBQuery query;
		query.setTables("contact_list");
		std::string condition = "cid='" + cid +
			+ "' AND contact_charname='" + otherCharname + "'";
		query.setCondition(condition);
		bool matches = SrvDBMgr::instance().queryMatch(&query);
		if (matches) {
			logERROR("Player already has this contact (player '%s', "
				 "contact player '%s', type '%s', comment '%s')",
				 player.getPlayerName(), otherCharname.c_str(),
				 otherType.c_str(), otherComment.c_str());
			return;
		}
	}

	// 2- get the data of the contact player (cid, last login)
	std::string otherCid;
	std::string otherLastLogin;
	{
		SrvDBQuery query;
		query.setTables("usr_chars");
		std::string condition = "charname='" + otherCharname + "'";
		query.setCondition(condition);
		query.addColumnWithoutValue("cid");
		query.addColumnWithoutValue("last_login");
		int numresults = SrvDBMgr::instance().querySelect(&query);
		if (numresults != 1) {
			logERROR("Add contact failed: no such character (%s)",
				 otherCharname.c_str());
			return;
		}

		query.getResult()->getValue(0, "cid", otherCid);
		query.getResult()->getValue(0, "last_login", otherLastLogin);
	}

	// 3- insert into the database
	{
		SrvDBQuery query;
		query.setTables("contact_list");
		query.addColumnWithValue("cid", cid);
		query.addColumnWithValue("charname", charname);
		query.addColumnWithValue("contact_cid", otherCid);
		query.addColumnWithValue("contact_charname", otherCharname);
		query.addColumnWithValue("type", otherType);
		query.addColumnWithValue("comments", otherComment);
		//query.addColumnWithValue("creation_date", "CURRENT_TIMESTAMP", false);
		bool success = SrvDBMgr::instance().queryInsert(&query);
		if (success) {
			logINFO("Success adding contact (player '%s', "
				"contact player '%s', type '%s', comment '%s')",
				player.getPlayerName(), otherCharname.c_str(),
				otherType.c_str(), otherComment.c_str());
		} else {
			logERROR("Add contact failed, unknown DB error (player '%s', "
				 "contact player '%s', type '%s', comment '%s')",
				 player.getPlayerName(), otherCharname.c_str(),
				 otherType.c_str(), otherComment.c_str());
			return;
		}
	}

	// 4- notify about the status of the recently added contact
	notifyPlayerOfOneContact(player,
				 otherCharname, otherTypeChar,
				 otherComment, otherLastLogin,
				 isContactPlaying(otherCharname));
}

void SrvWorldContactMgr::removeContact(const SrvLoginData& player,
				       std::string& otherCharname)
{
	logINFO("Player '%s' requested to remove contact '%s'",
		player.getPlayerName(), otherCharname.c_str());

	// steps
	// 1- remove the contact, complain if there's an error

	otherCharname = SrvDBMgr::instance().escape(otherCharname);

	SrvDBQuery query;
	query.setTables("contact_list");
	std::string condition = "cid='" + std::string(player.getPlayerID()) +
		"' AND contact_charname='" + otherCharname + "'";
	query.setCondition(condition);
	bool success = SrvDBMgr::instance().queryDelete(&query);
	if (success) {
		logINFO("Success removing contact (player '%s', contact player '%s'",
			player.getPlayerName(), otherCharname.c_str());
	} else {
		logERROR("Error removing contact (player '%s', contact player '%s')",
			 player.getPlayerName(), otherCharname.c_str());
	}
}

bool SrvWorldContactMgr::isContactPlaying(const std::string& name)
{
	const SrvLoginData* targetPlayer = SrvLoginMgr::instance().findPlayer(name);
	return (targetPlayer && targetPlayer->isPlaying());
}

void SrvWorldContactMgr::notifyPlayerOfOneContact(const SrvLoginData& player,
						  const std::string& contactName,
						  char contactType,
						  const std::string& contactComment,
						  const std::string& contactLastLogin,
						  bool connected)
{
	MsgContactStatus statusmsg;
	statusmsg.charname = contactName;
	statusmsg.type = contactType;
	statusmsg.comment = contactComment;
	statusmsg.lastLogin = contactLastLogin;
	statusmsg.status = (connected ? 'C' : 'D');

	SrvNetworkMgr::instance().sendToPlayer(statusmsg, &player);
}

void SrvWorldContactMgr::notifyPlayerOfContacts(const SrvLoginData& player)
{
	// steps
	// 1- get the list of contacts of the new player
	// 2- see if they're on-line and send a message with the status
	// of each

	// 1- get the list of contacts of the new player
	std::string charname = player.getPlayerName();
	std::string cid = player.getPlayerID();
	SrvDBQuery query;
	query.setTables("usr_chars as ch,contact_list as cl");
	std::string condition = "cl.cid='" + cid +
		"' AND ch.cid=cl.contact_cid";
	query.setCondition(condition);
	query.setOrder("contact_charname ASC");
	query.addColumnWithoutValue("cl.contact_charname");
	query.addColumnWithoutValue("cl.type");
	query.addColumnWithoutValue("cl.comments");
	query.addColumnWithoutValue("ch.last_login");
	int numresults = SrvDBMgr::instance().querySelect(&query);
	if (numresults == 0) {
		logDEBUG("No contacts for player '%s'", charname.c_str());
		return;
	} else if (numresults < 0) {
		logERROR("Unknown error happened getting contacts for player '%s'",
			 charname.c_str());
		return;
	}

	// 2- see if they're on-line and send a message with the status
	// of each
	std::string contactName, contactType, contactComment, contactLastLogin;
	for (int row = 0; row < numresults; ++row) {
		query.getResult()->getValue(row, "cl.contact_charname", contactName);
		query.getResult()->getValue(row, "cl.type", contactType);
		query.getResult()->getValue(row, "cl.comments", contactComment);
		query.getResult()->getValue(row, "ch.last_login", contactLastLogin);

		notifyPlayerOfOneContact(player,
					 contactName, contactType[0],
					 contactComment, contactLastLogin,
					 isContactPlaying(contactName));
	}

	logINFO("Sent contact status info to '%s' (%d contacts)",
		charname.c_str(), numresults);
}

void SrvWorldContactMgr::notifyOtherPlayers(const SrvLoginData& player,
					    bool connected)
{
	// steps
	// 1- get the list players who have this one as contact
	// 2- see if they're on-line and send them message with the status of
	// this player

	// 1- get the list of player who have this one as contact
	std::string charname = player.getPlayerName();
	std::string cid = player.getPlayerID();
	SrvDBQuery query;
	query.setTables("usr_chars as ch,contact_list as cl");
	std::string condition = "cl.contact_cid='" + cid +
		"' AND ch.cid=cl.contact_cid";
	query.setCondition(condition);
	query.setOrder("cl.charname ASC");
	query.addColumnWithoutValue("cl.charname");
	query.addColumnWithoutValue("cl.type");
	query.addColumnWithoutValue("cl.comments");
	query.addColumnWithoutValue("ch.last_login");
	int numresults = SrvDBMgr::instance().querySelect(&query);
	if (numresults == 0) {
		logDEBUG("Nobody has this player ('%s') as a contact",
			 charname.c_str());
		return;
	} else if (numresults < 0) {
		logERROR("Unknown error happened trying to get people who "
			 "have this player ('%s') as contact",
			 charname.c_str());
		return;
	}

	// 2- see if they're on-line and send them message with the status of
	// this player

	// mafm: last login doesn't change, no need to loop
	std::string lastLogin;
	query.getResult()->getValue(0, "ch.last_login", lastLogin);
	std::string otherType, otherComment;
	for (int row = 0; row < numresults; ++row) {
		std::string otherCharname;
		query.getResult()->getValue(row, "cl.charname", otherCharname);
		query.getResult()->getValue(row, "cl.type", otherType);
		query.getResult()->getValue(row, "cl.comments", otherComment);

		// sending only if they're logged in
		const SrvLoginData* otherPlayer = SrvLoginMgr::instance().findPlayer(otherCharname);
		bool otherConnected = (otherPlayer && otherPlayer->isPlaying());
		if (otherConnected) {
			logDEBUG("Sending info to '%s' about contact '%s'",
				 otherCharname.c_str(), charname.c_str());

			notifyPlayerOfOneContact(*otherPlayer,
						 charname, otherType[0],
						 otherComment, lastLogin,
						 connected);
		}
	}

	logINFO("Sent contact status info to other players having '%s'"
		" (%d contacts in total)",
		charname.c_str(), numresults);
}


NAMESPACE_END(AmbarMetta);
