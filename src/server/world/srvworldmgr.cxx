/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvworldmgr.hxx"

#include "common/tablemgr.hxx"
#include "common/net/msgs.hxx"

#include "server/db/srvdbmgr.hxx"
#include "server/console/srvconsolemgr.hxx"
#include "server/entity/srventityobject.hxx"
#include "server/entity/srventitycreature.hxx"
#include "server/entity/srventityplayer.hxx"
#include "server/login/srvloginmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"
#include "server/world/srvworldcontactmgr.hxx"
#include "server/world/srvworldtimemgr.hxx"

#include <iostream>
#include <string>
#include <list>
#include <algorithm>


NAMESPACE_START(AmbarMetta);


/// Distance from player to objects to allow him/her to pick the objects up
const float PICKUP_DISTANCE = 10.0f;


/*******************************************************************************
 * SrvWorldMgr
 ******************************************************************************/
template <> SrvWorldMgr* Singleton<SrvWorldMgr>::INSTANCE = 0;

SrvWorldMgr::SrvWorldMgr()
{
}

void SrvWorldMgr::finalize()
{
	// clear players
	for (size_t i = 0; i < _players.size(); ++i) {
		removePlayer(*(_players[i]->getLoginData()));
	}
	_players.clear();

	// clear creatures
	for (size_t i = 0; i < _creatures.size(); ++i) {
		removeEntity(_creatures[i]);
	}
	_creatures.clear();

	// clear objects
	for (size_t i = 0; i < _objects.size(); ++i) {
		removeEntity(_objects[i]);
	}
	_objects.clear();

	// clear areas
	_areas.clear();
}

bool SrvWorldMgr::isAreaLoaded(const std::string& name) const
{
	for (size_t i = 0; i < _areas.size(); ++i) {
		if (_areas[i] == name) {
			return true;
		}
	}
	return false;
}

bool SrvWorldMgr::loadArea(const std::string& name)
{
	if (isAreaLoaded(name)) {
		return false;
	} else {
		// area not loaded, we can go on
		_areas.push_back(name);
		return true;
	}
}

bool SrvWorldMgr::loadObjectsFromDB(const std::string& area)
{
	// steps
	// 1- get data from the db
	// 2- fill the data in the in-game structure

	// 1- get data from the db
	MsgEntityCreate msgBasic;
	MsgEntityMove msgMove;
	std::string id, pos1, pos2, pos3, rot;
	Vector3 position;

	SrvDBQuery query;
	query.setTables("entities");
	query.setCondition("owner ISNULL AND area='" + area + "'");
	query.setOrder("id");
	query.addColumnWithoutValue("id");
	query.addColumnWithoutValue("pos1");
	query.addColumnWithoutValue("pos2");
	query.addColumnWithoutValue("pos3");
	query.addColumnWithoutValue("rot");
	query.addColumnWithoutValue("type");
	query.addColumnWithoutValue("subtype");
	int numresults = SrvDBMgr::instance().querySelect(&query);
	if (numresults == 0) {
		logWARNING("There are no unowned entities in the DB in area '%s'",
			   area.c_str());
		return true;
	} else if (numresults < 0) {
		logERROR("Error loading entities from the DB");
		return false;
	}

	// 2- fill the data in the in-game structure
	for (int row = 0; row < numresults; ++row) {
		query.getResult()->getValue(row, "id", id);
		query.getResult()->getValue(row, "pos1", pos1);
		query.getResult()->getValue(row, "pos2", pos2);
		query.getResult()->getValue(row, "pos3", pos3);
		query.getResult()->getValue(row, "rot", rot);
		query.getResult()->getValue(row, "type", msgBasic.meshType);
		query.getResult()->getValue(row, "subtype", msgBasic.meshSubtype);

		msgBasic.entityID = EntityID(id).id;
		msgMove.area = area;
		msgMove.position = Vector3(atof(pos1.c_str()), atof(pos2.c_str()), atof(pos3.c_str()));
		msgMove.rot = atof(rot.c_str());
		msgBasic.entityClass = "Object";
		msgBasic.entityName = msgBasic.meshType;

		SrvEntityObject* object = new SrvEntityObject(msgBasic, msgMove);
		float load = TableMgr::instance().getValueAsInt("objects", msgBasic.meshType, "load");
		object->setLoad(load);
		addEntity(object);
	}

	return true;
}

bool SrvWorldMgr::loadCreaturesFromDB(const std::string& area)
{
	// steps
	// 1- get data from the db
	// 2- fill the data in the in-game structure

	// 1- get data from the db
	MsgEntityCreate msgBasic;
	MsgEntityMove msgMove;
	MsgPlayerData msgPlayer;
	std::string id, pos1, pos2, pos3, rot;
	Vector3 position;

	SrvDBQuery query;
	query.setTables("creatures");
	query.setCondition("owner ISNULL AND area='" + area + "'");
	query.setOrder("id");
	query.addColumnWithoutValue("id");
	query.addColumnWithoutValue("pos1");
	query.addColumnWithoutValue("pos2");
	query.addColumnWithoutValue("pos3");
	query.addColumnWithoutValue("rot");
	query.addColumnWithoutValue("type");
	query.addColumnWithoutValue("subtype");
	int numresults = SrvDBMgr::instance().querySelect(&query);
	if (numresults == 0) {
		logWARNING("There are no creatures in the DB in area '%s'",
			   area.c_str());
		return true;
	} else if (numresults < 0) {
		logERROR("Error loading creatures from the DB");
		return false;
	}

	// 2- fill the data in the in-game structure
	for (int row = 0; row < numresults; ++row) {
		query.getResult()->getValue(row, "id", id);
		query.getResult()->getValue(row, "pos1", pos1);
		query.getResult()->getValue(row, "pos2", pos2);
		query.getResult()->getValue(row, "pos3", pos3);
		query.getResult()->getValue(row, "rot", rot);
		query.getResult()->getValue(row, "type", msgBasic.meshType);
		query.getResult()->getValue(row, "subtype", msgBasic.meshSubtype);

		msgBasic.entityID = EntityID(id).id;
		msgMove.area = area;
		msgMove.position = Vector3(atof(pos1.c_str()), atof(pos2.c_str()), atof(pos3.c_str()));
		msgMove.rot = atof(rot.c_str());
		msgBasic.entityClass = "Creature";
		msgBasic.entityName = msgBasic.meshType;

		msgPlayer.ab_con = TableMgr::instance().getValueAsInt("creatures", msgBasic.meshType, "con");
		msgPlayer.ab_str = TableMgr::instance().getValueAsInt("creatures", msgBasic.meshType, "str");
		msgPlayer.ab_dex = TableMgr::instance().getValueAsInt("creatures", msgBasic.meshType, "dex");
		msgPlayer.ab_int = TableMgr::instance().getValueAsInt("creatures", msgBasic.meshType, "int");
		msgPlayer.ab_wis = TableMgr::instance().getValueAsInt("creatures", msgBasic.meshType, "wis");
		msgPlayer.ab_cha = TableMgr::instance().getValueAsInt("creatures", msgBasic.meshType, "cha");

		std::string hd = TableMgr::instance().getValue("creatures", msgBasic.meshType, "hd");
		/// \todo mafm: implement
		//msgPlayer.health_max = RollDie::instance().roll(hd);
		msgPlayer.health_max = atof(hd.c_str());
		msgPlayer.health_cur = msgPlayer.health_max;

		SrvEntityCreature* creature = new SrvEntityCreature(msgBasic, msgMove, msgPlayer);
		addEntity(creature);
	}

	return true;
}

SrvEntityPlayer* SrvWorldMgr::findPlayer(const SrvLoginData* loginData) const
{
	// mafm: We can use a hash for the player list, so the performance
	// should be better (O(1) instead of O(n)).  Since STL doesn't seem to
	// have hash functions as standard, and maps/set are logaritmic but you
	// don't seem to be able to remove a key after inserted (which gives a
	// lot of problems and additional test after player logged off), we'll
	// use plain vectors for now.

	for (size_t i = 0; i < _players.size(); ++i) {
		if (loginData->getPlayerEntity() == _players[i]) {
			return _players[i];
		}
	}
	// not found
	return 0;
}

SrvEntityCreature* SrvWorldMgr::findCreature(EntityID entityID) const
{
	// mafm: Read the comment for players about performance.
	for (size_t i = 0; i < _creatures.size(); ++i) {
		if (entityID == _creatures[i]->getID()) {
			return _creatures[i];
		}
	}
	// not found
	return 0;
}

SrvEntityObject* SrvWorldMgr::findObject(EntityID entityID) const
{
	// mafm: Read the comment for players about performance.
	for (size_t i = 0; i < _objects.size(); ++i) {
		if (entityID == _objects[i]->getID()) {
			return _objects[i];
		}
	}
	// not found
	return 0;
}

void SrvWorldMgr::addPlayer(SrvLoginData& loginData)
{
	logINFO("Adding player '%s' to world manager",
	       loginData.getPlayerName());

	SrvEntityPlayer* player = loginData.getPlayerEntity();
	loginData.setPlaying(true);

	// mafm: just subscribing everybody, do something more elaborate in the
	// future if needed
	for (size_t i = 0; i < _players.size(); ++i) {
		// mafm: we can do a small optimization avoiding this
		// comparison, but only if we're sure that player is always
		// added after being subscribed (to do the comparison elsewhere,
		// in example in the subscription class, doesn't help)
		if (player == _players[i]) {
			logWARNING("Player '%s' added to the player list before being subscribed",
				   player->getName());
			continue;
		}
		_players[i]->attachObserver(player);
		player->attachObserver(_players[i]);
	}

	// notifying client about game time, to set up environment lights and
	// whatever it might need
	SrvWorldTimeMgr::instance().sendTimeToPlayer(loginData);

	// subscribing to entities
	for (size_t i = 0; i < _creatures.size(); ++i) {
		_creatures[i]->attachObserver(player);
	}
	for (size_t i = 0; i < _objects.size(); ++i) {
		_objects[i]->attachObserver(player);
	}

	// contact notification
	SrvWorldContactMgr::playerStatusChange(loginData, true);

	// now after subscribing, add to the list
	_players.push_back(player);

	// broadcasting message
	MsgChat msg;
	msg.origin = "Server";
	msg.type = MsgChat::SYSTEM;
	msg.text = strFmt("'%s' joining the game (in '%s')",
			  player->getName(), player->getArea());
	SrvNetworkMgr::instance().sendToAllButPlayer(msg, &loginData);
	logINFO("Player joined the game: '%s' (entity id: %s)",
		player->getName(), player->getID().idStr.c_str());
}

void SrvWorldMgr::removePlayer(SrvLoginData& loginData)
{
	logINFO("Removing player '%s' from world manager",
	       loginData.getPlayerName());

	SrvEntityPlayer* player = loginData.getPlayerEntity();
	loginData.setPlaying(false);

	// remove from the list -- unsubscription works automatically from other
	// players and creatures
	auto itFound = _players.end();
	for (auto it = _players.begin(); it != _players.end(); ++it) {
		if (*it == player) {
			itFound = it;
		}
	}
	if (itFound == _players.end()) {
		logERROR("Player '%s' not found in list while trying to remove it",
			 player->getName());
		return;
	} else {
		_players.erase(itFound);
	}

	// contact status
	SrvWorldContactMgr::playerStatusChange(loginData, false);

	// broadcasting message
	MsgChat msg;
	msg.origin = "Server";
	msg.type = MsgChat::SYSTEM;
	msg.text = strFmt("'%s' leaving the game (in '%s')",
			  player->getName(), player->getArea());
	SrvNetworkMgr::instance().sendToAllButPlayer(msg, &loginData);

	// removing entity
	delete player;
}

void SrvWorldMgr::getNearbyPlayers(const SrvLoginData& player,
				   float radius,
				   std::vector<SrvLoginData*>& nearbyPlayers) const
{
	for (size_t i = 0; i < _players.size(); ++i) {
		float distance = player.getPlayerEntity()->getDistanceToEntity(*_players[i]);
		if (distance <= radius) {
			nearbyPlayers.push_back(_players[i]->getLoginData());
		}
	}
}

void SrvWorldMgr::addEntity(SrvEntityBase* entity)
{
	logINFO("Adding entity '%s' to world manager, class '%s'",
		entity->getID().idStr.c_str(), entity->getEntityClass());

	// mafm: just subscribing everybody, do something more elaborate in the
	// future if needed
	for (size_t i = 0; i < _players.size(); ++i) {
		entity->attachObserver(_players[i]);
	}

	// add to the list
	if (SrvEntityCreature* c = dynamic_cast<SrvEntityCreature*>(entity)) {
		_creatures.push_back(c);
	} else if (SrvEntityObject* o = dynamic_cast<SrvEntityObject*>(entity)) {
		_objects.push_back(o);
	} else {
		logERROR("Class '%s' is unknown, for entity '%s'",
			 entity->getEntityClass(), entity->getID().idStr.c_str());
	}
}

void SrvWorldMgr::removeEntity(SrvEntityBase* entity)
{
	logINFO("Removing entity '%s' from world manager, class '%s'",
		entity->getID().idStr.c_str(), entity->getEntityClass());

	// subscribers: the entity already informs the subscribers in the
	// destructor

	// removing entity from our list
	if (dynamic_cast<SrvEntityCreature*>(entity)) {
		_creatures.erase(std::remove(_creatures.begin(), _creatures.end(), entity),
				 _creatures.end());
	} else if (dynamic_cast<SrvEntityObject*>(entity)) {
		_objects.erase(std::remove(_objects.begin(), _objects.end(), entity),
			       _objects.end());
	} else {
		logERROR("Class '%s' is unknown, for entity '%s'",
			 entity->getEntityClass(), entity->getID().idStr.c_str());
	}

	// delete, at last
	delete entity;
}

void SrvWorldMgr::playerGetItem(SrvEntityPlayer* player, EntityID entityID)
{
	// mafm: We destroy the structure representing the item when it's added
	// to the inventory.  We want to do this instead of having all the
	// objects loaded as entities in the engine, because in this way we save
	// a lot of memory and probably overhead in the server (when the engine
	// has to check all the entities present for whatever purposes),
	// although this depends on the implementation.  So the data objects
	// instantiated as SrvEntityObject or similar represent an object in the
	// world not owned by anybody: the "free" objects standing out there in
	// the world.

	MsgChat msg;
	msg.origin = "Server";
	msg.type = MsgChat::ACTION;

	try {
		// check if the entity exists and it's an object, and can be
		// added
		SrvEntityObject* object = findObject(entityID);
		if (!object) {
			throw "EntityID not found or not an object";
		} else if (player->getDistanceToEntity(*object) > PICKUP_DISTANCE) {
			throw "Object too far away";
		} else if (!player->addToInventory(object)) {
			throw "Cannot put the object into the inventory";
		}

		// finally, destroy the object entity/behaviour
		removeEntity(object);

		// everything ok, send notifications
		msg.text = strFmt("Getting item (name: '%s', id %s)",
				  object->getName(), object->getID().idStr.c_str());
		SrvNetworkMgr::instance().sendToPlayer(msg, player->getLoginData());

		logDEBUG("Player '%s': %s", player->getName(), msg.text.c_str());

	} catch (const char* error) {
		msg.text = strFmt("Cannot get item: '%s'", error);
		SrvNetworkMgr::instance().sendToPlayer(msg,
						       player->getLoginData());

		logDEBUG("Player '%s' attempted to get an item but failed: '%s'",
			 player->getName(), error);
	}
}

void SrvWorldMgr::playerDropItem(SrvEntityPlayer* player, EntityID entityID)
{
	// mafm: read the comment of GetItem

	MsgChat msg;
	msg.origin = "Server";
	msg.type = MsgChat::ACTION;

	try {
		// check if object belongs to the player and remove (copy
		// before)
		InventoryItem invItem;
		const InventoryItem* found = player->getInventoryItem(entityID);
		if (!found) {
			throw "Cannot find the object in the inventory";
		} else {
			invItem = *found;
			player->removeFromInventory(entityID);
		}

		// create the object into the world again
		Vector3 position;
		player->getPositionWithRelativeOffset(position, Vector3(0, 1, -1));
		if (!dropObjectFromInventory(entityID,
					     player->getArea(),
					     position)) {
			throw "Cannot create the entity";
		}

		// everything ok, send notifications
		msg.text = strFmt("'%s' dropping item (type: '%s', id '%s')",
				  player->getName(), invItem.getType(), entityID.idStr.c_str());
		SrvNetworkMgr::instance().sendToPlayer(msg, player->getLoginData());

		logDEBUG("Player '%s': %s", player->getName(), msg.text.c_str());

	} catch (const char* error) {
		msg.text = strFmt("Cannot get item: %s", error);
		SrvNetworkMgr::instance().sendToPlayer(msg, player->getLoginData());

		logDEBUG("Player '%s' attempted to drop an item but failed: %s",
			 player->getName(), error);
	}
}

bool SrvWorldMgr::changeObjectOwner(EntityID entityID, const std::string& charname)
{
	std::string id = entityID.idStr;

	SrvDBQuery query;
	query.setTables("entities");
	query.setCondition("id='" + id + "'");
	query.addColumnWithValue("owner", charname);
	bool success = SrvDBMgr::instance().queryUpdate(&query);
	if (!success) {
		logERROR("Error setting object '%s' ownership to player '%s' in the DB",
			 id.c_str(), charname.c_str());
		return false;
	}

	return true;
}

bool SrvWorldMgr::dropObjectFromInventory(EntityID entityID,
					  const std::string& area,
					  const Vector3& position)
{
	// STEPS
	// 1- get data from the db
	// 2- fill the data in the in-game structure
	// 3- mark as removed from inventory, and update area in the
	//    DB (might have changed)


	// 1- get data from the db
	MsgEntityCreate msgBasic;
	MsgEntityMove msgMove;
	std::string id = entityID.idStr;

	SrvDBQuery query;
	query.setTables("entities");
	query.setCondition("id='" + id + "'");
	query.addColumnWithoutValue("type");
	query.addColumnWithoutValue("subtype");
	int numresults = SrvDBMgr::instance().querySelect(&query);
	if (numresults != 1) {
		logERROR("Error loading entity from the DB");
		return false;
	}

	query.getResult()->getValue(0, "type", msgBasic.meshType);
	query.getResult()->getValue(0, "subtype", msgBasic.meshSubtype);

	// 2- fill the data in the in-game structure
	msgBasic.entityID = entityID.id;
	msgBasic.entityClass = "Object";
	msgBasic.entityName = msgBasic.meshType;
	msgMove.area = area;
	msgMove.position = position;
	SrvEntityObject* object = new SrvEntityObject(msgBasic, msgMove);
	float load = TableMgr::instance().getValueAsInt("objects", msgBasic.meshType, "load");
	object->setLoad(load);
	addEntity(object);

	// 3- mark as removed from inventory
	bool success = changeObjectOwner(entityID, "NULL");
	if (!success) {
		logERROR("Error marking the object as an outside object");
		return false;
	}

	return true;
}


NAMESPACE_END(AmbarMetta);
