/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvworldtimemgr.hxx"

#include "common/net/msgs.hxx"

#include "server/db/srvdbmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvWorldTimeMgr
 ******************************************************************************/
template <> SrvWorldTimeMgr* Singleton<SrvWorldTimeMgr>::INSTANCE = 0;

SrvWorldTimeMgr::SrvWorldTimeMgr() :
	_gameTime(0), _ticks(0)
{
	loadFromDB();
}

void SrvWorldTimeMgr::finalize()
{
	saveToDB();
}

void SrvWorldTimeMgr::sendTick(uint32_t ticks)
{
	_ticks += ticks;

	// every 5 seconds add 1 minute to the game time counter (minimum
	// resolution)
	if (_ticks >= 5000) {
		_ticks -= 5000;
		++_gameTime;
		saveToDB();
		sendTimeToAllPlayers();
	}
}

void SrvWorldTimeMgr::sendTimeToPlayer(const SrvLoginData& player) const
{
	MsgTimeMinute msg;
	msg.gametime = _gameTime;
	SrvNetworkMgr::instance().sendToPlayer(msg, &player);
}

void SrvWorldTimeMgr::sendTimeToAllPlayers() const
{
	MsgTimeMinute msg;
	msg.gametime = _gameTime;
	SrvNetworkMgr::instance().sendToAllPlayers(msg);
}

uint32_t SrvWorldTimeMgr::getGameTime() const
{
	return _gameTime;
}

void SrvWorldTimeMgr::changeTime(int minutes)
{
	_gameTime += minutes;
	saveToDB();
	sendTimeToAllPlayers();
}

void SrvWorldTimeMgr::loadFromDB()
{
	SrvDBQuery query;
	query.setTables("world");
	query.addColumnWithoutValue("time");
	int numresults = SrvDBMgr::instance().querySelect(&query);
	if (numresults != 1) {
		logERROR("Could not get the game time from the DB (nresults: %d)",
		       numresults);
	} else {
		int time = 0;
		query.getResult()->getValue(0, "time", time);
		_gameTime = static_cast<uint32_t>(time);
		logINFO("Time loaded: %u", _gameTime);
	}
}

void SrvWorldTimeMgr::saveToDB()
{
	std::string time = strFmt("%u", _gameTime);

	SrvDBQuery query;
	query.setTables("world");
	query.addColumnWithValue("time", time);
	bool success = SrvDBMgr::instance().queryUpdate(&query);
	if (!success) {
		logERROR("Could not save the game time to the DB");
	}
}


NAMESPACE_END(AmbarMetta);
