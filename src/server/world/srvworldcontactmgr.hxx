/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_SERVER_WORLD_CONTACT_MGR_H__
#define __AMBARMETTA_SERVER_WORLD_CONTACT_MGR_H__


/** \file srvworldtimemgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * To maintain and notify about the status of contacts.
 */


#include "server/srvconfig.hxx"


NAMESPACE_START(AmbarMetta);


class SrvLoginData;


/** Manages everything about contact lists.
 *
 * Contact lists are kind of list of "friends" in instant messaging tools.  Note
 * that the methods have to be declared as static to avoid to create an object
 * for them.
 *
 * It's called Manager even if it's not a singleton and all of its methods are
 * static (so it's more of a helper), because from a high level perspective it
 * acts in a similar way as managers.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvWorldContactMgr
{
public:
	/** This function does all needed when the player status changes (logs
	 * in, logs out):
	 *
	 * 1) Send the newly connected player the status info about contact list
	 * (who in the contact list is connected); and
	 *
	 * 2) and notify the status to other players on-line interested in this
	 * player
	 */
	static void playerStatusChange(const SrvLoginData& player,
				       bool connected);

	/** Add a contact by request of the player */
	static void addContact(const SrvLoginData& player,
			       std::string& otherCharname,
			       char otherTypeChar,
			       std::string& otherComment);

	/** Remove a contact by request of the player */
	static void removeContact(const SrvLoginData& player,
				  std::string& otherCharname);

private:

	/** Helper method to check whether a player is connected */
	static bool isContactPlaying(const std::string& name);

	/** Helper method to notify the player being connected about one
	 * specific contact */
	static void notifyPlayerOfOneContact(const SrvLoginData& player,
					     const std::string& contactName,
					     char contactType,
					     const std::string& contactComment,
					     const std::string& contactLastLogin,
					     bool connected);

	/** Helper method to notify the player being connected about his/her
	 * contacts */
	static void notifyPlayerOfContacts(const SrvLoginData& player);

	/** Helper method to notify the other players about the player being
	 * connected */
	static void notifyOtherPlayers(const SrvLoginData& player,
				       bool connected);
};


NAMESPACE_END(AmbarMetta);
#endif
