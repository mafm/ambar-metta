/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_SERVER_WORLD_MGR_H__
#define __AMBARMETTA_SERVER_WORLD_MGR_H__


/** \file srvworldmgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains the world manager for the server.
 */


#include "server/srvconfig.hxx"

#include "common/datatypes.hxx"
#include "common/entities.hxx"
#include "common/patterns/singleton.hxx"

#include <vector>


NAMESPACE_START(AmbarMetta);


class SrvLoginData;
class SrvNetworkMgr;
class SrvEntityBase;
class SrvEntityObject;
class SrvEntityCreature;
class SrvEntityPlayer;


/** Class governing the world events.
 *
 * This class governs the world and manages the things composing it (entities
 * which make part of the world), and their relationships (subscribing players
 * to entities), and so on.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvWorldMgr : public Singleton<SrvWorldMgr>
{
public:
	/** Finalize, do whatever cleanup needed when the server shuts down. */
	void finalize();

	/** Returns whether the area is loaded or not */
	bool isAreaLoaded(const std::string& name) const;
	/** Load a new area, returns whether the operation is succesful or
	 * not. */
	bool loadArea(const std::string& name);

	/** Load objects from the DB for the given area.
	 *
	 * \return whether the operation is succesful or not. */
	bool loadObjectsFromDB(const std::string& area);

	/** Load creatures from the DB for the given area.
	 *
	 * \return whether the operation is succesful or not. */
	bool loadCreaturesFromDB(const std::string& area);

	/** Add a new player to the world */
	void addPlayer(SrvLoginData& loginData);
	/** Remove a player */
	void removePlayer(SrvLoginData& loginData);
	/** Returns the list of players within a certain radius from a given
	 * player (useful in example to distribute chat messages) */
	void getNearbyPlayers(const SrvLoginData& player,
			      float radius,
			      std::vector<SrvLoginData*>& nearbyPlayers) const;

	/** Player wants to get an entity from the world to the inventory */
	void playerGetItem(SrvEntityPlayer* player, EntityID entityID);
	/** Player wants to drop an entity from the inventory */
	void playerDropItem(SrvEntityPlayer* player, EntityID entityID);
	/** Change the owner of the object (useful for trading, in example) */
	static bool changeObjectOwner(EntityID entityID, const std::string& charname);

private:
	/** Singleton friend access */
	friend class Singleton<SrvWorldMgr>;


	/// List of areas
	std::vector<std::string> _areas;
	/// List of players connected
	std::vector<SrvEntityPlayer*> _players;
	/// List of creatures
	std::vector<SrvEntityCreature*> _creatures;
	/// List of objects
	std::vector<SrvEntityObject*> _objects;


	/** Default constructor */
	SrvWorldMgr();

	/** Find player by login data
	 *
	 * \returns 0 if not found
	 */
	SrvEntityPlayer* findPlayer(const SrvLoginData* loginData) const;
	/** Find a creature by id
	 *
	 * \returns 0 if not found
	 */
	SrvEntityCreature* findCreature(EntityID entityID) const;
	/** Find an object
	 *
	 * \returns 0 if not found
	 */
	SrvEntityObject* findObject(EntityID entityID) const;

	/** Add an entity to the game (other than players). */
	void addEntity(SrvEntityBase* entity);
	/** Remove an entity */
	void removeEntity(SrvEntityBase* entity);

	/** Drop an object from an inventory
	 *
	 * \returns whether the operation was successful
	 */
	bool dropObjectFromInventory(EntityID entityID,
				     const std::string& area,
				     const Vector3& position);
};


NAMESPACE_END(AmbarMetta);
#endif
