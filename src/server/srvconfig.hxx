/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_SERVER_CONFIG_H__
#define __AMBARMETTA_SERVER_CONFIG_H__


/** \file srvconfig.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains valuable information and includes for all the rest of
 * server classes.
 */

#include "common/logger.hxx"

#include "common/util.hxx"

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/// Path to the config file
#define SERVER_CONFIG_FILE	"data/server/server.cfg"

/// Directory where the server stores the content
#define SERVER_CONTENT_DIR	"data/server/content"


NAMESPACE_END(AmbarMetta);
#endif
