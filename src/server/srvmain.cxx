/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvmain.hxx"

#include "common/configmgr.hxx"

#include "server/console/srvconsolemgr.hxx"
#include "server/console/srvcommand.hxx"
#include "server/db/srvdbmgr.hxx"
#include "server/login/srvloginmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"
#include "server/world/srvworldmgr.hxx"
#include "server/world/srvworldtimemgr.hxx"

#include <iostream>
#include <istream>
#include <fstream>
#include <ctime>
#include <cerrno>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <sys/time.h>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvMain
 ******************************************************************************/
template <> SrvMain* Singleton<SrvMain>::INSTANCE = 0;

SrvMain::SrvMain()
{
	_interactiveMode = true;
	_startTime = time(0);

	// seed the rng -- mafm: with seconds is not very trustable, since
	// players can guess it by the uptime reported when connecting
	struct timeval now;
	gettimeofday(&now, 0);
	srand(now.tv_usec);
}

void SrvMain::start()
{
	printf(BANNER_STRING);
	printf("\n");
	logINFO("Ambar-metta server starting...");

	// load the config file
	bool configLoaded = ConfigMgr::instance().loadConfigFile(SERVER_CONFIG_FILE);
	if (!configLoaded) {
		logFATAL("Could not load config file: '%s'", SERVER_CONFIG_FILE);
		exit(EXIT_FAILURE);
	}

	// execution mode
	std::string execMode = ConfigMgr::instance().getConfigVar("Server.Runtime.ExecutionMode", "-");
	if (execMode == "interactive") {
		_interactiveMode = true;
		logINFO("Using Interactive mode");
	} else if (execMode == "daemon") {
		_interactiveMode = false;
		logINFO("Using Daemon mode");
	} else {
		logFATAL("Could not get ExecutionMode from config file");
		exit(EXIT_FAILURE);
	}

	// Run startup commands
	std::string scriptFile = ConfigMgr::instance().getConfigVar("Server.Runtime.StartUpScript", "-");
	if (scriptFile == "-") {
		logFATAL("Could not get StartUp Script file name from config file");
		exit(EXIT_FAILURE);
	} else {
		loadStartupScript(scriptFile.c_str());
	}

	// run loop until the end
	mainLoop();
}

void SrvMain::quit()
{
	// mafm: we perform here the proper destruction related with the game,
	// so managers can save the needed data and so on

	logINFO("Ambar-metta server stopping...");

	SrvWorldMgr::instance().finalize();
	SrvLoginMgr::instance().finalize();
	SrvNetworkMgr::instance().finalize();
	SrvWorldTimeMgr::instance().finalize();
	SrvDBMgr::instance().finalize();

	logINFO("Ambar-metta server shut down.");
	exit(EXIT_SUCCESS);
}

void SrvMain::mainLoop()
{
	const int sleep_10ns = 10*1000*1000;
	static struct timespec interval = { 0, sleep_10ns };

	// infinite loop, the app will exit by another means
	while (true) {
		// be friendly to computer, sleeping for a while
		struct timespec remaining = { 0, 0 };
		nanosleep(&interval, &remaining);
		struct timespec elapsed = {
			interval.tv_sec - remaining.tv_sec,
			interval.tv_nsec - remaining.tv_nsec
		};

		// send a tick to the time manager (milliseconds)
		SrvWorldTimeMgr::instance().sendTick(elapsed.tv_nsec/(1000*1000));

		// if in interactive mode, try to read input
		if (_interactiveMode) {
			interactiveModeReadInput();
		}

		// process incoming messages from the network
		SrvNetworkMgr::instance().processIncomingMsgs();
	}
}

bool SrvMain::loadStartupScript(const std::string& file)
{
	logINFO("Executing startup script: '%s'", file.c_str());
	std::string line;
	std::ifstream script(file.c_str());
	if (script.is_open()) {
		while (!script.eof()) {
			getline(script, line);
			strTrim(line);
			if (line.empty()) {
				continue;
			}
			logINFO("> %s", line.c_str());
			executeCommand(line.c_str());
		}
		logINFO("Startup script completed: '%s'", file.c_str());
		script.close();
		return true;
	} else {
		logERROR("Unable to open script file: '%s'", strerror(errno));
		return false;
	}
}

void SrvMain::interactiveModeReadInput() const
{
	// vars to store a command-line-in-progress, and the character to be
	// read
	std::string cmd;
	char c;

	// prepare values for select call (fd=1 is stdin)
	int fd = 1;
	fd_set fdset;
	FD_ZERO(&fdset);
	FD_SET(0, &fdset);
	struct timeval tspec = {0, 0};

	switch (select(fd, &fdset, 0, 0, &tspec))
	{
	case 1:
		c = getchar();
		while ((c != '\n') && (c != EOF)) {
			cmd.append(1, c);
			c = getchar();
		}

		if (c == '\n') {
			if (cmd.length() > 0) {
				logINFO("Command: %s", cmd.c_str());
				executeCommand(cmd);
			}
			cmd.clear();
		}
		break;
	default:
		tspec.tv_sec = 0;
		tspec.tv_usec = 0;
		break;
	}
}

void SrvMain::executeCommand(const std::string& cmd) const
{
	if (cmd.empty() || cmd[0] == '\n') {
		logWARNING("Empty command, try 'help'");
	} else {
		CommandOutput out;
		SrvCommandMgr::instance().execute(cmd.c_str(), Command::PermissionLevel::Admin, out);
		logINFO("Command output: %s", out.getString().c_str());
	}
}

std::string SrvMain::getUptime() const
{
	time_t current_time = time(0);
	double numeric_uptime_double = difftime(current_time, _startTime);
	int numeric_uptime = static_cast<int>(numeric_uptime_double);
	int days = numeric_uptime / (24*60*60);
	int days_rest = numeric_uptime % (24*60*60);
	int hours = days_rest / (60*60);
	int hours_rest = days_rest % (60*60);
	int minutes = hours_rest / (60);
	int seconds = hours_rest % 60;

	return strFmt("%dd %dh%dm%ds", days, hours, minutes, seconds);
}


NAMESPACE_END(AmbarMetta);


/*******************************************************************************
 * Main functions
 ******************************************************************************/

using namespace AmbarMetta;

void SignalHandler(int sig)
{
	logINFO("Handling signal %d.", sig);
	SrvMain::instance().quit();
}

int main(int argc, char* argv[])
{
	// check arguments
	if (argc != 1) {
		logFATAL("Program doesn't accept arguments, aborting");
		exit(EXIT_FAILURE);
	}

	// install the signal handler
	if ((signal(SIGHUP, SignalHandler) == SIG_ERR)
	    || (signal(SIGINT,  SignalHandler) == SIG_ERR)
	    || (signal(SIGTERM, SignalHandler) == SIG_ERR)) {
		logERROR("%s: Could not install signal handler.", argv[0]);
	}

	// now init the application
	SrvMain::instance().start();

	return 0;
}
