/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_SERVER_MAIN_H__
#define __AMBARMETTA_SERVER_MAIN_H__


/** \file srvmain.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file is the main entrance of the server program, instantiating the main
 * application and so on.
 */


#include "server/srvconfig.hxx"

#include "common/patterns/singleton.hxx"

#include <ctime>


NAMESPACE_START(AmbarMetta);


/** This is the main program.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvMain : public Singleton<SrvMain>
{
public:
	/** Start the application */
	void start();

	/** Quit the application */
	void quit();

	/** Get the uptime of the server */
	std::string getUptime() const;

private:
	/** Singleton friend access */
	friend class Singleton<SrvMain>;


	/// Helper var to calculate the uptime
	time_t _startTime;

	/// True if running in interactive mode (reading input from term)
	bool _interactiveMode;


	/** Default constructor */
	SrvMain();

	/** Loop of tasks to perform periodically */
	void mainLoop();

	/** Execute a command */
	void executeCommand(const std::string& cmd) const;

	/** Run a simple startup script, with commands to load the map and
	 * similar functions */
	bool loadStartupScript(const std::string& file);

	/** Method to read the input from the terminal */
	void interactiveModeReadInput() const;
};


NAMESPACE_END(AmbarMetta);
#endif
