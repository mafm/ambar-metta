/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srventitycreature.hxx"

#include "server/db/srvdbmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvEntityCreature
 ******************************************************************************/
SrvEntityCreature::SrvEntityCreature(const MsgEntityCreate& basic,
				     const MsgEntityMove& mov,
				     const MsgPlayerData& data) :
	SrvEntityBase(basic, mov),
	_msgPlayerData(data)
{
	/** \todo mafm: maybe use this info at some point, not now

	mPlayerInfo = new PlayerInfo(basic.meshSubtype,
				     (PlayerInfo::PLAYER_RACE)0,
				     "fighter",
				     (PlayerInfo::PLAYER_ALIGNMENT)4,
				     1);

	// Set attributes
	mPlayerInfo->setProperty("strength", mPlayerData.ab_str);
	mPlayerInfo->setProperty("constitution", mPlayerData.ab_con);
	mPlayerInfo->setProperty("dexterity", mPlayerData.ab_dex);
	mPlayerInfo->setProperty("wisdom", mPlayerData.ab_wis);
	mPlayerInfo->setProperty("intelligence", mPlayerData.ab_int);
	mPlayerInfo->setProperty("charisma", mPlayerData.ab_cha);

	// Set health
	mPlayerInfo->setHealth( mPlayerData.health_cur );

	*/
}

SrvEntityCreature::~SrvEntityCreature()
{
	saveToDB();
}

void SrvEntityCreature::saveToDB()
{
	// mafm: sometimes works bad with the "exact" data, +=0.5 for pos3=z
	// (height)
	std::string area = _msgMov.area;
	std::string pos1 = strFmt("%.1f", _msgMov.position.x);
	std::string pos2 = strFmt("%.1f", _msgMov.position.y);
	std::string pos3 = strFmt("%.1f", _msgMov.position.z + 0.5f);
	std::string rot = strFmt("%.3f", _msgMov.rot);
	std::string id = getID().idStr;

	SrvDBQuery query;
	query.setTables("creatures");
	query.setCondition("id='" + id + "'");
	query.addColumnWithValue("area", area);
	query.addColumnWithValue("pos1", pos1);
	query.addColumnWithValue("pos2", pos2);
	query.addColumnWithValue("pos3", pos3);
	query.addColumnWithValue("rot", rot);
	bool success = SrvDBMgr::instance().queryUpdate(&query);
	if (!success) {
		logERROR("Error while saving position for creature '%s'", getName());
		return;
	}
}


NAMESPACE_END(AmbarMetta);
