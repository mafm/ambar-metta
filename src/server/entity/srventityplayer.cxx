/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srventityplayer.hxx"

#include "common/tablemgr.hxx"

#include "server/db/srvdbmgr.hxx"
#include "server/entity/srventityobject.hxx"
#include "server/login/srvloginmgr.hxx"
#include "server/world/srvworldmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvPlayerInventory
 ******************************************************************************/
SrvPlayerInventory::SrvPlayerInventory() :
	_loadMax(0.0f), _loadCurrent(0.0f)
{
}

bool SrvPlayerInventory::addItem(const InventoryItem& item)
{
	logDEBUG("Load - cur: '%.1f' item: '%.1f' max: '%.1f'",
		 _loadCurrent, item.getLoad(), _loadMax);

	if (_loadCurrent + item.getLoad() > _loadMax) {
		return false;
	} else {
		_loadCurrent += item.getLoad();
		_inventory.push_back(item);
		return true;
	}
}

bool SrvPlayerInventory::removeItem(EntityID itemID)
{
	for (auto it = _inventory.begin(); it != _inventory.end(); ++it) {
		if (itemID == it->getItemID()) {
			logDEBUG("Removed item from inventory: %llu '%s' '%s' %g",
				 it->getItemID(), it->getType(),
				 it->getSubtype(), it->getLoad());
			_loadCurrent -= it->getLoad();
			_inventory.erase(it);
			return true;
		}
	}
	return false;
}

const InventoryItem* SrvPlayerInventory::getItem(EntityID itemID) const
{
	for (size_t i = 0; i < _inventory.size(); ++i) {
		const InventoryItem* item = &(_inventory[i]);
		if (itemID == item->getItemID()) {
			logDEBUG("Found item: %llu '%s' '%s' %g",
				 item->getItemID(), item->getType(),
				 item->getSubtype(), item->getLoad());
			return item;
		}
	}
	logDEBUG("Cannot get item from inventory: not found");
	return 0;
}

const InventoryItem* SrvPlayerInventory::getItemByIndex(size_t index) const
{
	if (index >= _inventory.size()) {
		logDEBUG("Cannot get item from inventory: index out of bounds");
		return 0;
	} else {
		return &(_inventory[index]);
	}
}

size_t SrvPlayerInventory::getCount()
{
	return _inventory.size();
}

void SrvPlayerInventory::setMaxLoad(float load)
{
	_loadMax = load;
}

float SrvPlayerInventory::getCurrentLoad() const
{
	return _loadCurrent;
}


/*******************************************************************************
 * SrvEntityPlayer
 ******************************************************************************/
SrvEntityPlayer::SrvEntityPlayer(const MsgEntityCreate& basic,
				 const MsgEntityMove& mov,
				 const MsgPlayerData& data,
				 SrvLoginData* loginData) :
	SrvEntityBase(basic, mov),
	Observer(basic.entityName),
	_msgPlayerData(data),
	_loginData(loginData)
{
	_loginData->setPlayerEntity(this);
	_inventory.setMaxLoad(_msgPlayerData.load_max);

	// self -- needs to send it before the reply message
	MsgEntityCreate msgself(_msgBasic);
	msgself.entityClass = "MainPlayer";
	SrvNetworkMgr::instance().sendToPlayer(msgself,  _loginData);

	// join reply message
	MsgJoinReply joinreply;
	joinreply.resultCode = 0;
	SrvNetworkMgr::instance().sendToPlayer(joinreply, _loginData);

	// player data message
	MsgPlayerData msgPD(_msgPlayerData);
	SrvNetworkMgr::instance().sendToPlayer(msgPD, _loginData);

	// inventory
	{
		std::string itemID, itemType, itemSubtype;
		SrvDBQuery query;
		query.setTables("entities");
		std::string condition = "owner='" + _msgBasic.entityName + "'";
		query.setCondition(condition);
		query.addColumnWithoutValue("id");
		query.addColumnWithoutValue("type");
		query.addColumnWithoutValue("subtype");
		int numresults = SrvDBMgr::instance().querySelect(&query);
		for (int row = 0; row < numresults; ++row) {
			query.getResult()->getValue(row, "id", itemID);
			query.getResult()->getValue(row, "type", itemType);
			query.getResult()->getValue(row, "subtype", itemSubtype);

			float load = TableMgr::instance().getValueAsInt("objects", itemType, "load");
			InventoryItem item(EntityID(itemID).id, itemType, itemSubtype, load);
			_inventory.addItem(item);
		}
	}
	MsgInventoryListing invmsg;
	for (size_t i = 0; i < _inventory.getCount(); ++i) {
		const InventoryItem* item = _inventory.getItemByIndex(i);
		invmsg.addItem(item);
	}
	SrvNetworkMgr::instance().sendToPlayer(invmsg, _loginData);

	/// \todo: duffolonious - load player info from db.
	std::string race, gender, playerClass, level, alignment;
	{
		SrvDBQuery query;
		query.setTables("usr_chars");
		std::string condition = "charname='" + _msgBasic.entityName + "'";
		query.setCondition(condition);
		query.addColumnWithoutValue("race");
		query.addColumnWithoutValue("gender");
		query.addColumnWithoutValue("class");
		//query.addColumnWithoutValue("alignment");
		int numresults = SrvDBMgr::instance().querySelect(&query);
		if (numresults != 1) {
			logERROR("SrvEntityPlayer: more than one character selected.");
		}

		int row = 0;

		race = "<none>";
		gender = "?";
		playerClass = "<none>";
		level = "1";
		alignment = "1";
		query.getResult()->getValue(row, "race", race);
		query.getResult()->getValue(row, "gender", gender);
		query.getResult()->getValue(row, "class", playerClass);
		//query.getResult()->getValue(row, "alignment", alignment);

		/** \todo mafm: implement

		_playerInfo = new PlayerInfo( gender,
						 (PlayerInfo::PLAYER_RACE)atoi( race.c_str() ), //assuming integer
						 playerClass,
						 (PlayerInfo::PLAYER_ALIGNMENT)atoi( alignment.c_str() ),
						 _msgPlayerData.level );

		// Set attributes
		_playerInfo->setProperty( "strength", _msgPlayerData.ab_str );
		_playerInfo->setProperty( "constitution", _msgPlayerData.ab_con );
		_playerInfo->setProperty( "dexterity", _msgPlayerData.ab_dex );
		_playerInfo->setProperty( "wisdom", _msgPlayerData.ab_wis );
		_playerInfo->setProperty( "intelligence", _msgPlayerData.ab_int );
		_playerInfo->setProperty( "charisma", _msgPlayerData.ab_cha );

		// Set health
		_playerInfo->setHealth( _msgPlayerData.health_cur );
		*/
	}
}

SrvEntityPlayer::~SrvEntityPlayer()
{
	saveToDB();
//	delete _playerInfo; _playerInfo = 0;
}

void SrvEntityPlayer::saveToDB()
{
	// save position
	{
		// mafm: sometimes works bad with the "exact" data, +=0.5 for
		// pos3=z (height)
		std::string id = EntityID(_msgBasic.entityID).idStr;
		std::string area = _msgMov.area;
		std::string pos1 = strFmt("%.1f", _msgMov.position.x);
		std::string pos2 = strFmt("%.1f", _msgMov.position.y);
		std::string pos3 = strFmt("%.1f", _msgMov.position.z + 0.5f);
		std::string rot = strFmt("%.3f", _msgMov.rot);

		SrvDBQuery query;
		query.setTables("usr_chars");
		std::string condition = "cid='" + id + "'";
		query.setCondition(condition);
		query.addColumnWithValue("area", area);
		query.addColumnWithValue("pos1", pos1);
		query.addColumnWithValue("pos2", pos2);
		query.addColumnWithValue("pos3", pos3);
		query.addColumnWithValue("rot", rot);
		bool success = SrvDBMgr::instance().queryUpdate(&query);
		if (!success) {
			logERROR("Error while saving position for player '%s'",
				 getName());
			return;
		}

		logDEBUG("Saving position for player '%s' in DB: '%s' (%s, %s, %s) rot=%s",
			 getName(), _msgMov.area.c_str(),
			 pos1.c_str(), pos2.c_str(), pos3.c_str(), rot.c_str());
	}

	// player stats
	{
		std::string health = strFmt("%d", _msgPlayerData.health_cur);
		std::string magic = strFmt("%d", _msgPlayerData.magic_cur);
		std::string stamina = strFmt("%d", _msgPlayerData.stamina);
		std::string gold = strFmt("%d", _msgPlayerData.gold);

		SrvDBQuery query;
		query.setTables("player_stats");
		query.setCondition("charname='" + _msgBasic.entityName + "'");
		query.addColumnWithValue("health", health);
		query.addColumnWithValue("magic", magic);
		query.addColumnWithValue("stamina", stamina);
		query.addColumnWithValue("gold", gold);
		bool success = SrvDBMgr::instance().queryUpdate(&query);
		if (!success) {
			logERROR("Error while saving stats for character '%s'",
				 getName());
			return;
		}

		logDEBUG("Saving data for player '%s' in DB: H=%s M=%s S=%s G=%s",
			 getName(),
			 health.c_str(), magic.c_str(), stamina.c_str(), gold.c_str());
	}
}

void SrvEntityPlayer::updateMovementFromClient(MsgEntityMove* msg)
{
	/** \todo mafm: try to implement a more robust system in combination
	 * with the client, in example using the updates when not moving to
	 * check that there were no changes, and not distribute that message to
	 * the other players.
	 *
	 * With the help of this scheme may be possible to do further checks,
	 * even if not very accurate, to check that players aren't cheating
	 * much
	 */

	// the client (in the current implementation) is not aware of its ID,
	// and we can't trust it anyway, we don't trust in the area either
	msg->entityID = _msgBasic.entityID;
	msg->area = _msgBasic.area;

	// "adopt" the message
	_msgMov = *msg;

	// notify the observers
	SrvEntityBaseObserverEvent event(SrvEntityBaseObserverEvent::ENTITY_CREATE, *msg);
	notifyObservers(event);

	logDEBUG("Updating position: '%s' id=%llu, pos (%.1f, %.1f, %.1f), rot=%.1f, "
		 "run=%d w.fw=%d w.bw=%d rot.l=%d rot.r=%d",
		 getName(), _msgMov.entityID,
		 _msgMov.position.x, _msgMov.position.y, _msgMov.position.z, _msgMov.rot,
		 _msgMov.run, _msgMov.mov_fwd, _msgMov.mov_bwd, _msgMov.rot_left, _msgMov.rot_right);

	// save to DB, so in the case of crash we have recent "snapshots"
	saveToDB();
}

void SrvEntityPlayer::sendMovementToClient()
{
	SrvNetworkMgr::instance().sendToPlayer(_msgMov, _loginData);
}

SrvLoginData* SrvEntityPlayer::getLoginData() const
{
	return _loginData;
}

PlayerInfo* SrvEntityPlayer::getPlayerInfo() const
{
//	return _playerInfo;
	return 0;
}

const InventoryItem* SrvEntityPlayer::getInventoryItem(EntityID itemID) const
{
	return _inventory.getItem(itemID);
}

bool SrvEntityPlayer::addToInventory(const SrvEntityObject* object)
{
	// add the inventory in the runtime structure and the DB, update load
	InventoryItem item(object->getID().id,
			   object->getType(),
			   object->getSubtype(),
			   object->getLoad());
	return addToInventory(&item);
}

bool SrvEntityPlayer::addToInventory(const InventoryItem* item)
{
	// add the inventory in the runtime structure and the DB, update load
	bool success = _inventory.addItem(*item);
	bool savedToDB = SrvWorldMgr::instance().changeObjectOwner(item->getItemID(), _msgBasic.entityName);
	if (!success) {
		logINFO("Player '%s' can't add item to inventory because of load: %g+%g > %g",
		       getName(),
		       static_cast<float>(_msgPlayerData.load_cur),
		       item->getLoad(),
		       static_cast<float>(_msgPlayerData.load_max));
		return false;
	} else if (!savedToDB) {
		logINFO("Player '%s' can't add item to inventory, can't save object status to DB",
		       getName());
		_inventory.removeItem(item->getItemID());
		return false;
	} else {
		// send a message with updated data (load changed)
		_msgPlayerData.load_cur = static_cast<uint16_t>(_inventory.getCurrentLoad());
		MsgPlayerData msgPD(_msgPlayerData);
		SrvNetworkMgr::instance().sendToPlayer(msgPD, _loginData);

		// item added to inventory
		MsgInventoryAdd msgAdded;
		msgAdded.item = InventoryItem(item->getItemID(),
					      item->getType(),
					      item->getSubtype(),
					      item->getLoad());
		SrvNetworkMgr::instance().sendToPlayer(msgAdded, _loginData);

		return true;
	}
}

void SrvEntityPlayer::removeFromInventory(EntityID itemID)
{
	// remove the item from the inventory
	_inventory.removeItem(itemID);
	MsgInventoryDel msgRemoved;
	msgRemoved.itemID = itemID.id;
	SrvNetworkMgr::instance().sendToPlayer(msgRemoved, _loginData);

	// send a message with updated data (load changed)
	_msgPlayerData.load_cur = static_cast<uint16_t>(_inventory.getCurrentLoad());
	MsgPlayerData msg(_msgPlayerData);
	SrvNetworkMgr::instance().sendToPlayer(msg, _loginData);
}

int32_t SrvEntityPlayer::getGold() const
{
	return _msgPlayerData.gold;
}

bool SrvEntityPlayer::addGold(int amount)
{
	if (amount <= 0) {
		logWARNING("Refusing to add %d gold to player '%s'",
			   amount, getName());
		return false;
	} else {
		_msgPlayerData.gold += amount;
		logINFO("Added %d gold to player '%s', total now: %d",
		       amount, getName(), _msgPlayerData.gold);
		return true;
	}
}

bool SrvEntityPlayer::subtractGold(int amount)
{
	if (amount <= 0) {
		logWARNING("Refusing to substract %d gold from player '%s'",
			   amount, getName());
		return false;
	} else if ((_msgPlayerData.gold - amount) < 0) {
		logWARNING("Cannot subtract %d gold from player '%s' (has %d in total)",
			   amount, getName(), _msgPlayerData.gold);
		return false;
	} else {
		_msgPlayerData.gold -= amount;
		logINFO("Subtracted %d gold to player '%s', total now: %d",
		       amount, getName(), _msgPlayerData.gold);
		return true;
	}
}

void SrvEntityPlayer::updateFromObservable(const ObserverEvent& event)
{
	try {
		// SrvEntityBase events
		{
			const SrvEntityBaseObserverEvent* e =
				dynamic_cast<const SrvEntityBaseObserverEvent*>(&event);
			if (e) {
				switch (e->_actionId) {
				case SrvEntityBaseObserverEvent::ENTITY_CREATE:
				case SrvEntityBaseObserverEvent::ENTITY_DESTROY:
					SrvNetworkMgr::instance().sendToPlayer(e->_msg,
									       _loginData);
					break;
				default:
					throw "Action not understood by Observer";
				}
				return;
			}
		}

		// event not processed before
		throw "Event type not expected by Observer";
	} catch (const char* error) {
		logWARNING("SrvEntityPlayer: '%s' event: %s", event._className.c_str(), error);
	}
}


NAMESPACE_END(AmbarMetta);
