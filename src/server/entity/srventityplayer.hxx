/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_ENTITY_PLAYER_H__
#define __AMBARMETTA_ENTITY_PLAYER_H__


/** \file srventityplayer.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Represents a player in the server.
 */


#include "srventitybase.hxx"

#include "common/net/msgs.hxx"
#include "common/patterns/observer.hxx"


NAMESPACE_START(AmbarMetta);


class PlayerInfo;
class SrvLoginData;
class SrvEntityObject;


/** Class to manage the player inventory in the server
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvPlayerInventory
{
	friend class SrvEntityPlayer;

public:
	/** Add an item */
	bool addItem(const InventoryItem& item);
	/** Remove an item, returning false if not found */
	bool removeItem(EntityID itemID);
	/** Get an item */
	const InventoryItem* getItem(EntityID itemID) const;
	/** Get item by index */
	const InventoryItem* getItemByIndex(size_t index) const;
	/** Get the number of objects */
	size_t getCount();
	/** Set the load that can be accepted */
	void setMaxLoad(float load);
	/** Get the current load */
	float getCurrentLoad() const;

private:
	/// The collection of objects
	std::vector<InventoryItem> _inventory;
	/// Load accepted (will refuse to add more items)
	float _loadMax;
	/// Current load
	float _loadCurrent;


	/** Default constructor */
	SrvPlayerInventory();
};


/** Representation of a player in the server
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvEntityPlayer : public SrvEntityBase, public Observer
{
public:
	/** Default constructor
	 *
	 * @param basic Basic data to pass to the base class
	 *
	 * @param mov Movement data to pass to the base class
	 *
	 * @param data Player-especific data
	 *
	 * @param loginData Connection data
	 */
	SrvEntityPlayer(const MsgEntityCreate& basic,
			const MsgEntityMove& mov,
			const MsgPlayerData& data,
			SrvLoginData* loginData);
	/** Destructor */
	virtual ~SrvEntityPlayer();

	/** Get the login data */
	SrvLoginData* getLoginData() const;

	/** Update all the movement related stuff with the data provided by the
	 * client */
	void updateMovementFromClient(MsgEntityMove* msg);
	/** Send the info about movement to the player.
	 *
	 * \note The client normally doesn't receive the own position from the
	 * server, so it's because the position has been reseted or something
	 * similar. */
	void sendMovementToClient();

	/** Get the description of an item in the inventory */
	const InventoryItem* getInventoryItem(EntityID itemID) const;
	/** Add entity to the inventory, it returns false if there's some
	 * problem or the inventory is too loaded */
	bool addToInventory(const SrvEntityObject* object);
	/** Add entity to the inventory, it returns false if there's some
	 * problem or the inventory is too loaded */
	bool addToInventory(const InventoryItem* item);
	/** Remove entity from the inventory */
	void removeFromInventory(EntityID itemID);

	/** Get the player info */
	PlayerInfo* getPlayerInfo() const;
	/** Get the amount of gold */
	int32_t getGold() const;
	/** Add some amount of gold, result MUST be checked */
	bool addGold(int amount);
	/** Substract some amount of gold, result MUST be checked */
	bool subtractGold(int amount);

	/** @see Observer::updateFromObservable */
	virtual void updateFromObservable(const ObserverEvent& event);

protected:
	/// Player data
	MsgPlayerData _msgPlayerData;
	/// Login data for this player
	SrvLoginData* _loginData;
	/// Inventory
	SrvPlayerInventory _inventory;
	/// Extra player info (class, alignment)
	PlayerInfo* _playerInfo;


	/** Virtual function overriden from base class, to treat the specifics
	 * of the player. */
	virtual void saveToDB();
};


NAMESPACE_END(AmbarMetta);
#endif
