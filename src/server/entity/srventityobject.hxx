/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_ENTITY_OBJECT_H__
#define __AMBARMETTA_ENTITY_OBJECT_H__


/** \file srventityobject.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Represents an object in the server.
 */


#include "srventitybase.hxx"


NAMESPACE_START(AmbarMetta);


/** Representation of an object in the server
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvEntityObject : public SrvEntityBase
{
public:
	/** Default constructor
	 *
	 * @param basic Basic data to pass to the base class
	 *
	 * @param mov Movement data to pass to the base class
	 */
	SrvEntityObject(const MsgEntityCreate& basic,
			const MsgEntityMove& mov);
	/** Destructor */
	virtual ~SrvEntityObject();

	/** Set the load value */
	void setLoad(float load);
	/** Get the load value */
	float getLoad() const;

protected:
	/// Load value, basically the weight for dense objects and some
	/// estimated value for the other.  This is determined in files defining
	/// objects
	float _load;
};


NAMESPACE_END(AmbarMetta);
#endif
