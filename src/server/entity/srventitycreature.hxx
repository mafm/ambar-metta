/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_ENTITY_CREATURE_H__
#define __AMBARMETTA_ENTITY_CREATURE_H__


/** \file srventitycreature.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Represents a creature in the server.
 */


#include "srventitybase.hxx"


NAMESPACE_START(AmbarMetta);


/** Representation of a creature in the server
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvEntityCreature : public SrvEntityBase
{
public:
	/** Default constructor
	 *
	 * @param basic Basic data to pass to the base class
	 *
	 * @param mov Movement data to pass to the base class
	 *
	 * @param data Player-especific data
	 */
	SrvEntityCreature(const MsgEntityCreate& basic,
			  const MsgEntityMove& mov,
			  const MsgPlayerData& data);
	/** Destructor */
	virtual ~SrvEntityCreature();

protected:
	/// Player data
	MsgPlayerData _msgPlayerData;


	/** Virtual function overriden from base class, to treat the specifics
	 * of the player. */
	void saveToDB();
};


NAMESPACE_END(AmbarMetta);
#endif
