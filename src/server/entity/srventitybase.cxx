/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srventitybase.hxx"

#include "server/db/srvdbmgr.hxx"
#include "server/net/srvnetworkmgr.hxx"

#include "srventityplayer.hxx"

#include <osg/Matrix>
#include <osg/Vec3>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * SrvEntityBaseMovable
 ******************************************************************************/
SrvEntityBaseMovable::SrvEntityBaseMovable(const MsgEntityMove& mov) :
	_msgMov(mov)
{
}

SrvEntityBaseMovable::~SrvEntityBaseMovable()
{
}

void SrvEntityBaseMovable::recalculatePosition(uint32_t ms)
{
	/** \todo mafm: check that matches previous behaviour

	Vector3 current;
	current = _msgMov.position + (_msgMov.direction / static_cast<float>(ms));
	logDEBUG("Position recalculation (%ums):\n"
	       "Original:\t (%.1f, %.1f, %.1f)\n"
	       "Current:\t (%.1f, %.1f, %.1f)",
	       ms,
	       _msgMov.position.x, _msgMov.position.y, _msgMov.position.z,
	       current.x, current.y, current.z);
	_msgMov.position = current;
	*/

	Vector3 current;
	current = _msgMov.position + (_msgMov.direction / static_cast<float>(ms));
	logDEBUG("Position recalculation (%ums):\n"
		 "  - Original:\t (%.1f, %.1f, %.1f)\n"
		 "  - Current:\t (%.1f, %.1f, %.1f)",
		 ms,
		 _msgMov.position.x, _msgMov.position.y, _msgMov.position.z,
		 current.x, current.y, current.z);
	_msgMov.position = current;
}

void SrvEntityBaseMovable::getPosition(Vector3& position) const
{
	position = _msgMov.position;
}

void SrvEntityBaseMovable::getPositionWithRelativeOffset(Vector3& position,
							 const Vector3& offset) const
{
	/** \todo mafm: check that matches previous behaviour

	csReversibleTransform transf;

	// set the origin, which is the position of the movable entity
	transf.SetOrigin(_msgMov.position);

	// rotate around the "up" vector, so the offset is oriented to
	// where the entity looks at
	Vector3 up(0.0f, 1.0f, 0.0f);
	transf.RotateThis(up, _msgMov.yrot);

	// now really calculate the offset
	position = transf.This2Other(offset);

	*/

	osg::Matrix transf;
	transf.setTrans(_msgMov.position.x, _msgMov.position.y, _msgMov.position.z);
	transf.makeRotate(_msgMov.rot, osg::Z_AXIS);
	transf.makeTranslate(offset.x, offset.y, offset.z);

	osg::Vec3 pos = transf.getTrans();
	position.x = pos.x();
	position.y = pos.y();
	position.z = pos.z();

	logDEBUG("PosWithOffset: Pos (%.1f, %.1f, %.1f),"
		 " offset (%.1f, %.1f, %.1f), "
		 " result = (%.1f, %.1f, %.1f)",
		 _msgMov.position.x, _msgMov.position.y, _msgMov.position.z,
		 offset.x, offset.y, offset.z,
		 position.x, position.y, position.z);
}

float SrvEntityBaseMovable::getDistanceToEntity(const SrvEntityBaseMovable& other) const
{
	Vector3 otherPos;
	other.getPosition(otherPos);

	float distance = _msgMov.position.distance(otherPos);

	logDEBUG("Distance (%.1f, %.1f, %.1f) to (%.1f, %.1f, %.1f) = %0.1f",
		 _msgMov.position.x, _msgMov.position.y, _msgMov.position.z,
		 otherPos.x, otherPos.y, otherPos.z,
		 distance);

	return distance;
}

const char* SrvEntityBaseMovable::getArea() const
{
	return _msgMov.area.c_str();
}

void SrvEntityBaseMovable::setMovementData(const MsgEntityMove& mov)
{
	_msgMov = mov;
}


/*******************************************************************************
 * SrvEntityBaseObservable
 ******************************************************************************/
SrvEntityBaseObservable::SrvEntityBaseObservable(const MsgEntityCreate& basic,
						 const MsgEntityMove& mov) :
	SrvEntityBaseMovable(mov), _msgBasic(basic)
{
}

void SrvEntityBaseObservable::onAttachObserver(Observer* observer)
{
	logDEBUG("Subscribing observer '%s' to entity: name='%s', id='%llu',"
		 " pos(%.1f, %.1f, %.1f)",
		 observer->_name.c_str(), _msgBasic.entityName.c_str(), _msgBasic.entityID,
		 _msgMov.position.x, _msgMov.position.y, _msgMov.position.z);

	_msgBasic.position = _msgMov.position;
	_msgBasic.area = _msgMov.area;

	MsgEntityCreate msg = _msgBasic;
	SrvEntityBaseObserverEvent event(SrvEntityBaseObserverEvent::ENTITY_CREATE, msg);
	observer->updateFromObservable(event);
}

void SrvEntityBaseObservable::onDetachObserver(Observer* observer)
{
	logDEBUG("Unsubscribing observer '%s' from entity: name='%s', id='%llu'",
		 observer->_name.c_str(), _msgBasic.entityName.c_str(), _msgBasic.entityID);

	MsgEntityDestroy msg;
	msg.entityID= _msgBasic.entityID;
	SrvEntityBaseObserverEvent event(SrvEntityBaseObserverEvent::ENTITY_DESTROY, msg);
	observer->updateFromObservable(event);
}


/*******************************************************************************
 * SrvEntityBase
 ******************************************************************************/
SrvEntityBase::SrvEntityBase(const MsgEntityCreate& basic,
			     const MsgEntityMove& mov) :
	SrvEntityBaseObservable(basic, mov)
{
	_meshFactory = _msgBasic.meshType + "_" + _msgBasic.meshSubtype;
}

void SrvEntityBase::saveToDB()
{
	logDEBUG("SrvEntityBase::saveToDB()");

	// mafm: sometimes works bad with the "exact" data, +=0.5 for pos3=z
	// (height)
	std::string id = EntityID(_msgBasic.entityID).idStr;
	std::string area = _msgMov.area;
	std::string pos1 = strFmt("%.1f", _msgMov.position.x);
	std::string pos2 = strFmt("%.1f", _msgMov.position.y);
	std::string pos3 = strFmt("%.1f", _msgMov.position.z + 0.5f);
	std::string rot = strFmt("%.3f", _msgMov.rot);

	SrvDBQuery query;
	query.setTables("entities");
	query.setCondition("id='" + id + "'");
	query.addColumnWithValue("area", area);
	query.addColumnWithValue("pos1", pos1);
	query.addColumnWithValue("pos2", pos2);
	query.addColumnWithValue("pos3", pos3);
	query.addColumnWithValue("rot", rot);
	bool success = SrvDBMgr::instance().queryUpdate(&query);
	if (!success) {
		logERROR("Error while saving position for entity '%s'",
			 id.c_str());
		return;
	}

	logDEBUG("Saving position for entity '%s' in DB: '%s' (%s, %s, %s) rot=%s",
		 id.c_str(), _msgMov.area.c_str(),
		 pos1.c_str(), pos2.c_str(), pos3.c_str(), rot.c_str());
}

const char* SrvEntityBase::getName() const
{
	return _msgBasic.entityName.c_str();
}

EntityID SrvEntityBase::getID() const
{
	return EntityID(_msgBasic.entityID);
}

const char* SrvEntityBase::getType() const
{
	return _msgBasic.meshType.c_str();
}

const char* SrvEntityBase::getSubtype() const
{
	return _msgBasic.meshSubtype.c_str();
}

const char* SrvEntityBase::getMeshFactory() const
{
	return _meshFactory.c_str();
}

const char* SrvEntityBase::getEntityClass() const
{
	return _msgBasic.entityClass.c_str();
}


NAMESPACE_END(AmbarMetta);
