/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_ENTITY_BASE_H__
#define __AMBARMETTA_ENTITY_BASE_H__


/** \file srventitybase.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * Represents an entity (base class) in the server.
 */


#include "server/srvconfig.hxx"

#include "common/net/msgs.hxx"
#include "common/patterns/observer.hxx"

#include <string>


class SrvEntityPlayer;


NAMESPACE_START(AmbarMetta);


/** Represents and operates with the movement of entities.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvEntityBaseMovable
{
	friend class SrvEntityBase;

public:
	/** Recalculate position (this should be called periodically from the
	 * server, telling the number of milliseconds since the last update) */
	void recalculatePosition(uint32_t ms);
	/** Get the position */
	void getPosition(Vector3& position) const;
	/** Get the position with offset */
	void getPositionWithRelativeOffset(Vector3& position, const Vector3& offset) const;
	/** Return the distance of this entity to the one passed */
	float getDistanceToEntity(const SrvEntityBaseMovable& other) const;
	/** Get the area where this player is currently in */
	const char* getArea() const;

protected:
	/// Data needed for movement
	MsgEntityMove _msgMov;


	/** Constructor
	 *
	 * @param mov Movement message, representing all data needed for this
	 * entity to operate
	 */
	SrvEntityBaseMovable(const MsgEntityMove& mov);
	/** Destructor */
	~SrvEntityBaseMovable();


	/** Set movement data (setting position alone is not useful, because we
	 * need to change the direction and so on) */
	void setMovementData(const MsgEntityMove& mov);
};


/** ObserverEvent for ServerEntityBase
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvEntityBaseObserverEvent : public ObserverEvent
{
public:
	/** Action Identifier enumerator */
	enum ActionId { ENTITY_CREATE = 1, ENTITY_DESTROY };

	/** Action Identifier */
	const ActionId _actionId;

	/** Payload */
	MsgBase& _msg;

	/** Constructor */
	SrvEntityBaseObserverEvent(ActionId actionId, MsgBase& msg) :
		ObserverEvent("SrvEntityBaseObserverEvent"), _actionId(actionId), _msg(msg)
		{ }
};


/** Base class for a observable entity (objects, creatures, players...).
 *
 * Observers (players, in principle) can be added to the entity classes derived
 * from this, so the entity sends them notifications to create the entity,
 * remove it, and appropriete changes depending on position and state of the
 * entity.  It needs to be derived from Movable, because it's one of the main
 * points of this class: tell subscribers about our movements and actions.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvEntityBaseObservable : public SrvEntityBaseMovable, public Observable
{
protected:
	/// Basic data about the entity
	MsgEntityCreate _msgBasic;

	/** Constructor
	 *
	 * @param basic Entity basic data message, basic data about the entity
	 * to be represented
	 *
	 * @param mov For the base movement class
	 */
	SrvEntityBaseObservable(const MsgEntityCreate& basic,
				const MsgEntityMove& mov);
	/** Destructor */
	virtual ~SrvEntityBaseObservable() { }

	/** @see Observable::onAttachObserver */
	virtual void onAttachObserver(Observer* observer);
	/** @see Observable::onDetachObserver */
	virtual void onDetachObserver(Observer* observer);
};


/** Base entity class in the server, all the rest of the classes (players,
 * creatures, objects, NPCs, ...) must be derived from this one.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvEntityBase : public SrvEntityBaseObservable
{
public:
	/** Destructor */
	virtual ~SrvEntityBase() { }

	/** Get the name of the entity */
	const char* getName() const;
	/** Get the ID  of the entity */
	EntityID getID() const;
	/** Get the type (first part in the mesh factory, in example races) */
	const char* getType() const;
	/** Get the subtype (second part of the mesh factory, in example
	 * genders) */
	const char* getSubtype() const;
	/** Get the factory to create the mesh for this entity (combination of
	 * type+subtype) */
	const char* getMeshFactory() const;
	/** Get the entity class */
	const char* getEntityClass() const;

protected:
	/// The mesh factory (generated on the fly, but has to be a variable
	/// because we return a pointer)
	std::string _meshFactory;


	/** Constructor
	 *
	 * @param basic For the base observable class
	 *
	 * @param mov For the base movement class
	 */
	SrvEntityBase(const MsgEntityCreate& basic,
		      const MsgEntityMove& mov);

	/** Virtual function which can be overriden for especial entities
	 * (players at least), to save the state of the entity to the DB */
	virtual void saveToDB();
};


NAMESPACE_END(AmbarMetta);
#endif
