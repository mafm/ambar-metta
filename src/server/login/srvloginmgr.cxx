/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srvloginmgr.hxx"

#include "common/net/netlayer.hxx"
#include "common/net/msgs.hxx"
#include "common/configmgr.hxx"
#include "common/tablemgr.hxx"
#include "common/util.hxx"

#include "server/srvmain.hxx"
#include "server/db/srvdbmgr.hxx"
#include "server/entity/srventityplayer.hxx"
#include "server/net/srvnetworkmgr.hxx"
#include "server/world/srvworldmgr.hxx"


NAMESPACE_START(AmbarMetta);


/** Small internal class to serve as container when throwing errors, so we can
 * pass several parameters.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class SrvLoginError
{
public:
	/// Error code
	const MsgUtils::Errors::Codes errorCode;
	/// Message
	const std::string logMsg;


	/** Constructor */
	SrvLoginError(MsgUtils::Errors::Codes e, const std::string& msg) :
		errorCode(e), logMsg(msg) { }
};


/*******************************************************************************
 * SrvLoginData
 ******************************************************************************/
SrvLoginData::SrvLoginData(Netlink* n) :
	netlink(n),
	username("<none>"), uid("<none>"), charname("<none>"), cid("<none>"),
	entity(0),
	permLevel(Command::PermissionLevel::NotSet),
	playingGame(false)
{
}

const char* SrvLoginData::getUserName() const
{
	return username.c_str();
}

const char* SrvLoginData::getPlayerName() const
{
	return charname.c_str();
}

const char* SrvLoginData::getPlayerID() const
{
	return cid.c_str();
}

SrvEntityPlayer* SrvLoginData::getPlayerEntity() const
{
	return entity;
}

void SrvLoginData::setPlayerEntity(SrvEntityPlayer* e)
{
	entity = e;
}

const char* SrvLoginData::getIP() const
{
	return netlink->getIP();
}

Netlink* SrvLoginData::getNetlink() const
{
	return netlink;
}

bool SrvLoginData::isPlaying() const
{
	return playingGame;
}

void SrvLoginData::setPlaying(bool playing)
{
	playingGame = playing;
}

Command::PermissionLevel SrvLoginData::getPermissionLevel() const
{
	return permLevel;
}

void SrvLoginData::setPermissionLevel(Command::PermissionLevel lvl)
{
	permLevel = lvl;
}


/*******************************************************************************
 * SrvLoginMgr
 ******************************************************************************/
template <> SrvLoginMgr* Singleton<SrvLoginMgr>::INSTANCE = 0;

SrvLoginMgr::SrvLoginMgr()
{
	mNetMgr = &SrvNetworkMgr::instance();
	mDBMgr = &SrvDBMgr::instance();

	// fetching vars from config file
	mProtocolVersion = ConfigMgr::instance().getConfigVar("Server.Network.ProtocolVersion", "99");
	if (mProtocolVersion == "99") {
		logERROR("Couldn't get ProtocolVersion variable");
	}
	mMaxCharsPerAccount = atoi(ConfigMgr::instance().getConfigVar("Server.Characters.MaxCharactersPerAccount", "-1").c_str());
	mNewCharArea = ConfigMgr::instance().getConfigVar("Server.Characters.NewCharArea", "-1");
	mNewCharPosX = ConfigMgr::instance().getConfigVar("Server.Characters.NewCharPosX", "-1");
	mNewCharPosY = ConfigMgr::instance().getConfigVar("Server.Characters.NewCharPosY", "-1");
	mNewCharPosZ = ConfigMgr::instance().getConfigVar("Server.Characters.NewCharPosZ", "-1");
	if (mMaxCharsPerAccount == -1
	    || mNewCharArea == "-1" || mNewCharPosX == "-1"
	    || mNewCharPosY == "-1"  || mNewCharPosZ == "-1") {
		logERROR("Couldn't fetch variables for new characters");
	}
}

void SrvLoginMgr::finalize()
{
	while (!mPlayerList.empty()) {
		SrvLoginData* elem = mPlayerList.back();
		mPlayerList.pop_back();
		delete elem;
	}
}

void SrvLoginMgr::addConnection(Netlink* netlink)
{
	// steps
	// 1- Check if already logged in, otherwise register connection
	// 2- get data (server statistics) from the db, and send it

	MsgConnectReply repmsg;

	// 1- Check if already logged in, otherwise register connection
	SrvLoginData* newConn = new SrvLoginData(netlink);

	/* mafm: disabling only-one-IP restriction, because it's annoying for
	 * testing (can't connect in the same computer...)

	for (auto it = mPlayerList.begin(); it != mPlayerList.end(); ++it) {
		if ((*it)->ip == newConn->ip) {
			repmsg.resultCode = EALREADYLOGGED; // Login failed (already logged in)
			mNetMgr->sendToPlayer(repmsg, newConn);
			logWARNING("Rejecting new connection, "
			       "already connected from the same IP (%s)",
			       newConn->ip.c_str());
			delete newConn;
			return;
		}
	}
	*/
	mPlayerList.push_back(newConn);

	// 2- get data (server statistics) from the db, and send it
	{
		repmsg.resultCode = MsgUtils::Errors::SUCCESS;
		repmsg.protocolVersion = mProtocolVersion;
		repmsg.uptime = SrvMain::instance().getUptime();
		repmsg.totalUsers = getNumberOfAccounts();
		repmsg.totalChars = getNumberOfCharacters();
		repmsg.currentPlayers = getNumberOfConnectionsPlaying();
		mNetMgr->sendToPlayer(repmsg, newConn);
		logINFO("Sending connect reply info (IP: '%s')",
			newConn->netlink->getIP());
	}

	/* test data, to check if the serialization/deserialization process is
	 * done correctly

	{
		MsgTestDataTypes testmsg;
		mNetMgr->sendToPlayer(testmsg, newConn);
	}
	*/
}

void SrvLoginMgr::removeConnection(SrvLoginData* loginData)
{
	removeConnection(loginData->netlink);
}

void SrvLoginMgr::removeConnection(Netlink* netlink)
{
	for (auto it = mPlayerList.begin(); it != mPlayerList.end(); ++it) {
		if ((*it)->netlink == netlink) {
			logINFO("Removing dead connection (usr: '%s', char: '%s', IP: '%s')",
				(*it)->getUserName(),
				(*it)->getPlayerName(),
				(*it)->getIP());

			// remove from world manager
			if ((*it)->isPlaying()) {
				logDEBUG("Player playing, removing from WorldMgr");
				SrvWorldMgr::instance().removePlayer(**it);

				// update time playing
				std::string charname = (*it)->charname;
				SrvDBQuery query;
				query.setTables("usr_chars");
				query.setCondition("charname='" + charname + "'");
				query.addColumnWithValue("time_playing",
							 "time_playing+CURRENT_TIMESTAMP-last_login",
							 false);
				bool success = mDBMgr->queryUpdate(&query);
				if (!success) {
					logERROR("Couldn't update time_playing for character '%s'",
						 charname.c_str());
				}
			}

			// remove from this one
			delete *it;
			mPlayerList.erase(it);
			return;
		}
	}
}

void SrvLoginMgr::login(SrvLoginData* loginData, std::string username, std::string pwd)
{
	// STEPS
	// 1- get data from the client form, check if the usr exists in db
	// 2- check password
	// 3- update user data (last_login, etc)
	// 4- send back data to user (chars available)

	MsgLoginReply repmsg;

	try {
		// 1- get username data
		username = mDBMgr->escape(username);
		std::string uid, passworddb;
		{
			SrvDBQuery query;
			query.addColumnWithoutValue("uid");
			query.addColumnWithoutValue("password");
			query.setTables("usr_accts");
			query.setCondition("username='" + username + "'");
			int numresults = mDBMgr->querySelect(&query);
			if (numresults != 1) {
				std::string logMsg = strFmt("No such user '%s', numresults %d",
							    username.c_str(), numresults);
				throw SrvLoginError(MsgUtils::Errors::EBADLOGIN, logMsg);
			}
			query.getResult()->getValue(0, "uid", uid);
			query.getResult()->getValue(0, "password", passworddb);
		}

		// 2- password cheching
		if (passworddb != pwd) {
			std::string logMsg = strFmt("Wrong password for '%s'", username.c_str());
			throw SrvLoginError(MsgUtils::Errors::EBADLOGIN, logMsg);
		}
		loginData->username = username;
		loginData->uid = uid;

		// 3- updating last login
		{
			SrvDBQuery query;
			query.setTables("usr_accts");
			query.setCondition("uid='" + uid + "'");
			query.addColumnWithValue("last_login", "CURRENT_TIMESTAMP", false);
			query.addColumnWithValue("number_logins", "number_logins+1", false);
			query.addColumnWithValue("last_login_ip", loginData->getIP());
			bool success = mDBMgr->queryUpdate(&query);
			if (!success) {
				std::string logMsg = strFmt("Couldn't update last_login for '%s'",
							    username.c_str());
				throw SrvLoginError(MsgUtils::Errors::EDATABASE, logMsg);
			}
		}

		// 4. sending back data to user (chars available)
		{
			SrvDBQuery query;
			query.setTables("usr_chars");
			query.setCondition("uid='" + uid + "' AND status='0'");
			query.addColumnWithoutValue("charname");
			query.addColumnWithoutValue("race");
			query.addColumnWithoutValue("gender");
			query.addColumnWithoutValue("class");
			query.addColumnWithoutValue("area");
			int numresults = mDBMgr->querySelect(&query);
			if (numresults > mMaxCharsPerAccount) {
				std::string logMsg = strFmt("This user has more than %d characters ('%s', num. chars %d)",
							    mMaxCharsPerAccount, username.c_str(), numresults);
				throw SrvLoginError(MsgUtils::Errors::ECHARCORRUPT, logMsg);
			}

			repmsg.resultCode = MsgUtils::Errors::SUCCESS;
			repmsg.charNumber = numresults;
			for (int row = 0; row < numresults; ++row) {
				std::string name, race, gender, playerClass, area;
				query.getResult()->getValue(row, "charname", name);
				query.getResult()->getValue(row, "race", race);
				query.getResult()->getValue(row, "gender", gender);
				query.getResult()->getValue(row, "class", playerClass);
				query.getResult()->getValue(row, "area", area);
				repmsg.addCharacter(name, race, gender, playerClass, area);
			}
			mNetMgr->sendToPlayer(repmsg, loginData);
			logINFO("User login successful: '%s'", username.c_str());
		}
	} catch (SrvLoginError& e) {
		repmsg.resultCode = e.errorCode;
		mNetMgr->sendToPlayer(repmsg, loginData);
		logWARNING("%s", e.logMsg.c_str());
		return;
	}
}

void SrvLoginMgr::createUser(SrvLoginData* loginData,
			     std::string username,
			     std::string pwd,
			     std::string email,
			     std::string realname)
{
	// STEPS
	// 1- get data from the client form
	// 2- check if username already exists
	// 3- create the user account

	MsgNewUserReply repmsg;

	try {
		// 1- get and prepare the data from the client
		username = mDBMgr->escape(username);
		pwd = mDBMgr->escape(pwd);
		email = mDBMgr->escape(email);
		realname = mDBMgr->escape(realname);

		// 2- check if username already exists
		{
			SrvDBQuery query;
			query.setTables("usr_accts");
			query.setCondition("username='" + username + "'");
			bool matches = mDBMgr->queryMatch(&query);
			if (matches) {
				std::string logMsg = strFmt("Create new user: already exists ('%s')",
							    username.c_str());
				throw SrvLoginError(MsgUtils::Errors::EUSERALREADYEXIST, logMsg);
			}
		}

		// 3- insert new data into the usr_accts
		{
			SrvDBQuery query;
			query.setTables("usr_accts");
			query.addColumnWithValue("username", username);
			query.addColumnWithValue("password", pwd);
			query.addColumnWithValue("email", email);
			query.addColumnWithValue("realname", realname);
			query.addColumnWithValue("roles",
						 strFmt("%d", static_cast<int>(Command::PermissionLevel::Player)));
			//query.addColumnWithValue("creation_date", "CURRENT_TIMESTAMP", false);
			bool success = mDBMgr->queryInsert(&query);
			if (!success) {
				std::string logMsg = strFmt("Create new user: failed because of DB problems ('%s')",
							    username.c_str());
				throw SrvLoginError(MsgUtils::Errors::EDATABASE, logMsg);
			}

			repmsg.resultCode = MsgUtils::Errors::SUCCESS;
			mNetMgr->sendToPlayer(repmsg, loginData);
			logINFO("New user created successfully: '%s'", username.c_str());
		}
	} catch (SrvLoginError& e) {
		repmsg.resultCode = e.errorCode;
		mNetMgr->sendToPlayer(repmsg, loginData);
		logWARNING("%s", e.logMsg.c_str());
		return;
	}
}

void SrvLoginMgr::createCharacter(SrvLoginData* loginData,
				  std::string charname,
				  std::string race,
				  std::string gender,
				  std::string playerClass,
				  uint8_t points_con,
				  uint8_t points_str,
				  uint8_t points_dex,
				  uint8_t points_int,
				  uint8_t points_wis,
				  uint8_t points_cha)
{
	// STEPS
	// 1- get data from the client form
	// 2- check if the character already exists
	// 3- check if the user has <= 8 active chars
	// 4- check if race, name and gender (and other data?) is appropriate
	// 5- create the new character

	MsgNewCharReply repmsg;

	try {
		// 1- get data from the client form
		charname = mDBMgr->escape(charname);
		race = mDBMgr->escape(race);
		gender = mDBMgr->escape(gender);
		playerClass = mDBMgr->escape(playerClass);

		// 2- check if the character already exists
		{
			SrvDBQuery query;
			query.setTables("usr_chars");
			query.setCondition("charname='" + charname + "'");
			bool matches = mDBMgr->queryMatch(&query);
			if (matches) {
				std::string logMsg = strFmt("Create new character: already exists ('%s')",
							    charname.c_str());
				throw SrvLoginError(MsgUtils::Errors::ECHARALREADYEXIST, logMsg);
			}
		}


		// 3- check if the user has <ALLOWED active chars
		{
			SrvDBQuery query;
			query.setTables("usr_chars");
			query.setCondition("uid='" + loginData->uid + "' AND status='0'");
			int numresults = mDBMgr->queryMatchNumber(&query);
			if (numresults >= mMaxCharsPerAccount) {
				std::string logMsg = strFmt("Create new character: too many chars (user '%s', %d)",
							    loginData->username.c_str(), numresults);
				throw SrvLoginError(MsgUtils::Errors::EMAXCHARS, logMsg);
			}
		}

		// 4- check if race, name and gender (and other data?) is appropriate
		if (!(race == "dwarf" || race == "elf" || race == "human")
		    || !(gender == "f" || gender == "m")
		    || !(playerClass == "fighter" || playerClass == "sorcerer") ) {
			std::string logMsg = strFmt("Create new character: bad data "
						    "(user %s, race '%s', gender '%s', class '%s')",
						    loginData->username.c_str(), race.c_str(),
						    gender.c_str(), playerClass.c_str());
			throw SrvLoginError(MsgUtils::Errors::ENEWCHARBADDATA, logMsg);
		}

		// abilities must be greater than 3 and less than 18, and if all
		// points are used, the total should equal 78 (avg 13 per
		// ability)

		int abilities_sum = points_con + points_str + points_dex +
			points_int + points_wis + points_cha;
		if (78 != abilities_sum
		    || points_str < 3 || points_str > 18
		    || points_dex < 3 || points_dex > 18
		    || points_con < 3 || points_con > 18
		    || points_int < 3 || points_int > 18
		    || points_wis < 3 || points_wis > 18
		    || points_cha < 3 || points_cha > 18) {
			std::string logMsg = strFmt("Create new character: bad data (user '%s', points %d,"
						    " con=%u str=%u agi=%u int=%u wis=%u cha=%u)",
						    loginData->username.c_str(), abilities_sum,
						    points_con, points_str, points_dex,
						    points_int, points_wis, points_cha);
			throw SrvLoginError(MsgUtils::Errors::ENEWCHARBADDATA, logMsg);
		}

		// 5- create the new character
		{
			SrvDBQuery query;
			query.setTables("usr_chars");
			query.addColumnWithValue("uid", loginData->uid);
			//query.addColumnWithValue("cid", "DEFAULT", false);
			query.addColumnWithValue("charname", charname);
			query.addColumnWithValue("area", mNewCharArea);
			query.addColumnWithValue("pos1", mNewCharPosX, false);
			query.addColumnWithValue("pos2", mNewCharPosY, false);
			query.addColumnWithValue("pos3", mNewCharPosZ, false);
			query.addColumnWithValue("rot", "0.0", false);
			query.addColumnWithValue("race", race);
			query.addColumnWithValue("gender", gender);
			query.addColumnWithValue("class", playerClass);
			bool success = mDBMgr->queryInsert(&query);

			// sum base points plus player selected
			int tot_con = points_con;
			int tot_str = points_str;
			int tot_dex = points_dex;
			int tot_int = points_int;
			int tot_wis = points_wis;
			int tot_cha = points_cha;

			// add race bonus - using d20 race modifiers
			if (race == "elf") {
				tot_con -= 2;
				tot_dex += 2;
			} else if (race == "dwarf") {
				tot_con += 2;
				tot_dex -= 2;
			} else if (race == "human") {
				// no change
			} else {
				throw SrvLoginError(MsgUtils::Errors::ENEWCHARBADDATA, "unknown race");
			}

			std::string ab_con = strFmt("%i", tot_con);
			std::string ab_str = strFmt("%i", tot_str);
			std::string ab_dex = strFmt("%i", tot_dex);
			std::string ab_int = strFmt("%i", tot_int);
			std::string ab_wis = strFmt("%i", tot_wis);
			std::string ab_cha = strFmt("%i", tot_cha);

			/// \todo mafm: reimplement
			// health: look at stats.h
			/*
			int health = (10
				      + Stats::getAbilityModifier(tot_con)
				      + Stats::getAbilityModifier(tot_str));
			*/
			int health = (10 + tot_con + tot_str);

			/** \todo: duffolonious: handle magic differently.  The
			 * magic score will be calculated as such: (memorized
			 * spells)/(total spells) = magic.  Also, the system is
			 * weighted, so a level three spell is worth 4 points
			 * (because level 0 spells need to be worth a point)
			 * This is low priority.
			 */
			std::string magic = "0";

			SrvDBQuery query2;
			query2.setTables("player_stats");
			query2.addColumnWithValue("charname", charname);
			query2.addColumnWithValue("health", strFmt("%i", health));
			query2.addColumnWithValue("magic", magic);
			query2.addColumnWithValue("stamina", "'100'", false);
			query2.addColumnWithValue("ab_con", ab_con);
			query2.addColumnWithValue("ab_str", ab_str);
			query2.addColumnWithValue("ab_dex", ab_dex);
			query2.addColumnWithValue("ab_int", ab_int);
			query2.addColumnWithValue("ab_wis", ab_wis);
			query2.addColumnWithValue("ab_cha", ab_cha);
			query2.addColumnWithValue("level", "1");
			success = mDBMgr->queryInsert(&query2);
			if (!success) {
				std::string logMsg = strFmt("Create new character: DB failure (user '%s', char '%s')",
							    loginData->username.c_str(), charname.c_str());
				throw SrvLoginError(MsgUtils::Errors::EDATABASE, logMsg);
			}

			repmsg.resultCode = MsgUtils::Errors::SUCCESS; // success
			repmsg.character.name = charname;
			repmsg.character.race = race;
			repmsg.character.gender = gender;
			repmsg.character.playerClass = playerClass;
			repmsg.character.area = mNewCharArea;
			mNetMgr->sendToPlayer(repmsg, loginData);
			logINFO("New character created successfully (user '%s', char '%s')",
				loginData->username.c_str(), charname.c_str());
		}
	} catch (SrvLoginError& e) {
		repmsg.resultCode = e.errorCode;
		mNetMgr->sendToPlayer(repmsg, loginData);
		logWARNING("%s", e.logMsg.c_str());
		return;
	}
}

void SrvLoginMgr::deleteCharacter(SrvLoginData* loginData, std::string charname)
{
	// STEPS
	// 1- get data from the client form
	// 2- check if the character already exists and belongs to the user
	// 3- delete the new character

	MsgDelCharReply repmsg;

	try {
		// 1- get data from the client form
		charname = mDBMgr->escape(charname);

		// 2- check if already exists and belongs to the user
		{
			SrvDBQuery query;
			query.setTables("usr_chars");
			query.setCondition("uid='" + loginData->uid
					   + "' AND charname='" + charname + "'");
			bool matches = mDBMgr->queryMatch(&query);
			if (!matches) {
				std::string logMsg = strFmt("Delete new character: doesn't exist or doesn't belong to"
							    " (user '%s', char '%s')",
							    loginData->username.c_str(), charname.c_str());
				throw SrvLoginError(MsgUtils::Errors::ENOSUCHCHAR, logMsg);
			}
		}

		// 3- delete the character
		{
			SrvDBQuery query;
			query.setTables("usr_chars");
			query.setCondition("charname='" + charname + "'");
			query.addColumnWithValue("status", "1", false);
			int numresults = mDBMgr->queryUpdate(&query);
			if (numresults != 1) {
				std::string logMsg = strFmt("Delete new character: DB failure (user '%s', char '%s')",
							    loginData->username.c_str(), charname.c_str());
				throw SrvLoginError(MsgUtils::Errors::ENOSUCHCHAR, logMsg);
			}

			repmsg.resultCode = MsgUtils::Errors::SUCCESS;
			repmsg.charname = charname;
			mNetMgr->sendToPlayer(repmsg, loginData);
			logINFO("Character deleted successfully (user '%s', char '%s')",
				loginData->username.c_str(), charname.c_str());
		}
	} catch (SrvLoginError& e) {
		repmsg.resultCode = e.errorCode;
		mNetMgr->sendToPlayer(repmsg, loginData);
		logWARNING("%s", e.logMsg.c_str());
		return;
	}
}

void SrvLoginMgr::joinGame(SrvLoginData* loginData, std::string charname)
{
	// STEPS
	// 1- get data from the client form
	// 2- check if the usr/char already joined the game with any character
	// 3- check if the character belongs to the user, and get the data
	// 4- join game

	MsgJoinReply repmsg;

	try {
		// 1- get data from the client form
		charname = mDBMgr->escape(charname);
		std::string playerClass, cid;

		// 2- check if the usr/char already joined the game with any character
		if (loginData->charname != "<none>") {
			std::string logMsg = strFmt("Joing game: already playing (char '%s')",
						    loginData->charname.c_str());
			throw SrvLoginError(MsgUtils::Errors::EALREADYPLAYING, logMsg);
		}

		// 3- check if the character belongs to the user, and get the data
		MsgEntityCreate msgBasic;
		MsgEntityMove msgMove;
		MsgPlayerData msgPlayer;
		{
			SrvDBQuery query;
			query.setTables("usr_chars,usr_accts");
			std::string condition = "charname='" + charname
				+ "' AND usr_accts.uid='" + loginData->uid
				+ "' AND usr_accts.uid=usr_chars.uid";
			query.setCondition(condition);
			query.addColumnWithoutValue("cid");
			query.addColumnWithoutValue("area");
			query.addColumnWithoutValue("pos1");
			query.addColumnWithoutValue("pos2");
			query.addColumnWithoutValue("pos3");
			query.addColumnWithoutValue("rot");
			query.addColumnWithoutValue("race");
			query.addColumnWithoutValue("gender");
			query.addColumnWithoutValue("class");
			query.addColumnWithoutValue("roles");
			int numresults = mDBMgr->querySelect(&query);
			if (numresults != 1) {
				std::string logMsg = strFmt("Joing game: no such char (char '%s', numresults %d)",
							    loginData->charname.c_str(), numresults);
				throw SrvLoginError(MsgUtils::Errors::ENOSUCHCHAR, logMsg);
			}

			float pos1 = 0.0f, pos2 = 0.0f, pos3 = 0.0f, rot = 0.0f;
			int roles = 0;
			query.getResult()->getValue(0, "cid", cid);
			query.getResult()->getValue(0, "area", msgMove.area);
			query.getResult()->getValue(0, "pos1", pos1);
			query.getResult()->getValue(0, "pos2", pos2);
			query.getResult()->getValue(0, "pos3", pos3);
			query.getResult()->getValue(0, "rot", rot);
			query.getResult()->getValue(0, "race", msgBasic.meshType);
			query.getResult()->getValue(0, "gender", msgBasic.meshSubtype);
			query.getResult()->getValue(0, "class", playerClass);
			query.getResult()->getValue(0, "roles", roles);

			msgMove.position = Vector3(pos1, pos2, pos3);
			msgMove.rot = rot;
			msgMove.entityID = EntityID(cid).id;
			msgBasic.entityID = msgMove.entityID;
			msgBasic.area = msgMove.area;
			msgBasic.entityName = charname;
			msgBasic.entityClass = "Player";
			msgBasic.position = msgMove.position;
			msgBasic.rot = msgMove.rot;
			if (roles == static_cast<int>(Command::PermissionLevel::Player)) {
				loginData->setPermissionLevel(Command::PermissionLevel::Player);
			} else if (roles == static_cast<int>(Command::PermissionLevel::Admin)) {
				loginData->setPermissionLevel(Command::PermissionLevel::Admin);
			} else {
				logERROR("The permission stored in the DB for player '%s'"
					 " is not recognized: %d",
					 charname.c_str(), roles);
			}

			// get player statistics
			SrvDBQuery query2;
			query2.setTables("player_stats");
			query2.setCondition("charname='" + charname + "'");
			query2.addColumnWithoutValue("health");
			query2.addColumnWithoutValue("magic");
			query2.addColumnWithoutValue("stamina");
			query2.addColumnWithoutValue("gold");
			query2.addColumnWithoutValue("level");
			query2.addColumnWithoutValue("ab_con");
			query2.addColumnWithoutValue("ab_str");
			query2.addColumnWithoutValue("ab_dex");
			query2.addColumnWithoutValue("ab_int");
			query2.addColumnWithoutValue("ab_wis");
			query2.addColumnWithoutValue("ab_cha");
			numresults = mDBMgr->querySelect(&query2);
			if (numresults != 1) {
				std::string logMsg = strFmt("Joing game: no such char (char '%s', numresults %d)",
							    loginData->charname.c_str(), numresults);
				throw SrvLoginError(MsgUtils::Errors::ENOSUCHCHAR, logMsg);
			}

			int health_cur = 0, magic_cur = 0, stamina = 0, gold = 0, level = 0;
			int ab_con = 0, ab_str = 0, ab_dex = 0, ab_int = 0, ab_wis = 0, ab_cha = 0;
			query2.getResult()->getValue(0, "health", health_cur);
			query2.getResult()->getValue(0, "magic", magic_cur);
			query2.getResult()->getValue(0, "stamina", stamina);
			query2.getResult()->getValue(0, "gold", gold);
			query2.getResult()->getValue(0, "level", level);
			query2.getResult()->getValue(0, "ab_con", ab_con);
			query2.getResult()->getValue(0, "ab_str", ab_str);
			query2.getResult()->getValue(0, "ab_dex", ab_dex);
			query2.getResult()->getValue(0, "ab_int", ab_int);
			query2.getResult()->getValue(0, "ab_wis", ab_wis);
			query2.getResult()->getValue(0, "ab_cha", ab_cha);

			msgPlayer.health_cur = health_cur;
			msgPlayer.magic_cur = magic_cur;
			msgPlayer.stamina = stamina;
			msgPlayer.gold = gold;
			msgPlayer.level = level;
			msgPlayer.ab_con = ab_con;
			msgPlayer.ab_str = ab_str;
			msgPlayer.ab_dex = ab_dex;
			msgPlayer.ab_int = ab_int;
			msgPlayer.ab_wis = ab_wis;
			msgPlayer.ab_cha = ab_cha;

			/// \todo duffolonious: Here is a hack to set max health
			/// (soon load) currectly.
			/*
			PlayerInfo tmp;
			tmp.setProperty("strength", ab_str);
			tmp.setProperty("constitution", ab_con);
			msgPlayer.health_max = tmp.getHP();
			msgPlayer.magic_max = 100;
			*/
			msgPlayer.health_max = health_cur;
			msgPlayer.magic_max = 100;
			std::string strength = strFmt("%d", ab_str);
			msgPlayer.load_max = TableMgr::instance().getValueAsInt("load", strength, "light");
			msgPlayer.load_cur = 0;
		}

		// 4- join game
		{
			SrvDBQuery query;
			query.setTables("usr_chars");
			query.setCondition("charname='" + charname + "'");
			query.addColumnWithValue("last_login", "CURRENT_TIMESTAMP", false);
			query.addColumnWithValue("number_logins", "number_logins+1", false);
			bool success = mDBMgr->queryUpdate(&query);
			if (!success) {
				std::string logMsg = strFmt("Joing game: DB failure (char '%s')",
							    loginData->charname.c_str());
				throw SrvLoginError(MsgUtils::Errors::EDATABASE, logMsg);
			}
		}
		loginData->charname = charname;
		loginData->cid = cid;
		SrvEntityPlayer* player = new SrvEntityPlayer(msgBasic, msgMove, msgPlayer, loginData);
		/** \todo mafm: implement

		player->getPlayerInfo()->setClass(playerClass);
		*/
		loginData->setPlayerEntity(player);
		SrvWorldMgr::instance().addPlayer(*loginData);

	} catch (SrvLoginError& e) {
		repmsg.resultCode = e.errorCode;
		mNetMgr->sendToPlayer(repmsg, loginData);
		logWARNING("%s", e.logMsg.c_str());
		return;
	}
}

SrvLoginData* SrvLoginMgr::findPlayer(const Netlink* netlink) const
{
	for (auto it = mPlayerList.begin(); it != mPlayerList.end(); ++it) {
		if ((*it)->netlink == netlink) {
			return *it;
		}
	}
	logWARNING("Cannot find SrvLoginData for netlink (socket %d, IP '%s')",
		   netlink->getSocket(), netlink->getIP());
	return 0;
}

SrvLoginData* SrvLoginMgr::findPlayer(const std::string& playerName) const
{
	for (auto it = mPlayerList.begin(); it != mPlayerList.end(); ++it) {
		if ((*it)->charname == playerName) {
			return *it;
		}
	}
	logWARNING("Cannot find SrvLoginData for player name '%s')",
		   playerName.c_str());
	return 0;
}

SrvLoginData* SrvLoginMgr::findPlayer(EntityID id) const
{
	for (auto it = mPlayerList.begin(); it != mPlayerList.end(); ++it) {
		if (id == EntityID((*it)->cid).id) {
			return *it;
		}
	}
	logWARNING("Cannot find SrvLoginData for player id '%s')", id.idStr.c_str());
	return 0;
}

int SrvLoginMgr::getNumberOfAccounts() const
{
	SrvDBQuery query;
	query.setTables("usr_accts");
	query.setCondition("1=1");
	return SrvDBMgr::instance().queryMatchNumber(&query);
}

int SrvLoginMgr::getNumberOfCharacters() const
{
	SrvDBQuery query;
	query.setTables("usr_chars");
	query.setCondition("1=1");
	return SrvDBMgr::instance().queryMatchNumber(&query);
}

size_t SrvLoginMgr::getNumberOfConnections() const
{
	return mPlayerList.size();
}

size_t SrvLoginMgr::getNumberOfConnectionsPlaying() const
{
	size_t count = 0;
	for (size_t i = 0; i < mPlayerList.size(); ++i) {
		if (mPlayerList[i]->isPlaying()) {
			++count;
		}
	}
	return count;
}

void SrvLoginMgr::getAllConnections(std::vector<SrvLoginData*>& allConnections) const
{
	allConnections = mPlayerList;
}

void SrvLoginMgr::getAllConnectionsPlaying(std::vector<SrvLoginData*>& allPlayers) const
{
	for (size_t i = 0; i < mPlayerList.size(); ++i) {
		if (mPlayerList[i]->isPlaying()) {
			allPlayers.push_back(mPlayerList[i]);
		}
	}
}

void SrvLoginMgr::getPlayerNameList(std::list<std::string>& nameList) const
{
	for (size_t i = 0; i < mPlayerList.size(); ++i) {
		if (mPlayerList[i]->isPlaying()) {
			nameList.push_back(mPlayerList[i]->getPlayerName());
		}
	}
}


NAMESPACE_END(AmbarMetta);
