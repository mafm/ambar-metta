/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_DATAMODEL_H__
#define __AMBARMETTA_CLIENT_DATAMODEL_H__


/** \file cltdatamodel.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * The data model contains a set of key-value pairs with data about the system,
 * that might be subscribed to and used in any part of the application.
 *
 * This is done with the intention to decouple many classes from many others and
 * couple them only with the data that they're interested in, especially the
 * GUI, which tends to change very often.
 *
 * Notable uses of these are GUI widgets, which will update different
 * characteristics in the interface as soon as the client receives them from the
 * server (or gets them by other means).  Hopefully in this way the non-GUI
 * classes won't have to depend on any GUI classes except on very specific
 * cases.
 */


#include "common/patterns/observer.hxx"
#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"

#include <map>
#include <utility>


NAMESPACE_START(AmbarMetta);


/** ObserverEvent for CltDataModelMgr.
 *
 * This observer event lets the receiver know about the new value of a variable.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltDataModelMgrObserverEvent : public ObserverEvent
{
public:
	/** Action Identifier enumerator */
	enum ActionId : int { NewValue = 1 };

	//// Action Identifier
	const ActionId _actionId;

	/// Payload
	std::pair<std::string, std::string> _data;


	/** Constructor */
	CltDataModelMgrObserverEvent(ActionId actionId, const std::string& key, const std::string& value) :
		ObserverEvent("CltDataModelMgrObserverEvent"),
		_actionId(actionId),
		_data(std::pair<std::string, std::string>(key, value))
		{ }
};


/** The data model contains a set of key-value pairs with data about the system.
 *
 * It implements an Observable, so other objects might subscribe to be notified
 * about changes in the model.  Notable uses of these are GUI widgets, who will
 * update different characteristics in the interface as soon as the client
 * receives them from the server (or gets them by other means).
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltDataModelMgr : public Singleton<CltDataModelMgr>, public Observable
{
public:
	/** Add or update a key-value pair */
	void addOrUpdate(const std::string& key, const std::string& value);

private:
	/** Singleton friend access */
	friend class Singleton<CltDataModelMgr>;


	/// Variables of the data model (key, value)
	std::map<std::string, std::string> _dataModel;


	/** Default constructor */
	CltDataModelMgr();
	/** Destructor */
	~CltDataModelMgr();

	/** @see Observable::onAttachObserver()
	 *
	 * When an observer attaches, this class sends an update to the observer
	 * with all of the values.
	 */
	virtual void onAttachObserver(Observer* observer);
};


NAMESPACE_END(AmbarMetta);
#endif
