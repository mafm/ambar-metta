/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_INPUT_H__
#define __AMBARMETTA_CLIENT_INPUT_H__


/** \file cltinput.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * Classes to handle keyboard and mouse input.
 */


#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"

#include <osgGA/GUIEventHandler>


NAMESPACE_START(AmbarMetta);


/** Event handler to capture keyboard/mouse (in general, input) events,
 * controlling the camera and other aspects.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltInputHandler : public osgGA::GUIEventHandler 
{
public:
	/** Default constructor */
	CltInputHandler();

	/** Destructor */
	~CltInputHandler();

	/** @see osgGA::GUIEventHandler::className() */
	virtual const char* className() const;

	/** @see osgGA::GUIEventHandler::handle() */
	virtual bool handle(const osgGA::GUIEventAdapter& ea,
			    osgGA::GUIActionAdapter& aa,
			    osg::Object* o,
			    osg::NodeVisitor* nv);
	/** @see osgGA::GUIEventHandler::handle() */
	virtual bool handle(const osgGA::GUIEventAdapter& ea,
			    osgGA::GUIActionAdapter& aa);

private:
	/// Name that will be used as identifier
	const std::string _className;
};


/** Controls main player movement and actions, based on the input.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltMainPlayerManipulator : public Singleton<CltMainPlayerManipulator>
{
public:
	/** Feeded every frame to know the time since last update sent */
	void tick(double elapsedSeconds);

	/** Set the full position of the player in the given area. */
	void setPosition(const osg::Vec3& position, float rotation);

	/** Set this action state. */
	void setMovingForward(bool b);
	/** Set this action state. */
	void setMovingBackward(bool b);
	/** Set this action state. */
	void setRunning(bool b);
	/** Toggle autorunning. */
	void toggleAutoRunning();
	/** Set this action state. */
	void setRotatingLeft(bool b);
	/** Set this action state. */
	void setRotatingRight(bool b);
	/** Find out whether the action is idle, or it's doing something
	 * (shortcut, returning OR of the boolean values). */
	bool isIdle() const;

	/** Get the transformation matrix, for other classes which might need to
	 * view or manipulate it. */
	osg::MatrixTransform* getMatrixTransform() const;

private:
	/** Singleton friend access */
	friend class Singleton<CltMainPlayerManipulator>;


	/// Speed when walking
	static const float WALK_SPEED;
	/// Speed when running
	static const float RUN_SPEED;
	/// Speed when rotating left of right
	static const float ROTATE_SPEED;

	/// The transformation representing the position of the player, and
	/// where's looking at
	osg::MatrixTransform* _playerTransform;

	/// Action state
	bool _movingForward;
	/// Action state
	bool _movingBackward;
	/// Action state
	bool _running;
	/// Action state
	bool _rotatingLeft;
	/// Action state
	bool _rotatingRight;


	/** Default constructor */
	CltMainPlayerManipulator();
	/** Destructor */
	~CltMainPlayerManipulator();

	/** Manipulate transformation (position and so on) according with the
	 * current state (called every frame, to update it if needed). */
	void updateTransform(double elapsedSeconds);

	/** Manipulate model according with the current state (called every
	 * frame, to update it if needed). */
	void update3DModel(double elapsedSeconds);

	/** Send information to the server (done internally in predefined
	 * periods). */
	void sendStateToServer();
};


NAMESPACE_END(AmbarMetta);
#endif
