/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_GUIEVENT_H__
#define __AMBARMETTA_CLIENT_GUIEVENT_H__


/** \file cltguievent.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * The purpose of this file is to contain code enabling different parts of the
 * system to create events (propagating them from server, or printing a user
 * message, in example), and the GUI (whatever it is) will listen for them.
 *
 * Ideally this would provide an decoupling of the GUI from the rest of the
 * application.
 */


#include "common/net/msgs.hxx"
#include "common/patterns/observer.hxx"
#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"

#include <vector>


NAMESPACE_START(AmbarMetta);


/** ObserverEvent for GUI (generic)
 *
 * The purpose is to have different parts of the system creating events
 * (propagating them from server, or printing a user message, in example), and
 * the GUI (whatever it is) will listen for them.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEvent : public ObserverEvent
{
public:
	/** Action Identifier enumerator */
	enum class ActionId : int {
		DisplayNotifMsg = 1,
		DisplayConsoleMsg,
		ConnOK,
		ConnFailed,
		LoginOK,
		LoginFailed,
		NewUserOK,
		NewUserFailed,
		JoinOK,
		JoinFailed,
		NewCharOK,
		NewCharFailed,
		DelCharOK,
		DelCharFailed,
		ContactStatus,
		EntityCreate,
		EntityMove,
		EntityDestroy,
		TimeUpdate,
		WindowToggleVisible,
		MinimapZoomIn,
		MinimapZoomOut
	};

	/// Action Identifier
	const ActionId _actionId;

protected:
	/** Constructor (protected so cannot be built directly) */
	CltGuiEvent(ActionId actionId) :
		ObserverEvent("CltGuiEvent"), _actionId(actionId) { }
};


/** ObserverEvent for displaying notification messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventDisplayNotifMsg : public CltGuiEvent
{
public:
	/// Payload
	std::string _msg;

	/** Constructor */
	CltGuiEventDisplayNotifMsg(const std::string& msg) :
		CltGuiEvent(CltGuiEvent::ActionId::DisplayNotifMsg),
		_msg(msg)
		{ }
};


/** ObserverEvent for displaying GUI console messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventDisplayConsoleMsg : public CltGuiEvent
{
public:
	/// Payload
	MsgChat _msg;

	/** Constructor */
	CltGuiEventDisplayConsoleMsg(const MsgChat& msg) :
		CltGuiEvent(CltGuiEvent::ActionId::DisplayConsoleMsg),
		_msg(msg)
		{ }
};


/** ObserverEvent for connection OK messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventConnOK : public CltGuiEvent
{
public:
	/// Payload
	std::string _uptime;
	/// Payload
	int _currentPlayers;
	/// Payload
	int _totalUsers;
	/// Payload
	int _totalChars;

	/** Constructor */
	CltGuiEventConnOK(const std::string& uptime,
			  int currentPlayers,
			  int totalUsers,
			  int totalChars) :
		CltGuiEvent(CltGuiEvent::ActionId::ConnOK),
		_uptime(uptime),
		_currentPlayers(currentPlayers),
		_totalUsers(totalUsers),
		_totalChars(totalChars)
		{ }
};


/** ObserverEvent for connection failed messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventConnFailed : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventConnFailed() :
		CltGuiEvent(CltGuiEvent::ActionId::ConnFailed)
		{ }
};


/** ObserverEvent for login OK messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventLoginOK : public CltGuiEvent
{
public:
	/// Payload
	std::vector<CharacterListEntry> _charList;

	/** Constructor */
	CltGuiEventLoginOK(const std::vector<CharacterListEntry>& charList) :
		CltGuiEvent(CltGuiEvent::ActionId::LoginOK),
		_charList(charList)
		{ }
};


/** ObserverEvent for login failed messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventLoginFailed : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventLoginFailed() :
		CltGuiEvent(CltGuiEvent::ActionId::LoginFailed)
		{ }
};


/** ObserverEvent for new user OK messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventNewUserOK : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventNewUserOK() :
		CltGuiEvent(CltGuiEvent::ActionId::NewUserOK)
		{ }
};


/** ObserverEvent for new user failed messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventNewUserFailed : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventNewUserFailed() :
		CltGuiEvent(CltGuiEvent::ActionId::NewUserFailed)
		{ }
};


/** ObserverEvent for join OK messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventJoinOK : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventJoinOK() :
		CltGuiEvent(CltGuiEvent::ActionId::JoinOK)
		{ }
};


/** ObserverEvent for join failed messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventJoinFailed : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventJoinFailed() :
		CltGuiEvent(CltGuiEvent::ActionId::JoinFailed)
		{ }
};


/** ObserverEvent for new char OK messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventNewCharOK : public CltGuiEvent
{
public:
	/// Payload
	CharacterListEntry _character;

	/** Constructor */
	CltGuiEventNewCharOK(const CharacterListEntry& character) :
		CltGuiEvent(CltGuiEvent::ActionId::NewCharOK),
		_character(character)
		{ }
};


/** ObserverEvent for new char failed messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventNewCharFailed : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventNewCharFailed() :
		CltGuiEvent(CltGuiEvent::ActionId::NewCharFailed)
		{ }
};


/** ObserverEvent for del char OK messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventDelCharOK : public CltGuiEvent
{
public:
	/// Payload
	std::string _characterName;

	/** Constructor */
	CltGuiEventDelCharOK(const std::string& characterName) :
		CltGuiEvent(CltGuiEvent::ActionId::DelCharOK),
		_characterName(characterName)
		{ }
};


/** ObserverEvent for del char failed messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventDelCharFailed : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventDelCharFailed() :
		CltGuiEvent(CltGuiEvent::ActionId::DelCharFailed)
		{ }
};


/** ObserverEvent for contact status messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventContactStatus : public CltGuiEvent
{
public:
	/// Payload
	MsgContactStatus _msg;

	/** Constructor */
	CltGuiEventContactStatus(const MsgContactStatus& msg) :
		CltGuiEvent(CltGuiEvent::ActionId::ContactStatus),
		_msg(msg)
		{ }
};


/** ObserverEvent for entity create messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventEntityCreate : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventEntityCreate() :
		CltGuiEvent(CltGuiEvent::ActionId::EntityCreate)
		{ }
};


/** ObserverEvent for entity move messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventEntityMove : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventEntityMove() :
		CltGuiEvent(CltGuiEvent::ActionId::EntityMove)
		{ }
};


/** ObserverEvent for entity destroy messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventEntityDestroy : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventEntityDestroy() :
		CltGuiEvent(CltGuiEvent::ActionId::EntityDestroy)
		{ }
};


/** ObserverEvent for time update messages
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventTimeUpdate : public CltGuiEvent
{
public:
	/// Payload
	uint32_t _gametime;
	/// Payload
        int _year;
	/// Payload
        int _season;
	/// Payload
        int _moon;
	/// Payload
        int _day;
	/// Payload
        int _hour;
	/// Payload
        int _minute;

	/** Constructor */
	CltGuiEventTimeUpdate(uint32_t gametime,
			      int year, int season, int moon, int day,
			      int hour, int minute) :
		CltGuiEvent(CltGuiEvent::ActionId::TimeUpdate),
		_gametime(gametime),
		_year(year), _season(season), _moon(moon), _day(day),
		_hour(hour), _minute(minute)
		{ }
};


/** ObserverEvent for toggling window visibility
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventWindowToggleVisible : public CltGuiEvent
{
public:
	/** Enum for window identifiers */
	enum class WindowId : int {
		Inventory = 1
	};

	/// Payload
	WindowId _windowId;

	/** Constructor */
	CltGuiEventWindowToggleVisible(WindowId windowId) :
		CltGuiEvent(CltGuiEvent::ActionId::WindowToggleVisible),
		_windowId(windowId)
		{ }
};


/** ObserverEvent for zooming minimap in
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventMinimapZoomIn : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventMinimapZoomIn() :
		CltGuiEvent(CltGuiEvent::ActionId::MinimapZoomIn)
		{ }
};


/** ObserverEvent for zooming minimap out
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventMinimapZoomOut : public CltGuiEvent
{
public:
	/** Constructor */
	CltGuiEventMinimapZoomOut() :
		CltGuiEvent(CltGuiEvent::ActionId::MinimapZoomOut)
		{ }
};


/** The GUI event manager is just a way to have an Observable (this class) to
 * emit events (notification messages, propagating events from server, etc) to
 * be catched by the GUI.
 *
 * With this intermediate step, and the GUI depending on this class and not
 * otherwise, we're more shielded against depending on GUI too heavily.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiEventMgr : public Singleton<CltGuiEventMgr>, public Observable
{
public:
	/** Broadcast the given event
	 *
	 * @param event The event.
	 */
	static void broadcast(const CltGuiEvent& event);

private:
	/** Singleton friend access */
	friend class Singleton<CltGuiEventMgr>;


	/** Default constructor */
	CltGuiEventMgr();
	/** Destructor */
	~CltGuiEventMgr();
};


NAMESPACE_END(AmbarMetta);
#endif
