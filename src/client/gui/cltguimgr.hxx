/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_GUI_MGR_H__
#define __AMBARMETTA_CLIENT_GUI_MGR_H__


/** \file cltguimgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * This file contains the master class(es) governing the GUI.
 */


#include "common/net/msgs.hxx"
#include "common/patterns/observer.hxx"
#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"

#include <vector>


NAMESPACE_START(AmbarMetta);


/** The GUI manager is the interface manager, properly setting up the GUI system
 * and controlling or acting as proxy for other GUI-related classes.
 *
 * It's also an observer of GUI events, so hopefully the rest of the client can
 * be GUI-agnostic (so it's easier to switch the GUI system, a worthy feature in
 * the fragile and ever-moving-target of OpenGL GUI systems).
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltGuiMgr : public Singleton<CltGuiMgr>, public Observer
{
public:
	/** @see Observer::updateFromObservable() */
	virtual void updateFromObservable(const ObserverEvent& event);

private:
	/** Singleton friend access */
	friend class Singleton<CltGuiMgr>;


	/** Default constructor */
	CltGuiMgr();
	/** Destructor */
	~CltGuiMgr();
};


NAMESPACE_END(AmbarMetta);
#endif
