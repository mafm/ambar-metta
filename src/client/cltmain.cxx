/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltmain.hxx"

#include "client/net/cltnetmgr.hxx"
#include "client/cltviewer.hxx"

#include "common/configmgr.hxx"

#include <osg/ArgumentParser>

#include <cstdlib>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltMain
 ******************************************************************************/

void CltMain::parseArguments(int argc, char* argv[])
{
	osg::ArgumentParser arguments(&argc, argv);
	int argIndex = 0;

	// --help
	if ((argIndex = arguments.find("-h")) > 0
	    || (argIndex = arguments.find("--help")) > 0) {
		arguments.remove(argIndex);
		printf("Options for Ambar-metta Client:\n");
		printf("  -v, --version:	Show program version and exit\n");
		printf("  -h, --help:		Show program help and exit\n");
		// mafm: example of more options
		//printf("\n");
		//printf("  --no-content-update:	Disables automatic content update\n");
		exit(EXIT_SUCCESS);
	}

	// --version
	if ((argIndex = arguments.find("-v")) > 0
	    || (argIndex = arguments.find("--version")) > 0) {
		arguments.remove(argIndex);
		printf(BANNER_STRING);
		exit(EXIT_SUCCESS);
	}

	// options left unread are converted into errors
	arguments.reportRemainingOptionsAsUnrecognized();
	if (arguments.errors()) {
		osg::ArgumentParser::ErrorMessageMap errors = arguments.getErrorMessageMap();
		for (auto it = errors.begin(); it != errors.end(); ++it) {
			logERROR("Argument parsing: %s", it->first.c_str());
		}
		exit(EXIT_FAILURE);
	}
}

void CltMain::start()
{
	printf(BANNER_STRING);
	printf("\n");
	logINFO("Ambar-metta client starting...");

	// loading config
	bool configLoaded = ConfigMgr::instance().loadConfigFile(CLIENT_CONFIG_FILE);
	if (!configLoaded) {
		logERROR("Couldn't load config file: '%s'", CLIENT_CONFIG_FILE);
		exit(EXIT_FAILURE);
	}

	// starting viewer
	CltViewer::instance().setup();

	// starting the render loop
	CltViewer::instance().start();
}

void CltMain::quit()
{
	logINFO("Ambar-metta client stopping...");
	CltNetworkMgr::instance().disconnect();
	CltViewer::instance().stop();
	logINFO("Ambar-metta client shut down.");
	exit(EXIT_SUCCESS);
}


NAMESPACE_END(AmbarMetta);


/*******************************************************************************
 * Main function call
 ******************************************************************************/
int main(int argc, char* argv[])
{
	AmbarMetta::CltMain::parseArguments(argc, argv);
	AmbarMetta::CltMain::start();

	return 0;
}
