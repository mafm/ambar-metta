/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltcamera.hxx"

#include "client/cltviewer.hxx"

#include "common/math.hxx"

#include <osg/MatrixTransform>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltCameraManipulator
 ******************************************************************************/
const char* CltCameraManipulator::className() const
{
	return "AmbarMettaCameraManipulator";
}

bool CltCameraManipulator::handle(const osgGA::GUIEventAdapter& ea,
				  osgGA::GUIActionAdapter& /* aa */)
{
	switch (ea.getEventType()) {
	case (osgGA::GUIEventAdapter::FRAME):
	{
		static double lastTime = 0.0;
		if (lastTime != 0.0)
			CltCameraMgr::instance().getActiveCameraMode().updateCamera(&_matrix, ea.time() - lastTime);
		lastTime = ea.time();
		return false;
	}
	default:
		return false;
	}

	return false;
}

void CltCameraManipulator::setByMatrix(const osg::Matrixd& matrix)
{
	_matrix = matrix;
}

void CltCameraManipulator::setByInverseMatrix(const osg::Matrixd& matrix)
{
	_matrix = _matrix.inverse(matrix);
}

osg::Matrixd CltCameraManipulator::getMatrix() const
{
	return _matrix;
}

osg::Matrixd CltCameraManipulator::getInverseMatrix() const
{
	return _matrix.inverse(_matrix);
}


/*******************************************************************************
 * CltCameraMode
 ******************************************************************************/
const osg::Vec3 CltCameraMode::DEFAULT_GROUND_TO_EYE(0.0f, 0.0f, 1.60f);

CltCameraMode::CltCameraMode() :
	_name("Base"),
	_zoomingIn(false), _zoomingOut(false),
	_lookingUp(false), _lookingDown(false),
	_lookingLeft(false), _lookingRight(false),
	_requestToCenter(false),
	_targetTransform(0)
{
}

const char* CltCameraMode::getName() const
{
	return _name.c_str();
}

void CltCameraMode::setTargetTransform(const osg::MatrixTransform* targetTransform)
{
	_targetTransform = targetTransform;
}

void CltCameraMode::setZoomingIn(bool b)
{
	_zoomingIn = b;
}

void CltCameraMode::setZoomingOut(bool b)
{
	_zoomingOut = b;
}

void CltCameraMode::setLookingUp(bool b)
{
	_lookingUp = b;
}

void CltCameraMode::setLookingDown(bool b)
{
	_lookingDown = b;
}

void CltCameraMode::setLookingLeft(bool b)
{
	_lookingLeft = b;
}

void CltCameraMode::setLookingRight(bool b)
{
	_lookingRight = b;
}

void CltCameraMode::setRequestToCenter(bool b)
{
	_requestToCenter = b;
}


/*******************************************************************************
 * CltCameraModeFollow
 ******************************************************************************/
const float CltCameraModeFollow::ZOOM_SPEED = 12.0f; // 12m/s
const float CltCameraModeFollow::ZOOM_MAX_DISTANCE = -24.0f; // 24m behind
const float CltCameraModeFollow::ZOOM_MIN_DISTANCE = 0.5f; // 0.5m before, to bypass the mesh
const float CltCameraModeFollow::LOOK_SPEED = PI_NUMBER/2.0f; // 4s for full revolution
const float CltCameraModeFollow::LOOK_MAX_ANGLE = PI_NUMBER/2.0f; // PI/2
const float CltCameraModeFollow::DEFAULT_HORIZONTAL_ROT = 0.0f; // degrees
const float CltCameraModeFollow::DEFAULT_VERTICAL_ROT = 0.0f; // degrees
const float CltCameraModeFollow::DEFAULT_ZOOM_DISTANCE = ZOOM_MAX_DISTANCE/3.0f; // m

CltCameraModeFollow::CltCameraModeFollow() :
	_zoomDistance(0.0f), _horizRot(0.0f), _vertRot(0.0f)
{
	_name = "Follow";
	reset();
}

void CltCameraModeFollow::reset()
{
	_zoomDistance = DEFAULT_ZOOM_DISTANCE;
	_horizRot = DEFAULT_HORIZONTAL_ROT;
	_vertRot = DEFAULT_VERTICAL_ROT;
}

void CltCameraModeFollow::updateCamera(osg::Matrix* cameraView, double elapsedSeconds)
{
	// whether we're already in the game with a node assigned
	if (!_targetTransform)
		return;

	// react to events
	if (_requestToCenter) {
		// reset when requested
		reset();
		_requestToCenter = false;
	} else {
		// apply displacement due to zoom
		if (_zoomingIn) {
			_zoomDistance += ZOOM_SPEED * elapsedSeconds;
			// zoom distance limit to the position of the player
			// (Less because < 0)
			ensureMaximum(_zoomDistance, ZOOM_MIN_DISTANCE);
		}
		if (_zoomingOut) {
			_zoomDistance -= ZOOM_SPEED * elapsedSeconds;
			// distance limit (Greater because < 0)
			ensureMinimum(_zoomDistance, ZOOM_MAX_DISTANCE);
		}

		// apply "loot-at" rotations, only when we have 1st p. view
		if (_zoomDistance == ZOOM_MIN_DISTANCE) {
			// treating only full zoom, ~= 1st person mode
			if (_lookingUp) {
				_vertRot += LOOK_SPEED * elapsedSeconds;
				ensureMaximum(_vertRot, LOOK_MAX_ANGLE);
			}
			if (_lookingDown) {
				_vertRot -= LOOK_SPEED * elapsedSeconds;
				ensureMinimum(_vertRot, -LOOK_MAX_ANGLE);
			}

			if (_lookingLeft) {
				_horizRot += LOOK_SPEED * elapsedSeconds;
				ensureMaximum(_horizRot, LOOK_MAX_ANGLE);
			}
			if (_lookingRight) {
				_horizRot -= LOOK_SPEED * elapsedSeconds;
				ensureMinimum(_horizRot, -LOOK_MAX_ANGLE);
			}
		} else {
			// reset when we're not in 1st person view
			_horizRot = DEFAULT_HORIZONTAL_ROT;
			_vertRot = DEFAULT_VERTICAL_ROT;
		}
	}

	// basic position
	osg::Matrix targetMatrix = osg::Matrix::translate(DEFAULT_GROUND_TO_EYE)
		* _targetTransform->getMatrix();

	// look matrix
	osg::Matrix lookMatrix = osg::Matrix::rotate(_vertRot, osg::Vec3(1, 0, 0))
		* osg::Matrix::rotate(_horizRot, osg::Vec3(0, 0, 1));

	// zoom distance-away
	osg::Matrix finalMatrix = osg::Matrix::translate(0, _zoomDistance, 0)
		* (lookMatrix * targetMatrix);

	// set the camera view
	osg::Matrix lookAt;
	if (_zoomDistance != ZOOM_MIN_DISTANCE) {
		// clipping -- only when reasonably far away
		if (_zoomDistance < -0.5f) {
			osg::Matrix clipSourceMatrix = osg::Matrix::translate(DEFAULT_GROUND_TO_EYE + osg::Vec3(0, -1, 0))
				* (targetMatrix);
			osg::Vec3 clipFinal;
			CltViewer::instance().cameraClipping(clipSourceMatrix.getTrans(),
							     finalMatrix.getTrans(),
							     clipFinal);
			finalMatrix.setTrans(clipFinal);
		}

		lookAt.makeLookAt(finalMatrix.getTrans(),
				  targetMatrix.getTrans(),
				  osg::Z_AXIS);
	} else {
		// with full zoom (= 1st person view), override to not cause
		// artifacts
		osg::Matrix fakeTargetMatrix = osg::Matrix::translate(osg::Y_AXIS)
			* finalMatrix;
		lookAt.makeLookAt(finalMatrix.getTrans(),
				  fakeTargetMatrix.getTrans(),
				  osg::Z_AXIS);
	}
	cameraView->set(osg::Matrix::inverse(lookAt));
}


/*******************************************************************************
 * CltCameraModeOrbital
 ******************************************************************************/
const float CltCameraModeOrbital::RADIUS_SPEED = 12.0f; // 12m/s
const float CltCameraModeOrbital::RADIUS_MAX_DISTANCE = 24.0f; // 24m
const float CltCameraModeOrbital::RADIUS_MIN_DISTANCE = 2.0f; // 2m
const float CltCameraModeOrbital::ROTATION_SPEED = PI_NUMBER/2.0f; // 4s for full revolution
const float CltCameraModeOrbital::DEFAULT_HORIZONTAL_ROT = 0.0f; // degrees
const float CltCameraModeOrbital::DEFAULT_VERTICAL_ROT = -PI_NUMBER/24.0f; // degrees
const float CltCameraModeOrbital::DEFAULT_RADIUS_DISTANCE = RADIUS_MAX_DISTANCE/3.0f; // m

CltCameraModeOrbital::CltCameraModeOrbital() :
	_radiusDistance(0.0f), _horizRot(0.0f), _vertRot(0.0f)
{
	_name = "Orbital";
	reset();
}

void CltCameraModeOrbital::reset()
{
	_radiusDistance = DEFAULT_RADIUS_DISTANCE;
	_horizRot = DEFAULT_HORIZONTAL_ROT;
	_vertRot = DEFAULT_VERTICAL_ROT;
}

void CltCameraModeOrbital::updateCamera(osg::Matrix* cameraView, double elapsedSeconds)
{
	// mafm: getting the node to follow some point in the orbit and so on
	// isn't difficult, but at the time to implement this I couldn't get an
	// easy way to rotate the camera towards the player, so the player is
	// always in the center of the image.
	//
	// This alternative implementation seems to be simpler and more
	// efficient: we just rotate in any direction (like the look-at of the
	// main camera mode, but without any restriction), and in the end we
	// "push" the camera back the given radius.  It seems to work flawlessly
	// :)

	// whether we're already in the game with a node assigned
	if (!_targetTransform)
		return;

	// react to events
	if (_requestToCenter) {
		// reset when requested
		reset();
		_requestToCenter = false;
	} else {
		// radius/zoom distance
		if (_zoomingIn) {
			_radiusDistance -= RADIUS_SPEED * elapsedSeconds;
			ensureMinimum(_radiusDistance, RADIUS_MIN_DISTANCE);
		}
		if (_zoomingOut) {
			_radiusDistance += RADIUS_SPEED * elapsedSeconds;
			ensureMaximum(_radiusDistance, RADIUS_MAX_DISTANCE);
		}

		// orbit rotations
		if (_lookingUp) {
			_vertRot += ROTATION_SPEED * elapsedSeconds;
		}
		if (_lookingDown) {
			_vertRot -= ROTATION_SPEED * elapsedSeconds;
		}
		if (_lookingLeft) {
			_horizRot += ROTATION_SPEED * elapsedSeconds;
		}
		if (_lookingRight) {
			_horizRot -= ROTATION_SPEED * elapsedSeconds;
		}
	}

	// basic position
	osg::Matrix targetMatrix = osg::Matrix::translate(DEFAULT_GROUND_TO_EYE)
		* _targetTransform->getMatrix();

	// orbit matrix
	osg::Matrix lookMatrix = osg::Matrix::rotate(_vertRot, osg::Vec3(1, 0, 0))
		* osg::Matrix::rotate(_horizRot, osg::Vec3(0, 0, 1));

	// "pushing" the camera the given radius away
	osg::Vec3f radiusDispl = osg::Vec3(0, -_radiusDistance, 0);
	osg::Matrix finalMatrix = osg::Matrix::translate(radiusDispl)
		* (lookMatrix * targetMatrix);

	// clipping
	osg::Matrix clipSourceMatrix = osg::Matrix::translate(radiusDispl/_radiusDistance)
		* (lookMatrix * targetMatrix);
	osg::Vec3 clipFinal;
	CltViewer::instance().cameraClipping(clipSourceMatrix.getTrans(),
					     finalMatrix.getTrans(),
					     clipFinal);
	finalMatrix.setTrans(clipFinal);

	// set the camera view
	osg::Matrix lookAt;
	lookAt.makeLookAt(finalMatrix.getTrans(),
			  targetMatrix.getTrans(),
			  osg::Z_AXIS);
	cameraView->set(osg::Matrix::inverse(lookAt));
}


/*******************************************************************************
 * CltCameraMgr
 ******************************************************************************/
template <> CltCameraMgr* Singleton<CltCameraMgr>::INSTANCE = 0;

CltCameraMgr::CltCameraMgr()
{
	// create camera modes that we'll use (default is the first one, it will
	// get in the front of the list)
	_cameraModeList.push_back(new CltCameraModeFollow());
	_cameraModeList.push_back(new CltCameraModeOrbital());
}

CltCameraMgr::~CltCameraMgr()
{
	while (!_cameraModeList.empty()) {
		delete _cameraModeList.front();
		_cameraModeList.pop_front();
	}
}

CltCameraMode& CltCameraMgr::getActiveCameraMode()
{
	return *(_cameraModeList.front());
}

void CltCameraMgr::setTargetTransform(const osg::MatrixTransform* targetTransform)
{
	for (auto it = _cameraModeList.begin(); it != _cameraModeList.end(); ++it) {
		(*it)->setTargetTransform(targetTransform);
	}
}

void CltCameraMgr::cycleCameraMode()
{
	// cycle mode
	_cameraModeList.push_back(_cameraModeList.front());
	_cameraModeList.pop_front();

	// notify to the interested classes
	notifyListenersCameraModeChanged(_cameraModeList.front());
}

void CltCameraMgr::addListener(CltCameraListener* listener)
{
	// adding element, not checking for duplicates
	_listenerList.push_back(listener);
}

void CltCameraMgr::removeListener(CltCameraListener* listener)
{
	// removing element, including duplicates
	_listenerList.remove(listener);
}

void CltCameraMgr::notifyListenersCameraModeChanged(const CltCameraMode* mode)
{
	for (auto it = _listenerList.begin(); it != _listenerList.end(); ++it) {
		(*it)->cameraModeChanged(mode);
	}
}


NAMESPACE_END(AmbarMetta);
