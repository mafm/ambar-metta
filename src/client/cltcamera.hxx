/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_CAMERA_H__
#define __AMBARMETTA_CLIENT_CAMERA_H__


/** \file cltcamera.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * This file contains the cameras that we use in the game.
 */


#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"

#include <osgGA/MatrixManipulator>
#include <osg/Vec3>

#include <deque>
#include <list>


NAMESPACE_START(AmbarMetta);


/** Event handler controlling the camera, linking between our application and
 * the engine.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltCameraManipulator : public osgGA::MatrixManipulator
{
public:
	/** Return the name, to be identified */
	virtual const char* className() const;

	/** @see osgGA::MatrixManipulator::handle() */
	virtual bool handle(const osgGA::GUIEventAdapter& ea,
			    osgGA::GUIActionAdapter& aa);

	/** @see osgGA::MatrixManipulator::setByMatrix() */
	virtual void setByMatrix(const osg::Matrixd& matrix);
	/** @see osgGA::MatrixManipulator::setByInverseMatrix() */
	virtual void setByInverseMatrix(const osg::Matrixd& matrix);

	/** @see osgGA::MatrixManipulator::getByMatrix() */
	virtual osg::Matrixd getMatrix() const;
	/** @see osgGA::MatrixManipulator::getByInverseMatrix() */
	virtual osg::Matrixd getInverseMatrix() const;

protected:
	/// The matrix to manipulate the camera
	osg::Matrix _matrix;
};


/** Camera mode, implementing the actions asked to the camera and behaving
 * different according to its own nature.
 *
 * This is an abstract class that it's implemented by several modes.  We keep
 * common functionality implemented here, like the node that all cameras follow
 * (they will calculate the offset and looking-at with the target node as bae).
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltCameraMode : public CltCameraManipulator
{
public:
	/** Default constructor */
	CltCameraMode();
	/** Destructor */
	virtual ~CltCameraMode() { }

	/** Get the name */
	const char* getName() const;

	/** Set flag for this camera action */
	void setZoomingIn(bool b);
	/** Set flag for this camera action */
	void setZoomingOut(bool b);
	/** Set flag for this camera action */
	void setLookingUp(bool b);
	/** Set flag for this camera action */
	void setLookingDown(bool b);
	/** Set flag for this camera action */
	void setLookingLeft(bool b);
	/** Set flag for this camera action */
	void setLookingRight(bool b);
	/** Set flag for this camera action */
	void setRequestToCenter(bool b);

	/** Reset to home position. */
	virtual void reset() = 0;
	/** It's called every frame via camera manager, with the milliseconds
	 * elapsed since last update, so we move the camera of the engine
	 * (depending on camera mode and commands issued by player), and thus
	 * control how do we view the scene.  This is where each camera mode
	 * behaves different.
	 *
	 * @param cameraView Camera View, to allow to modify it
	 * @param elapsedSeconds Time elapsed since last update
	 */
	virtual void updateCamera(osg::Matrix* cameraView, double elapsedSeconds) = 0;

protected:
	/// Default
	static const osg::Vec3 DEFAULT_GROUND_TO_EYE;

	/// Name of the mode
	std::string _name;

	/// Flag for camera action
	bool _zoomingIn;
	/// Flag for camera action
	bool _zoomingOut;
	/// Flag for camera action
	bool _lookingUp;
	/// Flag for camera action
	bool _lookingDown;
	/// Flag for camera action
	bool _lookingLeft;
	/// Flag for camera action
	bool _lookingRight;
	/// Flag for camera action
	bool _requestToCenter;

	/// The transformation of the node that we should follow
	const osg::MatrixTransform* _targetTransform;

private:
	/** Friend access to the camera manager, so it can set some functions
	 * for all the cameras that shouldn't be globally accessable */
	friend class CltCameraMgr;


	/** Set the transformation of the node that the cameras should follow
	 * (typically the player).
	 *
	 * \note This function is private with the camera manager accessing as
	 * friend, so the external code sets the target globally and not for
	 * each camera mode.
	 *
	 *
	 * @param targetTransform The transformation representation of the
	 * target to follow
	 */
	void setTargetTransform(const osg::MatrixTransform* targetTransform);
};


/** Follow camera mode.  The behavior of this camera mode is that zooming in
 * leads to first person view (where is possible to look up, down, left and
 * right), and zoom out leads to a distant point above and behind the player.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltCameraModeFollow : public CltCameraMode
{
public:
	/** Default constructor */
	CltCameraModeFollow();

	/** @see CltCameraMode */
	virtual void reset();
	/** @see CltCameraMode */
	virtual void updateCamera(osg::Matrix* cameraView, double elapsedSeconds);

private:
	/// Speed of the zoom in/out
	static const float ZOOM_SPEED;
	/// Maximum zoom distance
	static const float ZOOM_MAX_DISTANCE;
	/// Minimum zoom distance
	static const float ZOOM_MIN_DISTANCE;
	/// Speed of the "look" rotation
	static const float LOOK_SPEED;
	/// Maximum angle of the rotation (left-right, up-down)
	static const float LOOK_MAX_ANGLE;
	/// Default
	static const float DEFAULT_HORIZONTAL_ROT;
	/// Default
	static const float DEFAULT_VERTICAL_ROT;
	/// Default
	static const float DEFAULT_ZOOM_DISTANCE;

	/// Zoom distance
	float _zoomDistance;
	/// Horizontal rotation
	float _horizRot;
	/// Vertical rotation
	float _vertRot;
};


/** Orbital camera mode.  The behavior of this camera mode is that it orbits the
 * player, having it at the center, with zoom to control the radius and the keys
 * to look up/down/left/right controlling movement from "pole to pole" and
 * "ecuator".  This mode fulfills special needs that the other mode can't cover,
 * so you can turn around to see your character from any angle.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltCameraModeOrbital : public CltCameraMode
{
public:
	/** Default constructor */
	CltCameraModeOrbital();

	/** @see CltCameraMode */
	virtual void reset();
	/** @see CltCameraMode */
	virtual void updateCamera(osg::Matrix* cameraView, double elapsedSeconds);

private:
	/// Speed of the zoom in/out (to set the radius)
	static const float RADIUS_SPEED;
	/// Maximum radius distance
	static const float RADIUS_MAX_DISTANCE;
	/// Minimum radius distance
	static const float RADIUS_MIN_DISTANCE;
	/// Speed of the rotation
	static const float ROTATION_SPEED;
	/// Default
	static const float DEFAULT_HORIZONTAL_ROT;
	/// Default
	static const float DEFAULT_VERTICAL_ROT;
	/// Default
	static const float DEFAULT_RADIUS_DISTANCE;

	/// Radius distance
	float _radiusDistance;
	/// Horizontal rotation
	float _horizRot;
	/// Vertical rotation
	float _vertRot;
};


/** Listener for the event of changing the active camera.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltCameraListener
{
public:
	/** Notification that the camera mode changed
	 *
	 * @param newMode The new mode
	 */
	virtual void cameraModeChanged(const CltCameraMode* newMode) = 0;

	/** Destructor */
	virtual ~CltCameraListener() { }
};


/** Governs the camera: modes being used, etc.  The main loop of the client has
 * to call this function every frame before rendering, so the manager passes
 * down the info to the active camera mode, which in turn tells the engine
 * camera to render the scene (with the parameters given by the camera mode).
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltCameraMgr : public Singleton<CltCameraMgr>
{
public:
	/** Set the transformation of the node that the cameras should follow
	 * (typically the player)
	 *
	 * @param targetTransform The transformation representation of the
	 * target to follow
	 */
	void setTargetTransform(const osg::MatrixTransform* targetTransform);

	/** Get the active camera mode */
	CltCameraMode& getActiveCameraMode();
	/** Cycle the camera mode to the next one */
	void cycleCameraMode();

	/** Add a listener for events */
	void addListener(CltCameraListener* listener);
	/** Remove a listener */
	void removeListener(CltCameraListener* listener);

private:
	/** Singleton friend access */
	friend class Singleton<CltCameraMgr>;


	/// List of camera modes that we can use (front is the active one)
	std::deque<CltCameraMode*> _cameraModeList;

	/// List of listeners subscribed to our events
	std::list<CltCameraListener*> _listenerList;


	/** Default constructor */
	CltCameraMgr();
	/** Destructor */
	~CltCameraMgr();

	/** Notify the listeners that the camera mode changed
	 *
	 * @param newMode The new mode
	 */
	void notifyListenersCameraModeChanged(const CltCameraMode* mode);
};


NAMESPACE_END(AmbarMetta);
#endif
