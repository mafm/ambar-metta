/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_VIEWER_H__
#define __AMBARMETTA_CLIENT_VIEWER_H__


/** \file cltviewer.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * Classes for the viewer, i.e., the main part of the 3D client application.
 */


#include "common/entities.hxx"
#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"

#include <osg/BoundingBox>
#include <osg/Vec3>


namespace osg {
	class Group;
	class LightModel;
	class LightSource;
	class Node;
	class ShapeDrawable;
}
namespace osgViewer {
	class Viewer;
}
namespace osgGA {
	class MatrixManipulator;
}
namespace osgParticle {
	class PrecipitationEffect;
}


NAMESPACE_START(AmbarMetta);


class CltEntityBase;


/** Client 3D application.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltViewer : public Singleton<CltViewer>
{
public:
	/** Get window dimension (or fullscreen resolution). */
	int getWindowWidth() const;
	/** Get window dimension (or fullscreen resolution). */
	int getWindowHeight() const;

	/** Get terrain height.
	 *
	 * @param position The position (height will not be considered)
	 *
	 * @return The height at that position
	 */
	float getTerrainHeight(const osg::Vec3& position) const;

	/** Collision detection, simple fashion.
	 *
	 * This takes the source and the destination intended, and gives the
	 * resulting final position, including setting the position on the
	 * ground, which is what the function to move entities should use.
	 *
	 * \remark This solution is simplistic, since it doesn't take into
	 * account collision detection when the object is not positioned
	 * on the ground (fences, in example). */
	void applyCollisionAndTerrainHeight(CltEntityBase* entity,
					    const osg::Vec3& source,
					    const osg::Vec3& dest,
					    osg::Vec3& finalPos);

	/** Clip the camera to a suitable position.
	 *
	 * The viewer casts a ray into the scene, with the given endpoints,
	 * source being the center of the camera, such as player head,
	 * destination where the camera should be in the case that there's
	 * nothing in the middle.
	 *
	 * The third parameter is used to return the final position, which is
	 * either the destination (in the case that there's nothing in the
	 * middle), or a position just before the object in the middle and on
	 * the ground, which is what the camera mode should use. */
	void cameraClipping(const osg::Vec3& source,
			    const osg::Vec3& dest,
			    osg::Vec3& finalPos);

	/** Set up the initial window with our desired data. */
	void setup();
	/** Set full screen. */
	void setFullScreen(bool b);
	/** Whether we're in full screen mode. */
	bool isFullScreen() const;
	/** Load the scene.  It should be called when receiving the 'create main
	 * player' message, so we load the area and set up the main player
	 * related classes. */
	void loadScene(const std::string& area);
	/** Add node to the scene. */
	void addToScene(const osg::Node* node);
	/** Remove a node from the scene. */
	void removeFromScene(const osg::Node* node);
	/** Start rendering, calls the renderLoop. */
	void start();
	/** Stop rendering, the window quits. */
	void stop();
	/** Change the environment according with the time of the day. */
	void setEnvironment(uint32_t timeOfDay);
	/** Select something in the environment */
	EntityID::value_type pick(float x, float y) const;

private:
	/** Singleton friend access */
	friend class Singleton<CltViewer>;


	/// The engine viewer
	osgViewer::Viewer* _viewer;

	/// The scene, root as group of nodes
	osg::Group* _scene;

	/// The camera manipulator
	osgGA::MatrixManipulator* _cameraManipulator;

	/// Terrain node, to be able to find the height of it
	osg::Node* _terrainNode;

	/// Sky dome
	osg::ShapeDrawable* _skyDome;
	/// Sun lightsource
	osg::LightSource* _sun;

	/// Precipitations
	osgParticle::PrecipitationEffect* _precipitation;

	/// Light model of the scene
	osg::LightModel* _lightModel;

	/// Window dimension/resolution
	unsigned int _windowWidth;
	/// Window dimension/resolution
	unsigned int _windowHeight;

	/// Screen (hardware) dimension/resolution
	unsigned int _screenWidth;
	/// Screen (hardware) dimension/resolution
	unsigned int _screenHeight;


	/** Default constructor */
	CltViewer();
	/** Destructor */
	~CltViewer();


	/** The render loop. */
	void renderLoop();

	/** Create a sun light.
	 *
	 * @param position Create the sun at this position
	 *
	 * @return Return the node of the sun
	 */
	osg::Node* createSun(const osg::Vec3& position);

	/** Change the lights according with the time of the day.
	 *
	 * @param timeOfDay Variable representing the time of the day
	 */
	void setLights(uint32_t timeOfDay);
};


NAMESPACE_END(AmbarMetta);
#endif
