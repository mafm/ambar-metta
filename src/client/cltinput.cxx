/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltinput.hxx"

#include "client/cltcamera.hxx"
#include "client/cltguievent.hxx"
#include "client/cltviewer.hxx"
#include "client/entity/cltentitymgr.hxx"
#include "client/entity/cltentitymainplayer.hxx"
#include "client/net/cltnetmgr.hxx"

#include "common/math.hxx"
#include "common/net/msgs.hxx"

#include <osg/Matrix>
#include <osg/MatrixTransform>
#include <osg/Vec3>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltInputHandler
 ******************************************************************************/
CltInputHandler::CltInputHandler() :
	_className("InputEventHandler")
{
}

CltInputHandler::~CltInputHandler()
{
}

const char* CltInputHandler::className() const
{
	return _className.c_str();
}

bool CltInputHandler::handle(const osgGA::GUIEventAdapter& ea,
			     osgGA::GUIActionAdapter& aa,
			     osg::Object* /* o */,
			     osg::NodeVisitor* /* nv */)
{
	return handle(ea, aa);
}

bool CltInputHandler::handle(const osgGA::GUIEventAdapter& ea,
			     osgGA::GUIActionAdapter& aa)
{
	switch (ea.getEventType()) {
	case (osgGA::GUIEventAdapter::KEYDOWN) :
	{
		if (ea.getKey() == osgGA::GUIEventAdapter::KEY_F1) {
			// toggle fullscreen
			CltViewer::instance().setFullScreen(!CltViewer::instance().isFullScreen());
			return true;
		} else if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Up || 
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Up) {
			// start moving forward
			CltMainPlayerManipulator::instance().setMovingForward(true);
			return true;
		} else if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Down || 
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Down) {
			// start moving backward
			CltMainPlayerManipulator::instance().setMovingBackward(true);
			return true;
		} else if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Left || 
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Left) {
			// start rotating left
			CltMainPlayerManipulator::instance().setRotatingLeft(true);
			return true;
		} else if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Right || 
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Right) {
			// start rotating right
			CltMainPlayerManipulator::instance().setRotatingRight(true);
			return true;
		} else if (ea.getKey() ==  osgGA::GUIEventAdapter::KEY_Shift_L
			   || ea.getKey() == osgGA::GUIEventAdapter::KEY_Shift_R) {
			// set running mode
			CltMainPlayerManipulator::instance().setRunning(true);
			return true;
		} else if (ea.getKey() == 'R' || ea.getKey() == 'r') {
			// autorunning (toggle, nothing when unpressed)
			CltMainPlayerManipulator::instance().toggleAutoRunning();
			return true;
		} else if (ea.getKey() == 'C' || ea.getKey() == 'c') {
			// cycle camera mode
			CltCameraMgr::instance().cycleCameraMode();
			return true;
		} else if (ea.getKey() == 'E' || ea.getKey() == 'e') {
			// instruct camera to be centered
			CltCameraMgr::instance().getActiveCameraMode().setRequestToCenter(true);
			return true;
		} else if (ea.getKey() == 'Z' || ea.getKey() == 'z') {
			// start zooming in
			CltCameraMgr::instance().getActiveCameraMode().setZoomingIn(true);
			return true;
		} else if (ea.getKey() == 'Q' || ea.getKey() == 'q') {
			// start zooming out
			CltCameraMgr::instance().getActiveCameraMode().setZoomingOut(true);
			return true;
		} else if (ea.getKey() == 'W' || ea.getKey() == 'w') {
			// start looking up
			CltCameraMgr::instance().getActiveCameraMode().setLookingUp(true);
			return true;
		} else if (ea.getKey() == 'S' || ea.getKey() == 's') {
			// start looking down
			CltCameraMgr::instance().getActiveCameraMode().setLookingDown(true);
			return true;
		} else if (ea.getKey() == 'A' || ea.getKey() == 'a') {
			// start looking left
			CltCameraMgr::instance().getActiveCameraMode().setLookingLeft(true);
			return true;
		} else if (ea.getKey() == 'D' || ea.getKey() == 'd') {
			// start looking right
			CltCameraMgr::instance().getActiveCameraMode().setLookingRight(true);
			return true;
		} else if (ea.getKey() == 'I' || ea.getKey() == 'i') {
			// inventory window (toggle)
			CltGuiEventMgr::broadcast(CltGuiEventWindowToggleVisible(CltGuiEventWindowToggleVisible::WindowId::Inventory));
			return true;
		} else if (ea.getKey() == '+' ||
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Add) {
			// minimap zoom in
			CltGuiEventMgr::broadcast(CltGuiEventMinimapZoomIn());
			return true;
		} else if (ea.getKey() == '-' ||
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Subtract) {
			// minimap zoom out
			CltGuiEventMgr::broadcast(CltGuiEventMinimapZoomOut());
			return true;
		} else {
			return false;
		}
		break;
	}
	case (osgGA::GUIEventAdapter::KEYUP) :
	{
		if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Up || 
		    ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Up) {
			// stop moving forward
			CltMainPlayerManipulator::instance().setMovingForward(false);
			return true;
		} else if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Down || 
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Down) {
			// stop moving backward
			CltMainPlayerManipulator::instance().setMovingBackward(false);
			return true;
		} else if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Left || 
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Left) {
			// stop rotating left
			CltMainPlayerManipulator::instance().setRotatingLeft(false);
			return true;
		} else if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Right || 
			   ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Right) {
			// stop rotating right
			CltMainPlayerManipulator::instance().setRotatingRight(false);
			return true;
		} else if (ea.getKey() ==  osgGA::GUIEventAdapter::KEY_Shift_L
			   || ea.getKey() == osgGA::GUIEventAdapter::KEY_Shift_R) {
			// set running mode
			CltMainPlayerManipulator::instance().setRunning(false);
			return true;
		} else if (ea.getKey() == 'Z' || ea.getKey() == 'z') {
			// stop zooming in
			CltCameraMgr::instance().getActiveCameraMode().setZoomingIn(false);
			return true;
		} else if (ea.getKey() == 'Q' || ea.getKey() == 'q') {
			// stop zooming out
			CltCameraMgr::instance().getActiveCameraMode().setZoomingOut(false);
			return true;
		} else if (ea.getKey() == 'W' || ea.getKey() == 'w') {
			// stop looking up
			CltCameraMgr::instance().getActiveCameraMode().setLookingUp(false);
			return true;
		} else if (ea.getKey() == 'S' || ea.getKey() == 's') {
			// stop looking down
			CltCameraMgr::instance().getActiveCameraMode().setLookingDown(false);
			return true;
		} else if (ea.getKey() == 'A' || ea.getKey() == 'a') {
			// stop looking left
			CltCameraMgr::instance().getActiveCameraMode().setLookingLeft(false);
			return true;
		} else if (ea.getKey() == 'D' || ea.getKey() == 'd') {
			// stop looking right
			CltCameraMgr::instance().getActiveCameraMode().setLookingRight(false);
			return true;
		} else {
			return false;
		}
		break;
	}
	case (osgGA::GUIEventAdapter::PUSH):
	{
		if (ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON) {
			logDEBUG("Left mouse button clicked");
			/// \todo: mafm: picking
			//CltViewer::instance().pick(ea.getX(), ea.getY());
			return true;
		}
		break;
	}
	case (osgGA::GUIEventAdapter::RELEASE):
	{
		if (ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON) {
			logDEBUG("Left mouse button released");
			return true;
		} else if (ea.getButton() == osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON) {
			logDEBUG("Right mouse button released");
			/// \todo: mafm: mesh selection, showing up action menus
			//CltCEGUIActionMenu::instance().ShowAt(ea.getXnormalized(), ea.getYnormalized() );
			return true;
		}
		break;
	}
	case (osgGA::GUIEventAdapter::FRAME):
	{
		static double lastTime = 0.0;

		if (lastTime != 0.0) {
			double elapsedSeconds = ea.time() - lastTime;

			// main player movement
			CltEntityMainPlayer* mainPlayer = &CltEntityMainPlayer::instance();
			if (mainPlayer) {
				CltMainPlayerManipulator::instance().tick(elapsedSeconds);
			} else {
				// for the pings in the connection screen.  we
				// have to proccess it immediately to avoid
				// spurious delays (reply arrived but we don't
				// know the timestamp difference because it's
				// unprocessed).

				/// \todo mafm: reimplement
				//CltCEGUIInitial::instance().Connect_ProcessPingReplies(ea.time());

				// to send pings, it will send pings every 3
				// seconds.

				/// \todo mafm: reimplement
				/*
				static double lastPing = -10.0;
				if (ea.time() > lastPing+3.0) {
					lastPing = ea.time();

					CltCEGUIInitial::instance().Connect_SendPings(lastPing);
				}
				*/
			}

			// updating other entities
			CltEntityMgr::instance().updateTransforms(elapsedSeconds);

			// incoming network messages
			CltNetworkMgr::instance().processIncomingMsgs();
		}
		lastTime = ea.time();
		return false;
	}
	default:
		return false;
	}
	return false;
}


/*******************************************************************************
 * CltMainPlayerManipulator
 ******************************************************************************/
const float CltMainPlayerManipulator::WALK_SPEED = 2.0f; // 2m/s = 7.2km/h
const float CltMainPlayerManipulator::RUN_SPEED = 5.0f; // 5m/s = 18km/h
const float CltMainPlayerManipulator::ROTATE_SPEED = (2*PI_NUMBER)/4.0f; // in rad/s, 4s for full revolution

template <> CltMainPlayerManipulator* Singleton<CltMainPlayerManipulator>::INSTANCE = 0;

CltMainPlayerManipulator::CltMainPlayerManipulator()
{
	_movingForward = false;
	_movingBackward = false;
	_running = false;
	_rotatingLeft = false;
	_rotatingRight = false;
	_playerTransform = new osg::MatrixTransform();
}

CltMainPlayerManipulator::~CltMainPlayerManipulator()
{
}

void CltMainPlayerManipulator::setPosition(const osg::Vec3& position, float rotation)
{
	// mafm: translation is absolute, not depending on the rotation, so it's
	// set in different order than the rest of similar situations in this
	// class
	float terrainHeight = CltViewer::instance().getTerrainHeight(position);
	osg::Vec3 p(position.x(), position.y(), terrainHeight);
	osg::Matrix matrix = osg::Matrix::rotate(rotation, osg::Vec3(0, 0, 1))
		* osg::Matrix::translate(p);
	_playerTransform->setMatrix(matrix);

	logINFO("Position of the main player changed to: (%.1f, %.1f, %.1f), rot=%.1f",
		p.x(), p.y(), p.z(), rotation);
}

void CltMainPlayerManipulator::tick(double elapsedSeconds)
{
	// updating position and so on
	updateTransform(elapsedSeconds);

	// updating 3D model states
	update3DModel(elapsedSeconds);

	// updating every 5s if not moving
	static double elapsedSinceServerUpdate = 0.0;
	elapsedSinceServerUpdate += elapsedSeconds;
	if (elapsedSinceServerUpdate > 5.0) {
		sendStateToServer();
		elapsedSinceServerUpdate = 0.0;
	// updating every 0.5s if it's moving, to reduce innacuracies
	} else if (elapsedSinceServerUpdate > 0.5 && !isIdle()) {
		sendStateToServer();
		elapsedSinceServerUpdate = 0.0;
	}
}

void CltMainPlayerManipulator::updateTransform(double elapsedSeconds)
{
	// mafm: To calculate the new position of the player, we start by
	// calculating the displacement from the current position.  To achieve
	// this, we calculate the speed (walking or running), then whether is
	// moving forward or backward (y=1 and y=-1 respectively), and applying
	// the rotation; all of this related to the time elapsed.
	//
	// Once we get this, we have to apply the modification over the previous
	// position and look-at "attitude".

	if (isIdle()) {
		return;
	}

	// basic speed (not used if not moving)
	float linearSpeed = WALK_SPEED * elapsedSeconds;
	if (_running && _movingForward)
		linearSpeed = RUN_SPEED * elapsedSeconds;

	// rotation (affecting final displacement vector, and rotation of the 3D
	// model)
	float rotation = 0.0f;
	if (_rotatingLeft) {
		rotation = ROTATE_SPEED * elapsedSeconds;
	} else if (_rotatingRight) {
		rotation = ROTATE_SPEED * elapsedSeconds * (-1);
	}

	// vector of linear displacement (when walking forward, backwards is the
	// opposite), not counting rotation
	osg::Vec3 linearDisplacement(0, 0, 0);
	if (_movingForward) {
		// rotation influence in linear movement:
		// - x is the sin(rotation)*y, y=1*linearSpeed
		// - y is the cos(rotation)*y, y=1*linearSpeed
		// - z axis not affected
		linearDisplacement[0] = sin(rotation) * linearSpeed;
		linearDisplacement[1] = cos(rotation) * linearSpeed;
	} else if (_movingBackward) {
		// rotation is inverted, to result how player expects
		rotation = -rotation;

		// rotation influence in linear movement:
		// - x is the sin(rotation)*y, y=-1*linearSpeed
		// - y is the cos(rotation)*y, y=-1*linearSpeed
		// - z axis not affected
		linearDisplacement[0] = sin(rotation) * linearSpeed * (-1);
		linearDisplacement[1] = cos(rotation) * linearSpeed * (-1);
	}

	// do collision det and terrain height, only when moving and not
	// rotating (saves from warning messages about invalid ray segments of
	// length zero)
	if (linearDisplacement != osg::Vec3(0, 0, 0)) {
		osg::Vec3 source = _playerTransform->getMatrix().getTrans();
		// make a matrix for the new position (with rotation around Z
		// axis)
		osg::Matrix destMatrix = osg::Matrix::translate(linearDisplacement)
			* osg::Matrix::rotate(rotation, osg::Vec3(0, 0, 1))
			* _playerTransform->getMatrix();
		osg::Vec3 dest = destMatrix.getTrans();
		osg::Vec3 finalPos;
		CltViewer::instance().applyCollisionAndTerrainHeight(&CltEntityMainPlayer::instance(), source, dest, finalPos);
		destMatrix.setTrans(finalPos);

		// finally, set the new position
		_playerTransform->setMatrix(destMatrix);
	} else if (rotation != 0.0) {
		// only rotation
		osg::Matrix destMatrix = osg::Matrix::rotate(rotation, osg::Vec3(0, 0, 1))
			* _playerTransform->getMatrix();
		_playerTransform->setMatrix(destMatrix);
	}
}

void CltMainPlayerManipulator::update3DModel(double elapsedSeconds)
{
	// position + linear movement
	MsgEntityMove msg;

	osg::Vec3 trans = _playerTransform->getMatrix().getTrans();
	msg.position.x = trans.x();
	msg.position.y = trans.y();
	msg.position.z = trans.z();

	/// \todo mafm: is this correct?
	osg::Vec3f rotation;
	double rotation_angle;
	_playerTransform->getMatrix().getRotate().getRotate(rotation_angle, rotation);
	msg.rot = rotation.z() * rotation_angle;

	// action
	if (_movingForward) {
		msg.mov_fwd = true;
		msg.mov_bwd = false;
	} else if (_movingBackward) {
		msg.mov_fwd = false;
		msg.mov_bwd = true;
	} else {
		msg.mov_fwd = false;
		msg.mov_bwd = false;
	}

	msg.run = _running && _movingForward;

	if (_rotatingLeft) {
		msg.rot_left = true;
		msg.rot_right = false;
	} else if (_rotatingRight) {
		msg.rot_left = false;
		msg.rot_right = true;
	} else {
		msg.rot_left = false;
		msg.rot_right = false;
	}

	// update the model with the data gathered
	CltEntityMainPlayer::instance().setMovementProperties(&msg);
}

void CltMainPlayerManipulator::sendStateToServer()
{
	// copying to avoid const qualifier
	MsgEntityMove msg = *CltEntityMainPlayer::instance().getMovementProperties();
	CltNetworkMgr::instance().sendToServer(msg);
}

void CltMainPlayerManipulator::setMovingForward(bool b)
{
	_movingForward = b;
}

void CltMainPlayerManipulator::setMovingBackward(bool b)
{
	_movingBackward = b;
}

void CltMainPlayerManipulator::setRunning(bool b)
{
	_running = b;
}

void CltMainPlayerManipulator::toggleAutoRunning()
{
	if (_running) {
		_running = false;
		_movingForward = false;
	} else {
		_running = true;
		_movingForward = true;
	}
}

void CltMainPlayerManipulator::setRotatingRight(bool b)
{
	_rotatingRight = b;
}

void CltMainPlayerManipulator::setRotatingLeft(bool b)
{
	_rotatingLeft = b;
}

bool CltMainPlayerManipulator::isIdle() const
{
	return ! (_movingForward
		  || _movingBackward
		  || _rotatingLeft
		  || _rotatingRight);
}

osg::MatrixTransform* CltMainPlayerManipulator::getMatrixTransform() const
{
	return _playerTransform;
}


NAMESPACE_END(AmbarMetta);
