/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltnetmgr.hxx"

#include "client/cltguievent.hxx"
#include "client/net/cltmsghdls.hxx"

#include "common/net/msgs.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltNetworkMgr
 ******************************************************************************/
template <> CltNetworkMgr* Singleton<CltNetworkMgr>::INSTANCE = 0;

CltNetworkMgr::CltNetworkMgr() :
	_socketLayer(&_netlink)
{
	registerMsgHdls();
}

CltNetworkMgr::~CltNetworkMgr()
{
	_socketLayer.disconnect();
}

void CltNetworkMgr::registerMsgHdls()
{
#define	REGHDL(msgname) _msgHdlFactory.registerMsgWithHdl(new Msg ##msgname, new CltMsgHdl ##msgname)

	// test messages
	REGHDL(TestDataTypes);

	// connection messages
	REGHDL(ConnectReply);
	REGHDL(LoginReply);
	REGHDL(NewUserReply);
	REGHDL(NewCharReply);
	REGHDL(DelCharReply);
	REGHDL(JoinReply);

	// chat
	REGHDL(Chat);

	// contact list
	REGHDL(ContactStatus);

	// entities
	REGHDL(EntityCreate);
	REGHDL(EntityMove);
	REGHDL(EntityDestroy);

	// inventory
	REGHDL(InventoryListing);
	REGHDL(InventoryAdd);
	REGHDL(InventoryDel);

	// player data
	REGHDL(PlayerData);

	// time messages
	REGHDL(TimeMinute);

#undef REGHDL
}

bool CltNetworkMgr::isConnected() const
{
	return _socketLayer.isConnected();
}

bool CltNetworkMgr::connectToServer(const char* host, int port)
{
	if (_socketLayer.isConnected()) {
		logWARNING("Already connected, refusing to connect again");
		return true;
	} else {
		return _socketLayer.connectToServer(host, port);
	}
}

void CltNetworkMgr::disconnect()
{
	_socketLayer.disconnect();
}

void CltNetworkMgr::sendToServer(MsgBase& msg)
{
	if (!_socketLayer.isConnected() || !_netlink.sendMsg(msg)) {
		MsgChat consoleMsg;
		consoleMsg.origin = "Client";
		consoleMsg.type = MsgChat::SYSTEM;
		consoleMsg.text = "The connection was dropped, message could not be sent";
		CltGuiEventMgr::broadcast(CltGuiEventDisplayConsoleMsg(consoleMsg));

		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(consoleMsg.text));
		logERROR(consoleMsg.text);

		return;
	}
}

void CltNetworkMgr::processIncomingMsgs()
{
	if (!_socketLayer.isConnected()) {
		// we just return without printing anything because otherwise it
		// floods the client log until we get connected for the first
		// time
		return;
	}

	// it will try to send queued messages
	_netlink.processOutgoingMsgs();

	// this processes the messages alone internally, we just need to worry
	// if the server disconnects
	bool result = _netlink.processIncomingMsgs(_msgHdlFactory);
	if (!result) {
		MsgChat consoleMsg;
		consoleMsg.origin = "Client";
		consoleMsg.type = MsgChat::SYSTEM;
		consoleMsg.text = "Connection to server lost";
		CltGuiEventMgr::broadcast(CltGuiEventDisplayConsoleMsg(consoleMsg));

		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(consoleMsg.text));
		logERROR(consoleMsg.text);

		disconnect();
	}
}


NAMESPACE_END(AmbarMetta);
