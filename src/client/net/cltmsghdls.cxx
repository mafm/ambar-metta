/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltmsghdls.hxx"

#include "client/cltdatamodel.hxx"
#include "client/cltguievent.hxx"
#include "client/cltviewer.hxx"
#include "client/entity/cltentitymgr.hxx"
#include "client/entity/cltentitymainplayer.hxx"

#include "common/configmgr.hxx"
#include "common/net/netlayer.hxx"

#include <osg/Matrix>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * Test
 ******************************************************************************/

void CltMsgHdlTestDataTypes::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgTestDataTypes* msg = dynamic_cast<MsgTestDataTypes*>(&baseMsg);

	// don't do anything, the data gets printed automatically when
	// deserializing
}


/*******************************************************************************
 * Connections
 ******************************************************************************/

void CltMsgHdlConnectReply::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgConnectReply* msg = dynamic_cast<MsgConnectReply*>(&baseMsg);

	std::string clientVersion = ConfigMgr::instance().getConfigVar("Client.ProtocolVersion", "");
	if (msg->resultCode == MsgUtils::Errors::SUCCESS
	    && msg->protocolVersion != clientVersion) {
		std::string errmsg = strFmt("Protocol versions of client (%s) and server (%s) differ",
				       clientVersion.c_str(), msg->protocolVersion.c_str());
		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(errmsg));
		logERROR(errmsg);
	} else if (msg->resultCode == MsgUtils::Errors::SUCCESS) {
		CltGuiEventConnOK event(msg->uptime,
					msg->currentPlayers,
					msg->totalUsers,
					msg->totalChars);
		CltGuiEventMgr::broadcast(event);
	} else {
		std::string errmsg = strFmt("Failed to connect: %s",
				       MsgUtils::Errors::getDescription(msg->resultCode));
		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(errmsg));
		logERROR(errmsg);
	}
}


void CltMsgHdlLoginReply::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgLoginReply* msg = dynamic_cast<MsgLoginReply*>(&baseMsg);

	if (msg->resultCode == MsgUtils::Errors::SUCCESS) {
		// login->join
		CltGuiEventLoginOK event(msg->charList);
		CltGuiEventMgr::broadcast(event);
	} else {
		// login failed!
		CltGuiEventLoginFailed event;
		CltGuiEventMgr::broadcast(event);
		std::string errmsg = strFmt("Failed login: %s",
					    MsgUtils::Errors::getDescription(msg->resultCode));
		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(errmsg));
		logERROR(errmsg);
	}
}


void CltMsgHdlJoinReply::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgJoinReply* msg = dynamic_cast<MsgJoinReply*>(&baseMsg);

	if (msg->resultCode == MsgUtils::Errors::SUCCESS) {
		// start playing
		CltGuiEventJoinOK event;
		CltGuiEventMgr::broadcast(event);

		MsgChat msgReply;
		msgReply.origin = "Client";
		msgReply.type = MsgChat::SYSTEM;
		msgReply.text = "You are now playing in the world";
		CltGuiEventMgr::broadcast(CltGuiEventDisplayConsoleMsg(msgReply));
	} else {
		// joining the world failed!
		CltGuiEventJoinFailed event;
		CltGuiEventMgr::broadcast(event);
		std::string errmsg = strFmt("Failed to join: %s",
					    MsgUtils::Errors::getDescription(msg->resultCode));
		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(errmsg));
		logERROR(errmsg);
	}
}


void CltMsgHdlNewUserReply::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgNewUserReply* msg = dynamic_cast<MsgNewUserReply*>(&baseMsg);

	if (msg->resultCode == MsgUtils::Errors::SUCCESS) {
		// new user -> login
		CltGuiEventNewUserOK event;
		CltGuiEventMgr::broadcast(event);
	} else {
		// new user failed!
		CltGuiEventNewUserFailed event;
		CltGuiEventMgr::broadcast(event);
		std::string errmsg = strFmt("Failed to create new user: %s",
					    MsgUtils::Errors::getDescription(msg->resultCode));
		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(errmsg));
		logERROR(errmsg);
	}
}


void CltMsgHdlNewCharReply::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgNewCharReply* msg = dynamic_cast<MsgNewCharReply*>(&baseMsg);

	if (msg->resultCode == MsgUtils::Errors::SUCCESS) {
		// new character -> join (adding new character)
		CltGuiEventNewCharOK event(msg->character);
		CltGuiEventMgr::broadcast(event);
	} else {
		// new character failed!
		CltGuiEventNewCharFailed event;
		CltGuiEventMgr::broadcast(event);
		std::string errmsg = strFmt("Failed to create new char: %s",
					    MsgUtils::Errors::getDescription(msg->resultCode));
		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(errmsg));
		logERROR(errmsg);
	}
}


void CltMsgHdlDelCharReply::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgDelCharReply* msg = dynamic_cast<MsgDelCharReply*>(&baseMsg);

	if (msg->resultCode == MsgUtils::Errors::SUCCESS) {
		// delete character OK
		CltGuiEventDelCharOK event(msg->charname);
		CltGuiEventMgr::broadcast(event);
	} else {
		// delete character failed!
		CltGuiEventDelCharFailed event;
		CltGuiEventMgr::broadcast(event);
		std::string errmsg = strFmt("Failed to delete character: %s",
					    MsgUtils::Errors::getDescription(msg->resultCode));
		CltGuiEventMgr::broadcast(CltGuiEventDisplayNotifMsg(errmsg));
		logERROR(errmsg);
	}
}


/*******************************************************************************
 * Console
 ******************************************************************************/

void CltMsgHdlChat::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgChat& msg = dynamic_cast<MsgChat&>(baseMsg);

	CltGuiEventMgr::broadcast(CltGuiEventDisplayConsoleMsg(msg));
}


/*******************************************************************************
 * Contact
 ******************************************************************************/

void CltMsgHdlContactStatus::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgContactStatus* msg = dynamic_cast<MsgContactStatus*>(&baseMsg);

	CltGuiEventContactStatus event(*msg);
	CltGuiEventMgr::broadcast(event);
}


/*******************************************************************************
 * Entities
 ******************************************************************************/

void CltMsgHdlEntityCreate::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgEntityCreate* msg = dynamic_cast<MsgEntityCreate*>(&baseMsg);
	CltEntityMgr::instance().entityCreate(msg);
	CltGuiEventEntityCreate event;
	CltGuiEventMgr::broadcast(event);
}


void CltMsgHdlEntityMove::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgEntityMove* msg = dynamic_cast<MsgEntityMove*>(&baseMsg);
	CltEntityMgr::instance().entityMove(msg);
	CltGuiEventEntityMove event;
	CltGuiEventMgr::broadcast(event);
}


void CltMsgHdlEntityDestroy::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgEntityDestroy* msg = dynamic_cast<MsgEntityDestroy*>(&baseMsg);
	CltEntityMgr::instance().entityDestroy(msg);
	CltGuiEventEntityDestroy event;
	CltGuiEventMgr::broadcast(event);
}


/*******************************************************************************
 * Inventory
 ******************************************************************************/

void CltMsgHdlInventoryListing::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgInventoryListing* msg = dynamic_cast<MsgInventoryListing*>(&baseMsg);

	logDEBUG("InventoryListing received");

	for (size_t i = 0; i < msg->invListing.size(); ++i) {
		CltEntityMainPlayer::instance().addToInventory(msg->invListing[i]);
	}
}


void CltMsgHdlInventoryAdd::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgInventoryAdd* msg = dynamic_cast<MsgInventoryAdd*>(&baseMsg);

	logDEBUG("InventoryItemAdded received (itemID: %s)", msg->item.getItemIDAsStr());

	CltEntityMainPlayer::instance().addToInventory(msg->item);
}


void CltMsgHdlInventoryDel::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgInventoryDel* msg = dynamic_cast<MsgInventoryDel*>(&baseMsg);

	logDEBUG("InventoryDel received (itemID: %s)", EntityID::toString(msg->itemID).c_str());

	CltEntityMainPlayer::instance().removeFromInventory(msg->itemID);
}


/*******************************************************************************
 * Player data
 ******************************************************************************/

void CltMsgHdlPlayerData::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgPlayerData* msg = dynamic_cast<MsgPlayerData*>(&baseMsg);

	// player itself
	CltEntityMainPlayer::instance().setPlayerData(msg);

	/// \todo mafm: could/should update data model with in-game variables
	/// here, if they are to be used at all...

	// PlayerStats window
	//CltCEGUIMgr::instance().PlayerStats_Update(msg);
	// Inventory window -- mafm: load?
	//CltCEGUIInventory::instance().updateStats(msg);
}


/*******************************************************************************
 * Time
 ******************************************************************************/

void CltMsgHdlTimeMinute::handleMsg(MsgBase& baseMsg, Netlink* /* netlink */)
{
	MsgTimeMinute* msg = dynamic_cast<MsgTimeMinute*>(&baseMsg);

	// static values corresponding with in-game calendar
	static const int HOUR_DIVISOR		= 60;
	static const int DAY_DIVISOR		= 24 * HOUR_DIVISOR;
	static const int MOON_DIVISOR		= 23 * DAY_DIVISOR;
	static const int SEASON_DIVISOR		= 4  * MOON_DIVISOR;
	static const int YEAR_DIVISOR		= 4  * SEASON_DIVISOR;

	// converting time
	int numeric_uptime = msg->gametime;

	int year = numeric_uptime / YEAR_DIVISOR;
	int year_rest = numeric_uptime % YEAR_DIVISOR;
	int season = year_rest / SEASON_DIVISOR;
	int season_rest = year_rest % SEASON_DIVISOR;
	int moon = season_rest / MOON_DIVISOR;
	int moon_rest = season_rest % MOON_DIVISOR;
	int day = moon_rest / DAY_DIVISOR;
	int day_rest = moon_rest % DAY_DIVISOR;
	int hour = day_rest / HOUR_DIVISOR;
	int hour_rest = day_rest % HOUR_DIVISOR;
	int minute = hour_rest;

	/* mafm: uncomment this if you want to debug the time values
	logDEBUG("*-> %du %dy %ds %dm %02dd %02dh:%02dm",
	       numeric_uptime, year, season, moon, day, hour, minute);
	*/

	// update environment in the client viewer
	CltViewer::instance().setEnvironment(day_rest);

	CltGuiEventTimeUpdate event(msg->gametime, year, season, moon, day, hour, minute);
	CltGuiEventMgr::broadcast(event);

	/** \todo mafm: unused code

	// setting time in the calendar applet
	std::string prettyTime = strFmt("%02dh%02d", hour, minute);
	char prettySeason = '!';
	switch (season) {
	case 0: prettySeason = 'D'; break;
	case 1: prettySeason = 'E'; break;
	case 2: prettySeason = 'S'; break;
	case 3: prettySeason = 'R'; break;
	}
	std::string prettyMoon = strFmt("%dm", moon+1);
	std::string prettyDate = strFmt("%c %s %d", prettySeason, prettyMoon.c_str(), day);
	std::string prettyYear = strFmt("%d", year);
	CltCEGUIMgr::instance().Calendar_SetTime(prettyTime, prettyDate, prettyYear);

	// changing the moon in the date applet, if needed
	int moonPic = 15;
	switch (day)
	{
	case 0: case 1: moonPic = 1; break;
	case 2: case 3: moonPic = 2; break;
	case 4: moonPic = 3; break;
	case 5: case 6: moonPic = 4; break;
	case 7: case 8: case 9: moonPic = 5; break;
	case 10: moonPic = 6; break;
	case 11: case 12: case 13: moonPic = 7; break;
	case 14: case 15: moonPic = 8; break;
	case 16: moonPic = 9; break;
	case 17: case 18: moonPic = 10; break;
	case 19: case 20: case 21: moonPic = 11; break;
	case 22: moonPic = 0; break;
	}
	std::string moonPictureName = strFmt("Moon_%02d", moonPic);
	CltCEGUIMgr::instance().Calendar_SetMoonPicture(moonPictureName);

	*/
}


NAMESPACE_END(AmbarMetta);
