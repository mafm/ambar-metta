/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_NET_MGR_H__
#define __AMBARMETTA_CLIENT_NET_MGR_H__


/** \file cltnetmgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Network manager for the client.
 */


#include "common/net/netlayer.hxx"
#include "common/net/msgbase.hxx"
#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"


NAMESPACE_START(AmbarMetta);


/** Network manager for the client, abstracting all the operations.  The rest of
 * the application shouldn't worry about this, except for connecting and sending
 * messages, and very little else.
 *
 * This class is also responsible for calling the appropriate modules on the
 * client, when messages requiring client's attention are received.  The client
 * has to call this manager every frame or with some other frequency, so it can
 * process the incoming data.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltNetworkMgr : public Singleton<CltNetworkMgr>
{
public:
	/** Check whether we're connected to the server
	 *
	 * @return Whether we are connected to server.
	 */
	bool isConnected() const;

	/** Connect to server
	 *
	 * @param host Hostname of the server
	 * @param port Port of the server
	 *
	 * @return Whether the operation was successful or not
	 */
	bool connectToServer(const char* host, int port);

	/** Disconnect from server */
	void disconnect();

	/** Send a message to server
	 *
	 * @param msg The message to send
	 */
	void sendToServer(MsgBase& msg);

	/** Process incoming messages, called from the main app every frame or
	 * at least with some regularity */
	void processIncomingMsgs();

private:
	/** Singleton friend access */
	friend class Singleton<CltNetworkMgr>;


	/// Connection object (with data about us and the peer, the server)
	Netlink _netlink;

	/// Network layer
	SocketLayer _socketLayer;

	/// The message and handler factory
	MsgHdlFactory _msgHdlFactory;


	/** Default constructor */
	CltNetworkMgr();
	/** Destructor */
	~CltNetworkMgr();


	/** Register message handlers, called once to set it up */
	void registerMsgHdls();
};


NAMESPACE_END(AmbarMetta);
#endif
