/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_NET_MSGHDLS_H__
#define __AMBARMETTA_CLIENT_NET_MSGHDLS_H__


/** \file cltmsghdls.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Network message handlers for the client.
 */


#include "common/net/msgbase.hxx"
#include "common/net/msgs.hxx"

#include "client/cltconfig.hxx"


NAMESPACE_START(AmbarMetta);


// Define to simplify code and mistakes of these simple and monotonous classes
#define CLT_MSG_HDL(msgname) \
	class CltMsgHdl ##msgname : public MsgHdlBase				\
	{									\
	public:									\
		virtual MsgType getMsgType() const {				\
			return Msg ##msgname::_type;				\
		}								\
		virtual void handleMsg(MsgBase& msg, Netlink* netlink = 0);	\
	}


/*******************************************************************************
 * Test
 ******************************************************************************/
CLT_MSG_HDL(TestDataTypes);


/*******************************************************************************
 * Connections
 ******************************************************************************/

CLT_MSG_HDL(ConnectReply);
CLT_MSG_HDL(LoginReply);
CLT_MSG_HDL(JoinReply);
CLT_MSG_HDL(NewUserReply);
CLT_MSG_HDL(NewCharReply);
CLT_MSG_HDL(DelCharReply);


/*******************************************************************************
 * Console
 ******************************************************************************/

CLT_MSG_HDL(Chat);


/*******************************************************************************
 * Contact
 ******************************************************************************/

CLT_MSG_HDL(ContactStatus);


/*******************************************************************************
 * Entities
 ******************************************************************************/

CLT_MSG_HDL(EntityCreate);
CLT_MSG_HDL(EntityMove);
CLT_MSG_HDL(EntityDestroy);


/*******************************************************************************
 * Inventory
 ******************************************************************************/

CLT_MSG_HDL(InventoryListing);
CLT_MSG_HDL(InventoryAdd);
CLT_MSG_HDL(InventoryDel);


/*******************************************************************************
 * Player data
 ******************************************************************************/

CLT_MSG_HDL(PlayerData);


/*******************************************************************************
 * Time
 ******************************************************************************/

CLT_MSG_HDL(TimeMinute);


#undef CLT_MSG_HDL


NAMESPACE_END(AmbarMetta);
#endif
