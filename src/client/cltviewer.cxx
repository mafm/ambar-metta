/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltviewer.hxx"

#include "client/cltcamera.hxx"
#include "client/cltinput.hxx"
#include "client/content/cltcontentloader.hxx"
#include "client/entity/cltentitymgr.hxx"
#include "client/entity/cltentitymainplayer.hxx"
#include "client/net/cltnetmgr.hxx"

#include "common/configmgr.hxx"
#include "common/net/msgs.hxx"
#include "common/datatypes.hxx"
#include "common/math.hxx"

#include <osg/BlendFunc>
#include <osg/CullFace>
#include <osg/Depth>
#include <osg/Fog>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Material>
#include <osg/Math>
#include <osg/Matrix>
#include <osg/MatrixTransform>
#include <osg/PolygonMode>
#include <osg/PolygonOffset>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/LightSource>
#include <osg/LightModel>
#include <osg/TexGenNode>
#include <osg/Timer>
#include <osgUtil/IntersectVisitor>
#include <osgUtil/SmoothingVisitor>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgParticle/PrecipitationEffect>

#include <QtCore/QString>
#include <QtCore/QTimer>
#include <QtGui/QKeyEvent>
#include <QtGui/QApplication>
#include <QtOpenGL/QGLWidget>
#include <QtGui/QMainWindow>


NAMESPACE_START(AmbarMetta);



/** Helper class to make the render loop sleep when the FPS goes to high.  Huge
 * rendering speeds are of no benefit for the eye, while consuming energy and
 * slowing down CPU.  A limit of 60FPS (more than the double than TV update
 * frequencies) should be more than reasonable limitation.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class FPSLimitator
{
public:
	/** Constructor with target FPS limit as parameter */
	FPSLimitator(float fps) {
		mInterval = static_cast<uint32_t>(1000000.0f/fps);
	}

	/** Read the time when the frame begins (so the limitation takes the
	 * processing time into accoount) */
	void readTime() {
		mLastTick = osg::Timer::instance()->tick();
	}

	/** Sleep the necessary amount of time to effectively achieve the FPS
	 * limitation. */
	void sleep() {
		int32_t sleepFor = mInterval - (osg::Timer::instance()->tick() - mLastTick);
		if (sleepFor > 0) {
			usleep(sleepFor);
		}
		/*
		logDEBUG("FPS: %.1f (%.1f)",
			   1000000.0f/static_cast<float>(osg::Timer::instance()->tick() - mLastTick),
			   static_cast<float>(osg::Timer::instance()->tick() - mLastTick));
		*/
	}

private:
	/// The interval between frames (microseconds) for FPS limit
	uint32_t mInterval;
	/// Tick when the frame was started
	osg::Timer_t mLastTick;
};


/** Helper class to keep stats.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class StatsCounter
{
public:
	/** Constructor */
	StatsCounter() : _lastTick(0) {	}

	/** Get FPS since last count (should be called once every frame,
	 * otherwise is of no use). */
	double calculateFPS() {
		osg::Timer_t currentTick = osg::Timer::instance()->tick();
		double fps = 1.0/osg::Timer::instance()->delta_s(_lastTick, currentTick);
		_lastTick = currentTick;
		return fps;
	}

private:
	/// Tick when the frame was started
	osg::Timer_t _lastTick;
};


/** Shows a small red ball at some point that we want to visualize.
 *
 * It's useful for debugging, so it's commented out.  It was used to show when a
 * player is colliding with some object (to check that the math is right and we
 * didn't use wrong coordinates), and also it could be used as an example of
 * highlighting the point of the terrain that we clicked at ("Move to").
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>

class CollisionBall
{
public:
	CollisionBall() : sphere(0), radius(0.1f), color(1.0f, 0.0f, 0.0f, 0.0f), geode(0) { }

	osg::Geode* showAt(const osg::Vec3& pos) {
		sphere = new osg::ShapeDrawable(new osg::Sphere(pos, radius));
		sphere->setColor(color);

		geode = new osg::Geode();
		geode->setName("LastCollision");
		geode->addDrawable(sphere);
		geode->getOrCreateStateSet()->setMode(GL_LIGHTING,
						      osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);

		return geode;
	}

	/// Sphere to represent collision
	osg::ShapeDrawable* sphere;
	/// Property
	float radius;
	/// Property
	osg::Vec4 color;

	/// Geode
	osg::Geode* geode;
};
 */


/*******************************************************************************
 * CltViewer
 ******************************************************************************/
template <> CltViewer* Singleton<CltViewer>::INSTANCE = 0;

CltViewer::CltViewer() :
	_viewer(0),
	_scene(0),
	_cameraManipulator(0),
	_terrainNode(0),
	_skyDome(0),
	_sun(0),
	_precipitation(0),
	_windowWidth(0), _windowHeight(0),
	_screenWidth(0), _screenHeight(0)
{
}

CltViewer::~CltViewer()
{
	delete _viewer;
}

int CltViewer::getWindowWidth() const
{
	return _windowWidth;
}

int CltViewer::getWindowHeight() const
{
	return _windowHeight;
}

void CltViewer::setup()
{
	// check whether we already have the window set up
	if (_viewer != 0) {
		logWARNING("Refusing to set up the viewer more than once");
		return;
	}

	// get screen (hardware) resolution
	osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
	CHECK_FATAL(wsi);
	wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0), _screenWidth, _screenHeight);
	logINFO("Screen resolution: %ux%u", _screenWidth, _screenHeight);

	// read options from the config file
	_windowWidth = atoi(ConfigMgr::instance().getConfigVar("Client.Settings.Window.ScreenWidth", "0").c_str());
	_windowHeight = atoi(ConfigMgr::instance().getConfigVar("Client.Settings.Window.ScreenHeight", "0").c_str());
	if (_windowWidth == 0 || _windowHeight == 0) {
		logWARNING("Can't get the window dimensions from the config file");
		return;
	}
	bool fullscreen = false;
	if (std::string("yes") == ConfigMgr::instance().getConfigVar("Client.Settings.Window.FullScreen", "no")) {
		fullscreen = true;
	}

	// window characteristics
	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits();
	traits->x = 0;
	traits->y = 0;
	traits->width = _windowWidth;
	traits->height = _windowHeight;
	traits->windowDecoration = true;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;
	traits->useCursor = true;
	traits->windowName = "Ambar-metta Client";
	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	// set up viewer and camera
	_viewer = new osgViewer::Viewer();
	osg::Camera* camera = _viewer->getCamera();
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);

	// mafm: SingleThreaded at the moment, it seems faster than the default
	_viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);

	// mafm: see osgViewer::View documentation
	_viewer->addSlave(camera,
			  osg::Matrixd::translate(0, 0, 0),
			  osg::Matrixd()); 

	/// \note mafm: quick exit, mainly for devel purposes
	_viewer->setKeyEventSetsDone(osgGA::GUIEventAdapter::KEY_Escape);

	// camera manipulator
	_cameraManipulator = new CltCameraManipulator();
	_viewer->setCameraManipulator(_cameraManipulator);

	// set up the event handler.
	//
	// mafm: Note that it seems to be an issue with the way that this is
	// handled (only one seems to be active at a time), and we need a
	// fine-tuning for this (layer-like, GUI being the first to receive the
	// input events).  So it's this primary event handler who calls the
	// other ones in the correct order.
	_viewer->addEventHandler(new CltInputHandler());
	_viewer->addEventHandler(_cameraManipulator);

	// create the window, switch to fullscreen if apropriate
	_viewer->realize();
	CHECK_FATAL(_viewer->isRealized());
	setFullScreen(fullscreen);

	// create the scene
	_scene = new osg::Group();
	_scene->getOrCreateStateSet()->setMode(GL_LIGHTING,
					       osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);

	// sky dome
	{
		// variables
		float radius = 1024.0f;
		osg::Vec3 center(0.0f, 0.0f, 0.0f);
		osg::Vec4 color(0.0f, 0.0f, 0.0f, 1.0f);

		_skyDome = new osg::ShapeDrawable(new osg::Sphere(center, radius));
		_skyDome->setColor(color);
		osg::Geode* geode = new osg::Geode();
		geode->setName("SkyDome");
		geode->addDrawable(_skyDome);
		geode->getOrCreateStateSet()->setMode(GL_LIGHTING,
						      osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);
		_scene->addChild(geode);
	}

	// sun
	osg::Vec3 sunPosition(-1000, 100, 1000);
	_scene->addChild(createSun(sunPosition));

	// fog
	/** \todo mafm: doesn't work properly? */
	float fogEnd = 256.0f;
	{
		osg::Vec4 fogColor(0.8f, 0.8f, 0.8f, 1.0f);
		osg::Fog* fog = new osg::Fog();
		fog->setFogCoordinateSource(osg::Fog::FRAGMENT_DEPTH);
		fog->setMode(osg::Fog::LINEAR);
		fog->setStart(fogEnd/10.0f);
		fog->setEnd(fogEnd);
		fog->setDensity(0.01f);
		fog->setColor(fogColor);
		_scene->getOrCreateStateSet()->setAttributeAndModes(fog,
								    osg::StateAttribute::ON);
	}

	// precipitation
	{/** \todo mafm WIP

		_precipitation = new osgParticle::PrecipitationEffect();
		_precipitation->snow(0.0);
		_precipitation->rain(0.0);
		_precipitation->getFog()->setDensity(0.5);
		_precipitation->getFog()->setStart(50.0);
		_precipitation->getFog()->setEnd(500.0);
		osg::Vec4 color(1, 1, 1, 0.5);
		_precipitation->setNearTransition(1.0f);
		_precipitation->setFarTransition(50.0f);
		_precipitation->setParticleColor(color);
		_scene->addChild(_precipitation);

		logDEBUG("%g %g", _precipitation->getNearTransition(), _precipitation->getFarTransition());
	 */}

	// cull face
	{
		osg::PolygonMode* polygonMode = new osg::PolygonMode(osg::PolygonMode::FRONT, osg::PolygonMode::FILL);
		osg::CullFace* cullFace = new osg::CullFace(osg::CullFace::BACK);
		_scene->getOrCreateStateSet()->setAttributeAndModes(polygonMode,
								    osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
		_scene->getOrCreateStateSet()->setAttributeAndModes(cullFace,
								    osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	}

	// depth cull
	{
		osg::Depth* depth = new osg::Depth(osg::Depth::LESS, 0.0, fogEnd);
		_scene->getOrCreateStateSet()->setAttributeAndModes(depth,
								    osg::StateAttribute::ON);
	}

	// finally, set up the scene in the viewer
	_viewer->setSceneData(_scene);

	const std::string& clientMode = ConfigMgr::instance().getConfigVar("Client.Settings.Mode", "");
	if (clientMode == "local") {
		/* \todo mafm: code only useful for developers

		MsgNewUser msgNewUser;
		msgNewUser.username = "mafm";
		msgNewUser.pw_md5sum = "0e0e68cc27a6334256e0752d1243c4d894e56869";
		msgNewUser.email = "mafm@fearann.muin";
		msgNewUser.realname = "Manuel Montecelo";
		CltNetworkMgr::instance().sendToServer(msgNewUser);

		MsgNewChar msgNewChar;
		msgNewChar.charname = "Elfo";
		msgNewChar.race = "elf";
		msgNewChar.gender = "m";
		msgNewChar.playerClass = "sorcerer";
		msgNewChar.ab_choice_str = 13;
		msgNewChar.ab_choice_con = 13;
		msgNewChar.ab_choice_dex = 13;
		msgNewChar.ab_choice_int = 13;
		msgNewChar.ab_choice_wis = 13;
		msgNewChar.ab_choice_cha = 13;
		CltNetworkMgr::instance().sendToServer(msgNewChar);

		*/

		logINFO("Client mode: local (using local terrain and model)");
		MsgEntityCreate fakeMainPlayer;
		float posX = atof(ConfigMgr::instance().getConfigVar("Client.Settings.LocalMode.PositionX", "0.0").c_str());
		float posY = atof(ConfigMgr::instance().getConfigVar("Client.Settings.LocalMode.PositionY", "0.0").c_str());
		fakeMainPlayer.entityID = 0;
		fakeMainPlayer.position = Vector3(posX, posY, 0);
		fakeMainPlayer.rot = 0.0f;
		fakeMainPlayer.area = ConfigMgr::instance().getConfigVar("Client.Settings.LocalMode.Area", "");
		fakeMainPlayer.meshType = ConfigMgr::instance().getConfigVar("Client.Settings.LocalMode.PlayerModelType", "");
		fakeMainPlayer.meshSubtype = ConfigMgr::instance().getConfigVar("Client.Settings.LocalMode.PlayerModelSubtype", "");
		fakeMainPlayer.entityName = "Test Player";
		fakeMainPlayer.entityClass = "MainPlayer";
		CltEntityMgr::instance().entityCreate(&fakeMainPlayer);
	} else if (clientMode == "autologin") {
		logINFO("Client mode: autologin (no initial screens)");

		std::string serverName = ConfigMgr::instance().getConfigVar("Client.Settings.Login.Server", "");
		int serverPort = atoi(ConfigMgr::instance().getConfigVar("Client.Settings.Login.Port", "").c_str());
		bool connected = CltNetworkMgr::instance().connectToServer(serverName.c_str(),
									   serverPort);

		logDEBUG("Connected: %d", connected);

		MsgConnect msgConnect;
		CltNetworkMgr::instance().sendToServer(msgConnect);

		MsgLogin msgLogin;
		msgLogin.username = ConfigMgr::instance().getConfigVar("Client.Settings.Login.User", "");
		msgLogin.pw_md5sum = ConfigMgr::instance().getConfigVar("Client.Settings.Login.Password", "");
		CltNetworkMgr::instance().sendToServer(msgLogin);

		MsgJoin msgJoin;
		msgJoin.charname = ConfigMgr::instance().getConfigVar("Client.Settings.Login.Character", "");
		CltNetworkMgr::instance().sendToServer(msgJoin);
	} else {
		logINFO("Client mode: normal");

		/// \todo mafm: CEGUI disabled
		
		// add the GUI
		//CltCEGUIDrawable* ceguiDrawable = new CltCEGUIDrawable(_windowWidth, _windowHeight);
		//_scene->addChild(ceguiDrawable->getNode());
	}

	CHECK_FATAL(_viewer->isRealized());

	/// \todo mafm: CEGUI disabled

	//CltCEGUIDrawable* ceguiDrawable = new CltCEGUIDrawable(_windowWidth, _windowHeight);
	//_scene->addChild(ceguiDrawable->getNode());
}

void CltViewer::start()
{
	renderLoop();
}

void CltViewer::stop()
{
	_viewer->setDone(true);
}

void CltViewer::renderLoop()
{
	// simple limitator to make it sleep when we reach > 60FPS
	FPSLimitator fpsLim(60.0f);

	StatsCounter stats;

	// main loop
	while (!_viewer->done()) {
		// read the time when the frame begins
		fpsLim.readTime();

		// render a complete new frame
		_viewer->frame();

		// sleep for some time, to achieve limitation
		fpsLim.sleep();

		//logDEBUG("Stats (FPS): %0.3f", stats.calculateFPS());
	}
}

void CltViewer::setFullScreen(bool b)
{
	logINFO("Set fullscreen: %s", (b ? "true" : "false"));

	osgViewer::Viewer::Windows windows;
	_viewer->getWindows(windows);
	CHECK_FATAL(windows.size() > 0);

	if (b) {
		windows[0]->setWindowDecoration(false);
		windows[0]->setWindowRectangle(0, 0, _screenWidth, _screenHeight);
	} else {
		windows[0]->setWindowDecoration(true);
		windows[0]->setWindowRectangle(0, 0, _windowWidth, _windowHeight);
	}
}

bool CltViewer::isFullScreen() const
{
	osgViewer::Viewer::Windows windows;
	_viewer->getWindows(windows);
	CHECK_FATAL(windows.size() > 0);

	int x = 0;
	int y = 0;
	int currentWidth = 0;
	int currentHeight = 0;
	windows[0]->getWindowRectangle(x, y, currentWidth, currentHeight);

	return (currentWidth == static_cast<int>(_screenWidth))
		&& (currentHeight == static_cast<int>(_screenHeight))
		&& !(windows[0]->getWindowDecoration());
}

void CltViewer::addToScene(const osg::Node* node)
{
	_scene->addChild(const_cast<osg::Node*>(node));
}

void CltViewer::removeFromScene(const osg::Node* node)
{
	_scene->removeChild(const_cast<osg::Node*>(node));
}

void CltViewer::loadScene(const std::string& area)
{
	logINFO("Loading scene:");

	// load the terrain and buildings
	osg::Node* buildings = 0;
	CltContentLoader::instance().loadArea(area, &_terrainNode, &buildings);
	_terrainNode->getOrCreateStateSet()->setMode(GL_LIGHTING,
						     osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	_terrainNode->addDescription("terrain");
	_scene->addChild(_terrainNode);
	buildings->getOrCreateStateSet()->setMode(GL_LIGHTING,
						  osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	_scene->addChild(buildings);
	logINFO(" - Area '%s' loaded successfully", area.c_str());
}

osg::Node* CltViewer::createSun(const osg::Vec3& position)
{
	// refusing to create the sun more than once
	if (_sun)
		return 0;

	// create light source
	osg::Group* group = new osg::Group;
	_sun = new osg::LightSource();

	osg::Light* light = _sun->getLight();
	light->setLightNum(0);
	light->setPosition(osg::Vec4(position, 1.0f));
	light->setDiffuse(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));
	light->setSpecular(osg::Vec4(0.2f, 0.2f, 0.2f, 1.0f));
	light->setConstantAttenuation(0.0f);
	light->setLinearAttenuation(0.001f);
	light->setQuadraticAttenuation(0.0f);

	osg::Material* material = new osg::Material;
	material->setEmission(osg::Material::FRONT_AND_BACK, osg::Vec4(1.0f, 1.0f, 0.0f, 0.0f));
	_sun->getOrCreateStateSet()->setAttributeAndModes(material, osg::StateAttribute::ON);

	_lightModel = new osg::LightModel();
	_sun->getOrCreateStateSet()->setAttribute(_lightModel);

	group->addChild(_sun);

	osg::Vec3 direction(0.0f, 1.0f, -1.0f);
	float angle = 45.0f;

	// create tex gen
	osg::Vec3 up(0.0f, 0.0f, 1.0f);
	up = (direction ^ up) ^ direction;
	up.normalize();

	osg::TexGenNode* texgenNode = new osg::TexGenNode;
	texgenNode->setTextureUnit(1);
	osg::TexGen* texgen = texgenNode->getTexGen();
	texgen->setMode(osg::TexGen::EYE_LINEAR);
	texgen->setPlanesFromMatrix(osg::Matrixd::lookAt(position, position+direction, up)*
					osg::Matrixd::perspective(angle, 1.0f, 0.1f, 100));

	group->addChild(texgenNode);

	return group;
}

void CltViewer::setEnvironment(uint32_t timeOfDay)
{
	// set the lights of the different elements
	setLights(timeOfDay);
}

void CltViewer::setLights(uint32_t timeOfDay)
{
	//logDEBUG("CltViewer::setLights(%u)", timeOfDay);

	// mafm: we save the time of the last update, so we know whether the
	// last update was performed or not.  It might be not performed when: 1)
	// it's the first update since we connected to the client; or 2) the
	// admin changed the time in the server (which can happen when testing).
	//
	// Knowing this means that we avoid to set lightning colors again and
	// again when not needed, which is during most of the time -- we only
	// change colors around dawn and dusk.  Thus we save precious FPS,
	// because lightning changes are very expensive.
	static int previousTimeOfDay = -10000; // -1 may conflict with comparison
	bool previousUpdateWasPerformed = false;
	if (previousTimeOfDay == (static_cast<int>(timeOfDay)-1)
		|| (timeOfDay == 0 && previousTimeOfDay == 1440)) {
		previousUpdateWasPerformed = true;
	}
	previousTimeOfDay = timeOfDay;

	// initializing variables to be used
	const float noTransparency = 1.0f;
	osg::Vec4 sunColor(0.0f, 0.0f, 0.0f, noTransparency);
	osg::Vec4 ambientColor(0.0f, 0.0f, 0.0f, noTransparency);
	osg::Vec4 skyColor(0.0f, 0.0f, 0.0f, noTransparency);

	// magic numbers obtained by experimentation, it gives neat lightning
	// effects especially at dawn/dusk
	float sun_night_red = 0.123f;
	float sun_night_green = 0.123f;
	float sun_night_blue = 0.12f;
	osg::Vec4 sunNight(sun_night_red, sun_night_green, sun_night_blue, noTransparency);

	float sun_dawn_red = 0.9f;
	float sun_dawn_green = 0.45f;
	float sun_dawn_blue = 0.45f;
	osg::Vec4 sunDawn(sun_dawn_red, sun_dawn_green, sun_dawn_blue, noTransparency);

	float sun_day_red = 1.16f;
	float sun_day_green = 1.13f;
	float sun_day_blue = 1.1f;
	osg::Vec4 sunDay(sun_day_red, sun_day_green, sun_day_blue, noTransparency);

	float sun_dusk_red = 0.9f;
	float sun_dusk_green = 0.65f;
	float sun_dusk_blue = 0.45f;
	osg::Vec4 sunDusk(sun_dusk_red, sun_dusk_green, sun_dusk_blue, noTransparency);

	float ambient_night_red = 0.115f;
	float ambient_night_green = 0.1f;
	float ambient_night_blue = 0.15f;
	osg::Vec4 ambientNight(ambient_night_red, ambient_night_green, ambient_night_blue, noTransparency);

	float ambient_dawn_red = 0.21f;
	float ambient_dawn_green = 0.2f;
	float ambient_dawn_blue = 0.23f;
	osg::Vec4 ambientDawn(ambient_dawn_red, ambient_dawn_green, ambient_dawn_blue, noTransparency);

	float ambient_day_red = 0.43f;
	float ambient_day_green = 0.4f;
	float ambient_day_blue = 0.46f;
	osg::Vec4 ambientDay(ambient_day_red, ambient_day_green, ambient_day_blue, noTransparency);

	float ambient_dusk_red = 0.21f;
	float ambient_dusk_green = 0.2f;
	float ambient_dusk_blue = 0.23f;
	osg::Vec4 ambientDusk(ambient_dusk_red, ambient_dusk_green, ambient_dusk_blue, noTransparency);

	float sky_night_red = 0.055f;
	float sky_night_green = 0.055f;
	float sky_night_blue = 0.174f;
	osg::Vec4 skyNight(sky_night_red, sky_night_green, sky_night_blue, noTransparency);

	float sky_dawn_red = 0.292f;
	float sky_dawn_green = 0.198f;
	float sky_dawn_blue = 0.433f;
	osg::Vec4 skyDawn(sky_dawn_red, sky_dawn_green, sky_dawn_blue, noTransparency);

	float sky_day_red = 0.15f;
	float sky_day_green = 0.3f;
	float sky_day_blue = 1.0f;
	osg::Vec4 skyDay(sky_day_red, sky_day_green, sky_day_blue, noTransparency);

	float sky_dusk_red = 0.46f;
	float sky_dusk_green = 0.16f;
	float sky_dusk_blue = 0.018f;
	osg::Vec4 skyDusk(sky_dusk_red, sky_dusk_green, sky_dusk_blue, noTransparency);

	if (timeOfDay >= 1320 || timeOfDay < 300) {
		// night, 22:00 to 05:00
		if (previousUpdateWasPerformed && timeOfDay > 1320) {
			return;
		}
		sunColor = sunNight;
		ambientColor = ambientNight;
		skyColor = skyNight;
	} else if (timeOfDay >= 300 && timeOfDay < 390) {
		// night to dawn, 05:00 to 06:30
		float scale = float(timeOfDay - 300) / 90.0f;

		float sun_red = sun_night_red + (sun_dawn_red - sun_night_red) * scale;
		float sun_green = sun_night_green + (sun_dawn_green - sun_night_green) * scale;
		float sun_blue = sun_night_blue + (sun_dawn_blue - sun_night_blue) * scale;
		sunColor = osg::Vec4(sun_red, sun_green, sun_blue, noTransparency);

		float ambient_red = ambient_night_red + (ambient_dawn_red - ambient_night_red) * scale;
		float ambient_green = ambient_night_green + (ambient_dawn_green - ambient_night_green) * scale;
		float ambient_blue = ambient_night_blue + (ambient_dawn_blue - ambient_night_blue) * scale;
		ambientColor = osg::Vec4(ambient_red, ambient_green, ambient_blue, noTransparency);

		float sky_red = sky_night_red + (sky_dawn_red - sky_night_red) * scale;
		float sky_green = sky_night_green + (sky_dawn_green - sky_night_green) * scale;
		float sky_blue = sky_night_blue + (sky_dawn_blue - sky_night_blue) * scale;
		skyColor = osg::Vec4(sky_red, sky_green, sky_blue, noTransparency);
	} else if (timeOfDay >= 390 && timeOfDay < 480) {
		// dawn to day, 06:30 to 08:00
		float scale = float(timeOfDay - 390) / 90.0f;

		float sun_red = sun_dawn_red + (sun_day_red - sun_dawn_red) * scale;
		float sun_green = sun_dawn_green + (sun_day_green - sun_dawn_green) * scale;
		float sun_blue = sun_dawn_blue + (sun_day_blue - sun_dawn_blue) * scale;
		sunColor = osg::Vec4(sun_red, sun_green, sun_blue, noTransparency);

		float ambient_red = ambient_dawn_red + (ambient_day_red - ambient_dawn_red) * scale;
		float ambient_green = ambient_dawn_green + (ambient_day_green - ambient_dawn_green) * scale;
		float ambient_blue = ambient_dawn_blue + (ambient_day_blue - ambient_dawn_blue) * scale;
		ambientColor = osg::Vec4(ambient_red, ambient_green, ambient_blue, noTransparency);

		float sky_red = sky_dawn_red + (sky_day_red - sky_dawn_red) * scale;
		float sky_green = sky_dawn_green + (sky_day_green - sky_dawn_green) * scale;
		float sky_blue = sky_dawn_blue + (sky_day_blue - sky_dawn_blue) * scale;
		skyColor = osg::Vec4(sky_red, sky_green, sky_blue, noTransparency);
	} else if (timeOfDay >= 480 && timeOfDay < 1140) {
		// day, 08:00 to 19:00
		if (previousUpdateWasPerformed && timeOfDay > 480) {
			return;
		}
		sunColor = sunDay;
		ambientColor = ambientDay;
		skyColor = skyDay;
	} else if (timeOfDay >= 1140 && timeOfDay < 1230) {
		// day to dusk, 19:00 to 20:30
		float scale = float(timeOfDay - 1140) / 90.0f;

		float sun_red = sun_day_red + (sun_dusk_red - sun_day_red) * scale;
		float sun_green = sun_day_green + (sun_dusk_green - sun_day_green) * scale;
		float sun_blue = sun_day_blue + (sun_dusk_blue - sun_day_blue) * scale;
		sunColor = osg::Vec4(sun_red, sun_green, sun_blue, noTransparency);

		float ambient_red = ambient_day_red + (ambient_dusk_red - ambient_day_red) * scale;
		float ambient_green = ambient_day_green + (ambient_dusk_green - ambient_day_green) * scale;
		float ambient_blue = ambient_day_blue + (ambient_dusk_blue - ambient_day_blue) * scale;
		ambientColor = osg::Vec4(ambient_red, ambient_green, ambient_blue, noTransparency);

		float sky_red = sky_day_red + (sky_dusk_red - sky_day_red) * scale;
		float sky_green = sky_day_green + (sky_dusk_green - sky_day_green) * scale;
		float sky_blue = sky_day_blue + (sky_dusk_blue - sky_day_blue) * scale;
		skyColor = osg::Vec4(sky_red, sky_green, sky_blue, noTransparency);
	} else if (timeOfDay >= 1230 && timeOfDay < 1320) {
		// dusk to night, 20:30 to 22:00
		float scale = float(timeOfDay - 1230) / 90.0f;

		float sun_red = sun_dusk_red + (sun_night_red - sun_dusk_red) * scale;
		float sun_green = sun_dusk_green + (sun_night_green - sun_dusk_green) * scale;
		float sun_blue = sun_dusk_blue + (sun_night_blue - sun_dusk_blue) * scale;
		sunColor = osg::Vec4(sun_red, sun_green, sun_blue, noTransparency);

		float ambient_red = ambient_dusk_red + (ambient_night_red - ambient_dusk_red) * scale;
		float ambient_green = ambient_dusk_green + (ambient_night_green - ambient_dusk_green) * scale;
		float ambient_blue = ambient_dusk_blue + (ambient_night_blue - ambient_dusk_blue) * scale;
		ambientColor = osg::Vec4(ambient_red, ambient_green, ambient_blue, noTransparency);

		float sky_red = sky_dusk_red + (sky_night_red - sky_dusk_red) * scale;
		float sky_green = sky_dusk_green + (sky_night_green - sky_dusk_green) * scale;
		float sky_blue = sky_dusk_blue + (sky_night_blue - sky_dusk_blue) * scale;
		skyColor = osg::Vec4(sky_red, sky_green, sky_blue, noTransparency);
	}

	//_viewer->getCamera()->setClearColor(skyColor);
	_skyDome->setColor(skyColor);
	_sun->getLight()->setAmbient(sunColor);
	_sun->getLight()->setDiffuse(sunColor);
	_sun->getLight()->setSpecular(sunColor);
	_lightModel->setAmbientIntensity(ambientColor);
}

float CltViewer::getTerrainHeight(const osg::Vec3& position) const
{
	osg::LineSegment* raySegment = new osg::LineSegment();
	raySegment->set(osg::Vec3(position.x(), position.y(), position.z()+0.5),
			osg::Vec3(position.x(), position.y(), -999));

	osgUtil::IntersectVisitor terrainElevation;
	terrainElevation.addLineSegment(raySegment);
	_terrainNode->accept(terrainElevation);

	osgUtil::IntersectVisitor::HitList hits = terrainElevation.getHitList(raySegment);
	if (hits.empty()) {
		logERROR("Couldn't get terrain height");
		return -1000.0f;
	} else {
		osg::Vec3 hitPosition = hits.front().getWorldIntersectPoint();
		return hitPosition.z();
	}
}

void CltViewer::cameraClipping(const osg::Vec3& source, const osg::Vec3& dest, osg::Vec3& finalPos)
{
	osg::LineSegment* raySegment = new osg::LineSegment();
	raySegment->set(source, dest);

	osgUtil::IntersectVisitor cameraInters;
	cameraInters.addLineSegment(raySegment);
	_scene->accept(cameraInters);

	osgUtil::IntersectVisitor::HitList hits = cameraInters.getHitList(raySegment);
	if (hits.empty()) {
		finalPos = dest;
	} else {
		finalPos = hits.front().getWorldIntersectPoint();
		float terrainHeight = getTerrainHeight(finalPos);
		if (finalPos[2] < terrainHeight)
			finalPos[2] = terrainHeight;
		// logDEBUG("Clipping camera! at: (%.1f, %.1f, %.1f)", finalPos.x(), finalPos.y(), finalPos.z());
	}
}

void CltViewer::applyCollisionAndTerrainHeight(CltEntityBase* entity,
					       const osg::Vec3& source,
					       const osg::Vec3& dest,
					       osg::Vec3& finalPos)
{
	// mafm: this is not accurate, when player keeps the key pressed
	// (*especially* with auto-running enabled) the model keeps moving
	// forward with some jerkiness, it seems as it in some frames the ray
	// casted didn't hit anything and so allowed the player to move.
	//
	// This is quite puzzling, and so further tricks are considered like
	// considering the initial destination further away and moving the
	// player backwards (so hopefully the player won't cross the obstacle no
	// matter how long it keeps trying).

	//logDEBUG("Collision Detection with player at (%.3f, %.3f, %.3f)", source.x(), source.y(), source.z());

	/*
	static CollisionBall colBall;
	if (colBall.geode)
		_scene->removeChild(colBall.geode);
	*/

	// We devise here 5 displacements which are around the body center,
	// which correspond roughly to hips and shoulders as well as the center
	// itself (not the full body represented by bounding box, since it's
	// clear that players are flexible and can overcome little obstacles).
	// Similar effect would be achieved if we displaced a polygon with those
	// edges, and it hit something between source and destination.
	//
	// The idea is that instead of projecting a single ray source->dest, we
	// project several of them, so the player can't pass if there's a little
	// crack on a wall if it happens to be in the point that we check for
	// hits.  Probably the method of displacing a polygon would be too
	// sensible and wouldn't allow players to cross branches or similar
	// objects.
	float bodyCenter = entity->getBoundingBox().center().z();
	float heightDispl = 0.30;
	float widthDispl = 0.15;
	std::vector<osg::Vec3> displ;
	displ.push_back(osg::Vec3(0, 0, bodyCenter));
	displ.push_back(osg::Vec3( widthDispl, 0, bodyCenter+heightDispl));
	displ.push_back(osg::Vec3(-widthDispl, 0, bodyCenter+heightDispl));
	displ.push_back(osg::Vec3( widthDispl, 0, bodyCenter-heightDispl));
	displ.push_back(osg::Vec3(-widthDispl, 0, bodyCenter-heightDispl));

	// final position if nothing happens (overwritten if collision)
	finalPos = osg::Vec3(dest.x(), dest.y(), getTerrainHeight(dest));

	osg::Vec3 destCol(source+(dest-source)*5);

	// add segments to test intersections for
	osgUtil::IntersectVisitor collisionDet;
	for (auto itDispl = displ.begin(); itDispl != displ.end(); ++itDispl) {
		osg::Matrix tmpMatrix;

		// source, with a little space behind for safety
		tmpMatrix = osg::Matrix::translate(source) * osg::Matrix::translate(*itDispl);
		osg::Vec3 tmpSrc = tmpMatrix.getTrans();

		tmpMatrix = osg::Matrix::translate(destCol) * osg::Matrix::translate(*itDispl);
		osg::Vec3 tmpDest = tmpMatrix.getTrans();

		/*
		logDEBUG(" - ray from (%.3f, %.3f, %.3f) to (%.3f, %.3f, %.3f)",
			 tmpSrc.x(), tmpSrc.y(), tmpSrc.z(),
			 tmpDest.x(), tmpDest.y(), tmpDest.z());
		*/

		osg::LineSegment* raySegment = new osg::LineSegment(tmpSrc, tmpDest);
		collisionDet.addLineSegment(raySegment);
	}

	// intersect and gather hits in a big list
	_scene->accept(collisionDet);
	osgUtil::IntersectVisitor::LineSegmentHitListMap hitsMap = collisionDet.getSegHitList();
	osgUtil::IntersectVisitor::HitList hits;
	for (auto it = hitsMap.begin(); it != hitsMap.end(); ++it) {
		hits.insert(hits.begin(), it->second.begin(), it->second.end());
	}

	// check whether the hit is with the player itself (or some object/part
	// of the same node)
	logDEBUG(" - %zu hits", hits.size());
	for (auto it = hits.begin(); it != hits.end(); ++it) {
		//logDEBUG(" - collided with genode: %p", it->getGeode());

		// gather parents of the node with which the player collided
		osg::NodePathList nodePathList = it->getGeode()->getParentalNodePaths(_scene);
		osg::NodePath allParents;
		for (auto it2 = nodePathList.begin(); it2 != nodePathList.end(); ++it2) {
			allParents.insert(allParents.begin(),
					  it2->begin(), it2->end());
		}

		// check whether some of those nodes are the player itself
		auto itIsParent = std::find(allParents.begin(), allParents.end(), entity->getNode());
		if (itIsParent != allParents.end()) {
			//logDEBUG("     - self collision ignored ('%s')", it->getGeode()->getName().c_str());
			break;
		} else {
			osg::Vec3 collision(it->getWorldIntersectPoint());
			/*
			logDEBUG("     - collision with %p '%s' at (%.3f, %.3f, %.3f)",
				 it->getGeode(),
				 it->getGeode()->getName().c_str(),
				 collision.x(), collision.y(), collision.z());
			*/

			// mafm: moving slightly backwards
			osg::Vec3 keepDistance = (dest-source);
			keepDistance.normalize();
			keepDistance *= 0.5;
			finalPos = source - keepDistance;

			//_scene->addChild(colBall.showAt(collision));

			// exit, one valid hit is enough
			return;
		}
	}
}

EntityID::value_type CltViewer::pick(float x, float y) const
{
	osgUtil::LineSegmentIntersector::Intersections intersections;
	if (_viewer->computeIntersections(x, y, intersections)) {
		if (!intersections.begin()->nodePath.empty()) {
			// nodePath.back() is the 1st geode being hit
			osg::Node* target = intersections.begin()->nodePath.back();
			if (target->getNumDescriptions() > 0) {
				EntityID::value_type id = EntityID::toNumerical(target->getDescription(0).c_str());
				logDEBUG("Picking: name='%s', description=%s",
					 target->getName().c_str(),
					 target->getDescription(0).c_str());
				return id;
			}
		}
	}

	return 0;
}


NAMESPACE_END(AmbarMetta);
