/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltentitycreature.hxx"



NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltEntityCreature
 ******************************************************************************/
CltEntityCreature::CltEntityCreature(const MsgEntityCreate* entityBasicData) :
	CltEntityPlayer(entityBasicData)
{
	// override
	_className = "Creature";
}

CltEntityCreature::~CltEntityCreature()
{
}


NAMESPACE_END(AmbarMetta);
