/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltentitymainplayer.hxx"

#include "client/entity/cltentitymgr.hxx"
#include "client/net/cltnetmgr.hxx"

#include "common/math.hxx"
#include "common/net/msgs.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltInventory
 ******************************************************************************/
void CltInventory::addItem(const InventoryItem& item)
{
        logDEBUG("Adding entity (%s, %s, %s, %g) to the inventory",
		 EntityID::toString(item.entityID).c_str(),
		 item.getType(), item.getSubtype(), item.getLoad());

        _inventory[item.entityID] = item;

	// notifications
        CltInventoryObserverEvent event(CltInventoryObserverEvent::ActionId::ItemAdded);
	event._data.push_back(item);
	notifyObservers(event);
}

void CltInventory::removeItem(EntityID::value_type entityID)
{
        logDEBUG("Removing entity %s from the inventory", EntityID::toString(entityID).c_str());

        // removing this entity from the inventory list
        for (auto it = _inventory.begin(); it != _inventory.end(); ++it) {
                if (entityID == it->second.entityID) {
                        _inventory.erase(it);

			// notifications
			CltInventoryObserverEvent event(CltInventoryObserverEvent::ActionId::ItemRemoved);
			event._data.push_back(it->second);
			notifyObservers(event);

                        return;
                }
        }

        logERROR("Entity not found in inventory when trying to remove it");
}

void CltInventory::onAttachObserver(Observer* observer)
{
        logDEBUG("CltInventory: Subscribing observer '%s'", observer->_name.c_str());

        CltInventoryObserverEvent event(CltInventoryObserverEvent::ActionId::ItemListing);

        for (auto it = _inventory.begin(); it != _inventory.end(); ++it) {
                event._data.push_back(it->second);
        }

	observer->updateFromObservable(event);
}


/*******************************************************************************
 * CltEntityMainPlayer
 ******************************************************************************/
CltEntityMainPlayer* CltEntityMainPlayer::_instance = 0;

CltEntityMainPlayer& CltEntityMainPlayer::instance()
{
	if (!_instance) {
		logFATAL("Main player instance not set up");
	}

	return *_instance;
}

CltEntityMainPlayer::CltEntityMainPlayer(const MsgEntityCreate* entityBasicData) :
	CltEntityPlayer(entityBasicData)
{
	// override var initialized by base class
	_className = "MainPlayer";

	// update instance
	if (_instance) {
		logERROR("Main player instance already set up, ignoring request");
	} else {
		_instance = this;
	}
}

CltEntityMainPlayer::~CltEntityMainPlayer()
{
}

void CltEntityMainPlayer::setPlayerData(MsgPlayerData* playerData)
{
	_playerData = *playerData;
}

void CltEntityMainPlayer::pick(EntityID::value_type entityID) const
{
	// send message to the server
	MsgInventoryGet msg;
	msg.itemID = entityID;
	CltNetworkMgr::instance().sendToServer(msg);
}

void CltEntityMainPlayer::drop(EntityID::value_type entityID) const
{
	// send message to the server
	MsgInventoryDrop msg;
	msg.itemID = entityID;
	CltNetworkMgr::instance().sendToServer(msg);
}

void CltEntityMainPlayer::addToInventory(const InventoryItem& item)
{
	_inventory.addItem(item);
}

void CltEntityMainPlayer::removeFromInventory(EntityID::value_type entityID)
{
	_inventory.removeItem(entityID);
}

float CltEntityMainPlayer::getDistanceTo(CltEntityBase* target) const
{
	const MsgEntityMove& mMov = *target->getMovementProperties();

	// formula for distance, look in some place for more info if you don't
	// understand it
	float x = power2(_entityMovData.position.x - mMov.position.x);
	float y = power2(_entityMovData.position.y - mMov.position.y);
	float z = power2(_entityMovData.position.z - mMov.position.z);
	float distance = sqrt(x + y + z);
	logDEBUG("Distance (%.1f, %.1f, %.1f) to (%.1f, %.1f, %.1f) = %0.1f",
	       _entityMovData.position.x, _entityMovData.position.y, _entityMovData.position.z,
	       mMov.position.x, mMov.position.y, mMov.position.z,
	       distance);

	return distance;
}

float CltEntityMainPlayer::getDistanceTo(EntityID::value_type entityID) const
{
	CltEntityBase* target = CltEntityMgr::instance().getEntity(entityID);
	CHECK_FATAL(target);

	return getDistanceTo(target);
}


NAMESPACE_END(AmbarMetta);
