/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltentitybase.hxx"

#include "client/cltviewer.hxx"

#include <osg/BoundingBox>
#include <osg/ComputeBoundsVisitor>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/Node>
#include <osg/Vec3>
#include <osgText/Text>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltEntityBase
 ******************************************************************************/
CltEntityBase::CltEntityBase(const MsgEntityCreate* entityBasicData, const char* className_) :
	_className(className_),
	_entityBasicData(*entityBasicData),
	_node(0),
	_transform(new osg::MatrixTransform()),
	_label(0)
{
}

CltEntityBase::~CltEntityBase()
{
}

void CltEntityBase::loadModel()
{
	// load and set basic properties
	_node = loadModelImplementation();
	_node->getOrCreateStateSet()->setMode(GL_LIGHTING,
					      osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	_node->setName(_entityBasicData.entityName);
	_node->addDescription(EntityID::toString(_entityBasicData.entityID));

	// put in position
	_transform->addChild(_node);

	// label
	_label = new osgText::Text();
	_label->setFont(FONT_TEXT_LABEL);
	_label->setCharacterSizeMode(osgText::Text::OBJECT_COORDS);
	_label->setCharacterSize(0.16f);
	_label->setAxisAlignment(osgText::Text::SCREEN);
	_label->setAlignment(osgText::Text::CENTER_CENTER);
	_label->setPosition(_node->getBound().center()
			    + osg::Vec3(0, 0, _node->getBound().radius()));
	_label->setBackdropType(osgText::Text::OUTLINE);
	_label->setBackdropType(osgText::Text::DROP_SHADOW_BOTTOM_RIGHT);
	_label->setText(_entityBasicData.entityName);
	osg::Geode* labelGeode = new osg::Geode;
	labelGeode->addDrawable(_label);
	labelGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF); // mafm: doesn't seem to work
	_transform->addChild(labelGeode);

	// calculate bounding box
	osg::ComputeBoundsVisitor nv;
	_node->accept(nv);
	_boundingBox = nv.getBoundingBox();
	//logDEBUG("bb x: %g %g", _boundingBox.xMin(), _boundingBox.xMax());
	//logDEBUG("bb y: %g %g", _boundingBox.yMin(), _boundingBox.yMax());
	//logDEBUG("bb z: %g %g", _boundingBox.zMin(), _boundingBox.zMax());
	CHECK_FATAL(_boundingBox.valid());

	/* mafm: for visualizing the bounding box

	// draw collision bounding box
	{
		float lengthX = (_boundingBox.xMax() - _boundingBox.xMin()) * 0.66;
		float lengthY = (_boundingBox.yMax() - _boundingBox.yMin()) * 0.66;
		float lengthZ = (_boundingBox.zMax() - _boundingBox.zMin()) * 0.66;

		osg::ShapeDrawable* shapeDrawable = new osg::ShapeDrawable(new osg::Box(_boundingBox.center(),
											lengthX,
											lengthY,
											lengthZ));
		shapeDrawable->setColor(osg::Vec4(0, 0, 1, 0.5));
		osg::Geode* geode = new osg::Geode();
                geode->addDrawable(shapeDrawable);

		_transform->addChild(geode);
	}
	*/
}

const char* CltEntityBase::className() const
{
	return _className.c_str();
}

const char* CltEntityBase::getName() const
{
	return _entityBasicData.entityName.c_str();
}

EntityID CltEntityBase::getID() const
{
	return EntityID(_entityBasicData.entityID);
}

const osg::Node* CltEntityBase::getNode() const
{
	return _node;
}

osg::Node* CltEntityBase::getNode()
{
	return _node;
}

const osg::MatrixTransform* CltEntityBase::getTransform() const
{
	return _transform;
}

const osg::BoundingBox& CltEntityBase::getBoundingBox() const
{
	return _boundingBox;
}

void CltEntityBase::setMovementProperties(const MsgEntityMove* entityMovData)
{
	_entityMovData = *entityMovData;

	osg::Vec3 position(_entityMovData.position.x,
			   _entityMovData.position.y,
			   _entityMovData.position.z);

	// have into account terrain height
	float terrainHeight = CltViewer::instance().getTerrainHeight(position);
	position[2] = terrainHeight;
	_entityMovData.position.z = position.z();

	// mafm: translation is absolute, not depending on the rotation, so it's
	// set in a specific order
	osg::Matrix matrix = osg::Matrix::rotate(_entityMovData.rot, osg::Z_AXIS)
		* osg::Matrix::translate(position);
	_transform->setMatrix(matrix);
}

const MsgEntityMove* CltEntityBase::getMovementProperties() const
{
	return &_entityMovData;
}


NAMESPACE_END(AmbarMetta);
