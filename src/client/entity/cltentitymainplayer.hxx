/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_ENTITY_MAINPLAYER_H__
#define __AMBARMETTA_CLIENT_ENTITY_MAINPLAYER_H__


/** \file cltentitymainplayer.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Class for main player entity and related things.
 */


#include "client/entity/cltentityplayer.hxx"

#include "common/datatypes.hxx"
#include "common/patterns/observer.hxx"

#include "client/cltconfig.hxx"

#include <map>
#include <vector>


NAMESPACE_START(AmbarMetta);


/** ObserverEvent for CltInventory.
 *
 * This observer event lets the receiver know about the contents, additions and
 * removal of the inventory.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltInventoryObserverEvent : public ObserverEvent
{
public:
        /** Action Identifier enumerator */
        enum ActionId : int { ItemListing = 1, ItemAdded, ItemRemoved };

        //// Action Identifier
        const ActionId _actionId;

        /// Payload
	std::vector<InventoryItem> _data;


        /** Constructor */
        CltInventoryObserverEvent(ActionId actionId) :
                ObserverEvent("CltInventoryObserverEvent"),
                _actionId(actionId)
                { }
};


/** Inventory of the main player, simple implementation at the moment.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltInventory : public Observable
{
public:
	/** Add item to the inventory */
        void addItem(const InventoryItem& item);
        /** Remove an item from the inventory */
        void removeItem(EntityID::value_type entityID);

private:
	/// The list of items in the inventory
	std::map<EntityID::value_type, InventoryItem> _inventory;

        /** @see Observable::onAttachObserver()
         *
         * When an observer attaches, this class sends an update with all of the
         * items.
         */
        virtual void onAttachObserver(Observer* observer);
};


/** Controls the main player entity, which handles most of the game logic.  Note
 * that it's convenient to have a Singleton for many reasons, among other to
 * have only one instance, and not having to rely on external modules to hold a
 * pointer to it (so any class can access the main player with ::instance()).
 *
 * \remark mafm This doesn't use the Singleton template of the rest of the
 * classes because of the aforementioned issues.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltEntityMainPlayer : public CltEntityPlayer
{
public: 
	/** Singleton-like access to instance */
	static CltEntityMainPlayer& instance();

	/** Default constructor */
	CltEntityMainPlayer(const MsgEntityCreate* entityBasicData);
	/** Destructor */
	~CltEntityMainPlayer();

	/** Set the player statistics */
	void setPlayerData(MsgPlayerData* playerData);

	/** Ask the server to pick up an item from the world to the inventory */
	void pick(EntityID::value_type entityID) const;
	/** Ask the server to drop an item from the inventory to the world */
	void drop(EntityID::value_type entityID) const;

	/** Add an object to the inventory (after server confirmation) */
	void addToInventory(const InventoryItem& item);
	/** Remove an object from the inventory (after server confirmation) */
	void removeFromInventory(EntityID::value_type entityID);

	/** Get distance to entity */
	float getDistanceTo(CltEntityBase* target) const;
	/** Get distance to entity */
	float getDistanceTo(EntityID::value_type entityID) const;


private:
	/// Singleton-like instance
	static CltEntityMainPlayer* _instance;


	/// The player data (statistics, abilities and so on)
	MsgPlayerData _playerData;

	/// The inventory
	CltInventory _inventory;
};


NAMESPACE_END(AmbarMetta);
#endif
