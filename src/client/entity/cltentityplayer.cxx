/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltentityplayer.hxx"

#include "client/cltviewer.hxx"
#include "client/content/cltcontentloader.hxx"

#include "common/math.hxx"

#include <osg/MatrixTransform>
#include <osg/Vec3>
#include <osgCal/CoreModel>
#include <osgCal/Model>
#include <cal3d/coremodel.h>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltEntityPlayer
 ******************************************************************************/
const float CltEntityPlayer::WALK_SPEED = 2.0f; // 2m/s = 7.2km/h
const float CltEntityPlayer::RUN_SPEED = 5.0f; // 5m/s = 18km/h
const float CltEntityPlayer::ROTATE_SPEED = (2*PI_NUMBER)/4.0f; // in rad/s, 4s for full revolution

CltEntityPlayer::CltEntityPlayer(const MsgEntityCreate* entityBasicData) :
	CltEntityBase(entityBasicData, "Player"),
	_OSGCal3DModel(0)
{
}

CltEntityPlayer::~CltEntityPlayer()
{
}

osg::Node* CltEntityPlayer::loadModelImplementation()
{
	// load model with the help of the content loader
	_OSGCal3DModel = new osgCal::Model();
	osgCal::CoreModel* coreModel = CltContentLoader::instance().loadCal3DCoreModel(_entityBasicData.meshType,
										       _entityBasicData.meshSubtype);
	if (!_OSGCal3DModel->setCoreModel(coreModel)) {
		logERROR("Loading cal3d model: %s", CalError::getLastErrorDescription().c_str());
		return 0;
	}

	for (int i = 0; i < _OSGCal3DModel->getCalCoreModel()->getCoreMeshCount(); ++i) {
		_OSGCal3DModel->getCalModel()->attachMesh(i);
	}

	// set the material set of the whole model
	_OSGCal3DModel->getCalModel()->setMaterialSet(0);

	// creating a concrete model using the core template
	if (!_OSGCal3DModel->create()) {
		logERROR("Creating cal3d model: %s", CalError::getLastErrorDescription().c_str());
		return 0;
	}

	_node = _OSGCal3DModel;
	return _node;
}

void CltEntityPlayer::setMovementProperties(const MsgEntityMove* entityMovData)
{
	// set in base class
	CltEntityBase::setMovementProperties(entityMovData);

	// action
	if (_entityMovData.run && _entityMovData.mov_fwd) {
		setAction(Action::Run);
	} else if (_entityMovData.mov_fwd || _entityMovData.mov_bwd) {
		setAction(Action::Walk);
	} else {
		setAction(Action::Idle);
	}

	/*
	logDEBUG("Position of player '%s' changed to: (%.1f, %.1f, %.1f), rot=%.1f",
	       getName(), p.x(), p.y(), p.z(), _entityMovData.rot);
	*/
}

void CltEntityPlayer::setAction(Action action)
{
	_OSGCal3DModel->getCalModel()->getMixer()->clearCycle(Action::Idle, 0.2f);
	_OSGCal3DModel->getCalModel()->getMixer()->clearCycle(Action::Walk, 0.2f);
	_OSGCal3DModel->getCalModel()->getMixer()->clearCycle(Action::Run, 0.2f);
	_OSGCal3DModel->getCalModel()->getMixer()->blendCycle(action, 0.8f, 0.5f);
}

void CltEntityPlayer::updateTransform(double elapsedSeconds)
{
	// mafm: This is similar to the control of the main player, but it's not
	// autonomous.
        //
	// To calculate the new position of the player, we start by calculating
	// the displacement from the current position.  To achieve this, we
	// calculate the speed (walking or running), then whether is moving
	// forward or backward (y=1 and y=-1 respectively), and applying the
	// rotation; all of this related to the time elapsed.
	//
	// Once we get this, we have to apply the modification over the previous
	// position and look-at "attitude".

	bool running = _entityMovData.run;
	bool movingForward = _entityMovData.mov_fwd;
	bool movingBackward = _entityMovData.mov_bwd;
	bool rotatingLeft = _entityMovData.rot_left;
	bool rotatingRight = _entityMovData.rot_right;

	// idle?
	if (!_entityMovData.mov_fwd && !_entityMovData.mov_bwd
	    && !_entityMovData.rot_left && _entityMovData.rot_right) {
		return;
	}

	// basic speed (not used if not moving)
	float linearSpeed = WALK_SPEED * elapsedSeconds;
	if (running && movingForward)
		linearSpeed = RUN_SPEED * elapsedSeconds;

	// rotation (affecting final displacement vector, and rotation of the 3D
	// model)
	float rotation = 0.0f;
	if (rotatingLeft) {
		rotation = ROTATE_SPEED * elapsedSeconds;
	} else if (rotatingRight) {
		rotation = ROTATE_SPEED * elapsedSeconds * (-1);
	}

	// vector of linear displacement (when walking forward, backwards is the
	// opposite), not counting rotation
	osg::Vec3 linearDisplacement(0, 0, 0);
	if (movingForward) {
		// rotation influence in linear movement:
		// - x is the sin(rotation)*y, y=1*linearSpeed
		// - y is the cos(rotation)*y, y=1*linearSpeed
		// - z axis not affected
		linearDisplacement[0] = sin(rotation) * linearSpeed;
		linearDisplacement[1] = cos(rotation) * linearSpeed;
	} else if (movingBackward) {
		// rotation is inverted, to result how player expects
		rotation = -rotation;

		// rotation influence in linear movement:
		// - x is the sin(rotation)*y, y=-1*linearSpeed
		// - y is the cos(rotation)*y, y=-1*linearSpeed
		// - z axis not affected
		linearDisplacement[0] = sin(rotation) * linearSpeed * (-1);
		linearDisplacement[1] = cos(rotation) * linearSpeed * (-1);
	}

	// do collision det and terrain height, only when moving and not
	// rotating (saves from warning messages about invalid ray segments of
	// length zero)
	if (linearDisplacement != osg::Vec3(0, 0, 0)) {
		osg::Vec3 source = _transform->getMatrix().getTrans();
		// make a matrix for the new position (with rotation around Z
		// axis)
		osg::Matrix destMatrix = osg::Matrix::translate(linearDisplacement)
			* osg::Matrix::rotate(rotation, osg::Z_AXIS)
			* _transform->getMatrix();
		osg::Vec3 dest = destMatrix.getTrans();
		osg::Vec3 finalPos;
		CltViewer::instance().applyCollisionAndTerrainHeight(_entityBasicData.entityName,
								     source, dest, finalPos);
		destMatrix.setTrans(finalPos);

		// finally, set the new position
		_transform->setMatrix(destMatrix);
	} else if (rotation != 0.0) {
		// only rotation
		osg::Matrix destMatrix = osg::Matrix::rotate(rotation, osg::Z_AXIS)
			* _transform->getMatrix();
		_transform->setMatrix(destMatrix);
	}

/*
	osg::Vec3 p = _transform->getMatrix().getTrans();
	logDEBUG("Player '%s' position internal update: (%.1f, %.1f, %.1f)",
	       getName(), p.x(), p.y(), p.z());
*/
}


NAMESPACE_END(AmbarMetta);
