/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_ENTITY_MGR_H__
#define __AMBARMETTA_CLIENT_ENTITY_MGR_H__


/** \file cltentitymgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Manager for client-side entities.
 */


#include "common/entities.hxx"
#include "common/patterns/singleton.hxx"

#include "client/cltconfig.hxx"

#include <map>


NAMESPACE_START(AmbarMetta);


class MsgEntityCreate;
class MsgEntityMove;
class MsgEntityDestroy;
class CltEntityBase;


/** This class takes care of managing the entities received through network --
 * creating, destroying and manipulating them.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltEntityMgr : public Singleton<CltEntityMgr>
{
public:
	/** Handle a 'Entity New' msg */
	void entityCreate(const MsgEntityCreate* msg);
	/** Handle a 'Entity Move' msg */
	void entityMove(const MsgEntityMove* msg);
	/** Handle a 'Entity Destroy' msg */
	void entityDestroy(const MsgEntityDestroy* msg);

	/** Function to get called every frame, to update the position of the
	 * entities in the world. */
	void updateTransforms(double elapsedSeconds);

	/** Get entity */
	CltEntityBase* getEntity(EntityID::value_type id) const;

private:
	/** Singleton friend access */
	friend class Singleton<CltEntityMgr>;


	/// Entity list
	std::map<EntityID::value_type, CltEntityBase*> _entityList;


	/** Default constructor */
	CltEntityMgr();
	/** Destructor */
	~CltEntityMgr();
};


NAMESPACE_END(AmbarMetta);
#endif
