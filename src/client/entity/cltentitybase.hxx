/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_ENTITY_BASE_H__
#define __AMBARMETTA_CLIENT_ENTITY_BASE_H__


/** \file cltentitybase.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Base class for client-side entities.
 */


#include "common/net/msgs.hxx"

#include "client/cltconfig.hxx"

#include <osg/BoundingBox>
#include <osg/Vec3>


namespace osg {
	class Node;
	class MatrixTransform;
}
namespace osgText {
	class Text;
}


NAMESPACE_START(AmbarMetta);


/** Base class for entities in the client, which holds the basic data (class,
 * mesh type and so on).
 *
 * The objects created from this class are entities from the game logic point of
 * view, thus they must react to game events when needed, adhere to ruleset and
 * so on.  Basically, all the data depending on the game itself must be kept in
 * the derived classes, for clarity.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltEntityBase
{
public:
	/** Destructor */
	virtual ~CltEntityBase();

	/** Class Name, to identify different entities */
	const char* className() const;

	/** Entity name. */
	const char* getName() const;

	/** Entity id. */
	EntityID getID() const;

	/** Get Node. */
	const osg::Node* getNode() const;
	/** Get Node. */
	osg::Node* getNode();

	/** Entity transform. */
	const osg::MatrixTransform* getTransform() const;

	/** Entity bounding box. */
	const osg::BoundingBox& getBoundingBox() const;

	/** Get movement properties */
	const MsgEntityMove* getMovementProperties() const;

	/** Load the model, high-level version (depends on implementation of
	 * derived classes) */
	void loadModel();

	/** Load the model, implementation for derived classes */
	virtual osg::Node* loadModelImplementation() = 0;
	/** Function to get called every frame, to update the position of the
	 * entity in the world. */
	virtual void updateTransform(double elapsedSeconds) = 0;
	/** Set movement properties (overriden from base class, because we have
	 * to deal with animations and so on too). */
	virtual void setMovementProperties(const MsgEntityMove* entityMovData) = 0;

protected:
	/// Class name
	std::string _className;

	/// The basic entity data
	MsgEntityCreate _entityBasicData;
	/// The movement data
	MsgEntityMove _entityMovData;

	/// The engine representation of this entity
	osg::Node* _node;
	/// The engine representation of the position
	osg::MatrixTransform* _transform;
	/// The bounding box of the object
	osg::BoundingBox _boundingBox;
	/// The in-screen label
	osgText::Text* _label;


	/** Default constructor */
	CltEntityBase(const MsgEntityCreate* entityBasicData, const char* className);
};


NAMESPACE_END(AmbarMetta);
#endif
