/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_ENTITY_PLAYER_H__
#define __AMBARMETTA_CLIENT_ENTITY_PLAYER_H__


/** \file cltentityplayer.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Class for client-side player entities.
 */


#include "client/entity/cltentitybase.hxx"

#include "client/cltconfig.hxx"


namespace osgCal {
	class Model;
}


NAMESPACE_START(AmbarMetta);


/** Controls a regular player entity (other than the main player), which handles
 * most of the game logic.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltEntityPlayer : public CltEntityBase
{
public:
	/** Actions of the player, for animations.
	 *
	 * \note The number associated must match the animation in the model,
	 * and as corollary, all models have to implement the animations in the
	 * same place.
	 */
	enum Action : int { Idle = 0, Walk, Run };

public: 
	/** Default constructor */
	CltEntityPlayer(const MsgEntityCreate* entityBasicData);
	/** Destructor */
	virtual ~CltEntityPlayer();

	/** @see CltEntityBase::loadModelImplementation */
	virtual osg::Node* loadModelImplementation();
	/** @see CltEntityBase::updateTransform */
	virtual void updateTransform(double elapsedSeconds);
	/** @see CltEntityBase::setMovementProperties */
	virtual void setMovementProperties(const MsgEntityMove* entityMovData);

protected:
	/// Speed when walking
	static const float WALK_SPEED;
	/// Speed when running
	static const float RUN_SPEED;
	/// Speed when rotating left of right
	static const float ROTATE_SPEED;


	/// Animated model
	osgCal::Model* _OSGCal3DModel;


	/** Set the action (animation) to perform */
	void setAction(Action action);
};


NAMESPACE_END(AmbarMetta);
#endif
