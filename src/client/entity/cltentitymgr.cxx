/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltentitymgr.hxx"

#include "client/cltviewer.hxx"
#include "client/cltinput.hxx"
#include "client/entity/cltentitybase.hxx"
#include "client/entity/cltentitybuilder.hxx"

#include "common/net/msgs.hxx"

#include <osg/MatrixTransform>
#include <osg/Vec3>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltEntityMgr
 ******************************************************************************/
template <> CltEntityMgr* Singleton<CltEntityMgr>::INSTANCE = 0;

CltEntityMgr::CltEntityMgr()
{
}

CltEntityMgr::~CltEntityMgr()
{
}

void CltEntityMgr::entityCreate(const MsgEntityCreate* msg)
{
	logINFO("New entity: id: %llu, name: '%s', class '%s', mesh: '%s' '%s', pos: %.1f %.1f %.1f",
	       msg->entityID, msg->entityName.c_str(),
	       msg->entityClass.c_str(),
	       msg->meshType.c_str(), msg->meshSubtype.c_str(),
	       msg->position.x, msg->position.y, msg->position.z);

	CltEntityBase* entity = CltEntityBuilder::buildEntity(msg);
	if (!entity) {
		logERROR("Entity could not be created: id %llu, name '%s'",
		       msg->entityID, msg->entityName.c_str());
	} else {
		// add to the list
		CHECK_FATAL(getEntity(msg->entityID) == 0);
		_entityList[msg->entityID] = entity;
	}
}

void CltEntityMgr::entityMove(const MsgEntityMove* msg)
{
	logDEBUG("Received EntityMove msg for entity (id: %llu)", msg->entityID);

	// checking autenticity
	CltEntityBase* entity = getEntity(msg->entityID);
	if (entity) {
		// logDEBUG("Moving entity id '%llu'", msg->entityID);
		if (std::string("MainPlayer") == entity->className()) {
			logWARNING("Got a message requesting to change the position of the main player "
			       "(it should happen only when resetting the position for some reason)");
			osg::Vec3 position(msg->position.x, msg->position.y, msg->position.z);
			CltMainPlayerManipulator::instance().setPosition(position, msg->rot);
		} else {
			entity->setMovementProperties(msg);
		}
	} else {
		logERROR("Received entity move notification but entity unknown! (id: %llu)",
		       msg->entityID);
	}
}

void CltEntityMgr::entityDestroy(const MsgEntityDestroy* msg)
{
	auto it = _entityList.find(msg->entityID);
	if (it == _entityList.end()) {
		logERROR("Received entity destruction notification but entity unknown! (id: %llu)",
		       msg->entityID);
	} else {
		logDEBUG("Destroying entity id '%llu'", msg->entityID);

		// destroying entity entity
		CltEntityBase* entity = it->second;
		if (std::string("MainPlayer") == entity->className()) {
			logWARNING("Refusing to destroy main player entity!!!");
		} else {
			// removing model from to scene
			CltViewer::instance().removeFromScene(entity->getTransform());

			// removing from list
			_entityList.erase(it);

			delete entity;
		}
	}
}

void CltEntityMgr::updateTransforms(double elapsedSeconds)
{
	for (auto it = _entityList.begin(); it != _entityList.end(); ++it) {
		CltEntityBase* entity = it->second;
		entity->updateTransform(elapsedSeconds);
	}
}

CltEntityBase* CltEntityMgr::getEntity(EntityID::value_type id) const
{
	auto it = _entityList.find(id);
	if (it == _entityList.end()) {
		// not found
		return 0;
	} else {
		return it->second;
	}
}


NAMESPACE_END(AmbarMetta);
