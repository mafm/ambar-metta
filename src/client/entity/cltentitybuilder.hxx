/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_ENTITY_BUILDER_H__
#define __AMBARMETTA_CLIENT_ENTITY_BUILDER_H__


/** \file cltentitybuilder.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Builder for client-side entities.
 */


#include "client/cltconfig.hxx"


NAMESPACE_START(AmbarMetta);


class CltEntityBase;
class MsgEntityCreate;


/** Entity builder in the client.
 *
 * The objects created by this class are entities from the game logic point of
 * view, the process of creation is dependent on the engine to some extent, and
 * thus it's a bit inconvenient to mix it with the code from the entity
 * behaviours.
 *
 * \note The constructor and destructor are private so objects of this class
 * cannot be built, only the static method can be accessed.
 *
 * \todo mafm: needs to be implemented properly
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltEntityBuilder
{
public:
	/** Build entity according to the incoming data */
	static CltEntityBase* buildEntity(const MsgEntityCreate* createMsg);

private:
	/** Default constructor */
	CltEntityBuilder() { }
	/** Destructor */
	~CltEntityBuilder() { }
};


NAMESPACE_END(AmbarMetta);
#endif
