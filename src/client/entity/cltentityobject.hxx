/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CLIENT_ENTITY_OBJECT_H__
#define __AMBARMETTA_CLIENT_ENTITY_OBJECT_H__


/** \file cltentityobject.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Class for client-side object entities.
 */


#include "client/entity/cltentitybase.hxx"

#include "client/cltconfig.hxx"


NAMESPACE_START(AmbarMetta);


/** Controls an object entity, which handles most of the game logic for this
 * kind of entity.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CltEntityObject : public CltEntityBase
{
public: 
	/** Default constructor */
	CltEntityObject(const MsgEntityCreate* entityBasicData);
	/** Destructor */
	virtual ~CltEntityObject();

	/** @see CltEntityBase::loadModelImplementation */
	virtual osg::Node* loadModelImplementation();
	/** @see CltEntityBase::updateTransform */
	virtual void updateTransform(double elapsedSeconds);
	/** @see CltEntityBase::setMovementProperties */
	virtual void setMovementProperties(const MsgEntityMove* entityMovData);
};


NAMESPACE_END(AmbarMetta);
#endif
