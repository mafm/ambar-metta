/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltentityobject.hxx"

#include "client/cltviewer.hxx"
#include "client/content/cltcontentloader.hxx"

#include <osg/Matrix>
#include <osg/MatrixTransform>
#include <osg/Vec3>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltEntityObject
 ******************************************************************************/
CltEntityObject::CltEntityObject(const MsgEntityCreate* entityBasicData) :
	CltEntityBase(entityBasicData, "Object")
{
}

CltEntityObject::~CltEntityObject()
{
}

osg::Node* CltEntityObject::loadModelImplementation()
{
	// load model with the help of the content loader
	_node = CltContentLoader::instance().loadModel(_entityBasicData.meshType,
						       _entityBasicData.meshSubtype);
	if (!_node) {
		logERROR("Loading OSG model: %s", "error");
		return 0;
	}

	return _node;
}

void CltEntityObject::setMovementProperties(const MsgEntityMove* entityMovData)
{
	// set in base class
	CltEntityBase::setMovementProperties(entityMovData);

	/*
	logDEBUG("Position of object '%s' changed to: (%.1f, %.1f, %.1f), rot=%.1f",
	       getName(), p.x(), p.y(), p.z(), _entityMovData.rot);
	*/
}

void CltEntityObject::updateTransform(double /* elapsedSeconds */)
{
	// no update needed, our current objects are static...
}


NAMESPACE_END(AmbarMetta);
