/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltentitybuilder.hxx"

#include "client/cltcamera.hxx"
#include "client/cltinput.hxx"
#include "client/cltviewer.hxx"
#include "client/entity/cltentitybase.hxx"
#include "client/entity/cltentitycreature.hxx"
#include "client/entity/cltentityobject.hxx"
#include "client/entity/cltentitymainplayer.hxx"
#include "client/entity/cltentityplayer.hxx"

#include <osg/MatrixTransform>


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltEntityBuilder
 ******************************************************************************/
CltEntityBase* CltEntityBuilder::buildEntity(const MsgEntityCreate* createMsg)
{
	// create entity
	CltEntityBase* entity = 0;

	if (createMsg->entityClass == "MainPlayer") {
		// load the area where the main player wanders in, and add the
		// player transform too
		CltViewer::instance().loadScene(createMsg->area);

		// initialize the main player class before calling any method
		// through the singleton
		entity = new CltEntityMainPlayer(createMsg);

		// set position in the class controlling the player movement
		osg::Vec3 position(createMsg->position.x,
				   createMsg->position.y,
				   createMsg->position.z);
		CltMainPlayerManipulator::instance().setPosition(position, createMsg->rot);

		// set the camera having as target the main player transform
		osg::MatrixTransform* mainPlayerTransform = CltMainPlayerManipulator::instance().getMatrixTransform();
		CltCameraMgr::instance().setTargetTransform(mainPlayerTransform);

		logINFO("Main player created");

	} else if (createMsg->entityClass == "Player") {
		entity = new CltEntityPlayer(createMsg);
	} else if (createMsg->entityClass == "Creature") {
		entity = new CltEntityCreature(createMsg);
	} else if (createMsg->entityClass == "Object") {
		entity = new CltEntityObject(createMsg);
	} else {
		logERROR("Entity class not recognized: '%s'",
		       createMsg->entityClass.c_str());
		return 0;
	}

	if (!entity) {
		return 0;
	}

	// load model
	logINFO("Loading model for entity: %s", createMsg->entityClass.c_str());
	entity->loadModel();

	// movement
	MsgEntityMove movemsg;
	movemsg.area = createMsg->area;
	movemsg.position = createMsg->position;
	movemsg.run = false;
	movemsg.mov_fwd = false;
	movemsg.mov_bwd = false;
	movemsg.rot = createMsg->rot;
	movemsg.rot_left = false;
	movemsg.rot_right = false;
	entity->setMovementProperties(&movemsg);

	// add to scene
	CltViewer::instance().addToScene(entity->getTransform());

	return entity;
}


NAMESPACE_END(AmbarMetta);
