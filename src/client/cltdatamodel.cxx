/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cltdatamodel.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * CltDataModelMgr
 ******************************************************************************/
template <> CltDataModelMgr* Singleton<CltDataModelMgr>::INSTANCE = 0;


CltDataModelMgr::CltDataModelMgr()
{
}

CltDataModelMgr::~CltDataModelMgr()
{
}

void CltDataModelMgr::addOrUpdate(const std::string& key, const std::string& value)
{
	logDEBUG("CltDataModelMgr: adding/updating '%s' to '%s'", key.c_str(), value.c_str());

	_dataModel[key] = value;

	CltDataModelMgrObserverEvent event(CltDataModelMgrObserverEvent::ActionId::NewValue,
					   key, value);
	notifyObservers(event);
}

void CltDataModelMgr::onAttachObserver(Observer* observer)
{
	logDEBUG("CltDataModelMgr: Subscribing observer '%s'", observer->_name.c_str());

	CltDataModelMgrObserverEvent event(CltDataModelMgrObserverEvent::ActionId::NewValue,
					   "", "");

	for (auto it = _dataModel.begin(); it != _dataModel.end(); ++it) {
		event._data.first = it->first;
		event._data.second = it->second;
		observer->updateFromObservable(event);
	}
}


NAMESPACE_END(AmbarMetta);
