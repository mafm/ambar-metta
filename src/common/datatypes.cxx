/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datatypes.hxx"

#include "common/math.hxx"
#include "common/includes.hxx"

#include <cmath>


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * Vector3
 *******************************************************************************/
const float Vector3::TOLERANCE(0.000000001f);

Vector3::Vector3(float X, float Y, float Z) :
	x(X), y(Y), z(Z)
{
	checkLimits();
}

Vector3 Vector3::operator+(const Vector3& u) const
{
	return Vector3(x + u.x, y + u.y, z + u.z);
}

Vector3 Vector3::operator-(const Vector3& u) const
{
	return Vector3(x - u.x, y - u.y, z - u.z);
}

Vector3 Vector3::operator-() const
{
	return Vector3(-x, -y, -z);
}

float Vector3::operator*(const Vector3& u) const
{
	return (x * u.x + y * u.y + z * u.z);
}

Vector3 Vector3::operator/(float f) const
{
	return Vector3(x/f, y/f, z/f);
}

float Vector3::magnitude() const
{
	float mag = sqrt(x*x + y*y + z*z);
	if (mag < TOLERANCE) {
		return 0.0f;
	} else {
		return mag;
	}
}

float Vector3::distance(const Vector3& u) const
{
	float dist = sqrt(power2(x - u.x) + power2(y - u.y) + power2(z - u.z));
	if (dist < TOLERANCE) {
		return 0.0f;
	} else {
		return dist;
	}
}

void Vector3::normalize()
{
	float mag = magnitude();
	if (mag < TOLERANCE) {
		// nothing else to do
		//mag = 1.0f;
		return;
	}

	x /= mag;
	y /= mag;
	z /= mag;

	checkLimits();
}

void Vector3::checkLimits()
{
	if (fabs(x) < TOLERANCE) {
		x = 0.0f;
	}
	if (fabs(y) < TOLERANCE) {
		y = 0.0f;
	}
	if (fabs(z) < TOLERANCE) {
		z = 0.0f;
	}
}


NAMESPACE_END(AmbarMetta);
