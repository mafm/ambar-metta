/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.hxx"

#include "common/includes.hxx"

#include <cstdarg>
#include <cstdio>


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * strFmt
 *******************************************************************************/
std::string strFmt(const char* format, ...)
{
	// we first try to format the string into a buffer with fixed length,
	// and if it doesn't fit, we try a second time with the proper size that
	// the function vsnprintf returns.

	std::string output;

	// initial format, initial size not too small as to fail almost every
	// time and waste a lot of computation instead of using a bit of memory
	const size_t formatBufferSize = 64;
	char formatBuffer[formatBufferSize] = { 0 };

	// call to vsnprintf()
	va_list arg;
	va_start(arg, format);
	int result = vsnprintf(formatBuffer, formatBufferSize, format, arg);
	va_end(arg);

	// whether it was successful or not, or we need a different length
	if (result < 0) {
		// formatting error
		output = "formatting error or incorrect arguments: ";
		output += format;
	} else if (static_cast<size_t>(result) > formatBufferSize) {
		// space needed: result+1 (final \0)
		const size_t formatBuffer2Size = result+1;
		char formatBuffer2[formatBuffer2Size];
		formatBuffer2[0] = 0;

		va_list arg2;
		va_start(arg2, format);
		vsnprintf(formatBuffer2, formatBuffer2Size, format, arg2);
		va_end(arg2);

		output = formatBuffer2;
	} else {
		// ok
		output = formatBuffer;
	}

	return output;
}


/********************************************************************************
 * strTrim
 *******************************************************************************/
void strTrim(std::string& str)
{
	std::string::size_type index = str.find_last_not_of(" \t\n\r");
	if (index != std::string::npos) {
		str = str.substr(0, index+1);
	} else {
		// we only found blanks, we can safely quit
		str.clear();
		return;
	}

	index = str.find_first_not_of(" \t\n\r");
	if (index != std::string::npos) {
		str = str.substr(index, std::string::npos);
	}
}


NAMESPACE_END(AmbarMetta);
