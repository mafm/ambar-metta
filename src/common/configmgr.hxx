/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_CONFIGMGR_H__
#define __AMBARMETTA_COMMON_CONFIGMGR_H__


/** \file configmgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains a manager for configuration files, reading the <key,value>
 * pairs and storing them internally to serve them to other parts of the
 * application when requested.
 */


#include "common/patterns/singleton.hxx"

#include <map>

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** This class reads (and writes) configuration files, considering only the
 * lines containing "=" symbols, and tries to parse the key and the value, to
 * the left and the right sides of the equal symbol, removing blanks surrounding
 * the characters.  It stores the parsed key-value pairs so they can be
 * retrieved later, asking for the variable name (the "key" component).
 *
 * This class makes several assumptions, which will make the program to work
 * unreliably or abort if not met:
 *
 * - There is only one config file, so there are no ambiguities when it comes to
 * decide where to save variables.
 *
 * - Each key can be present only once, so we don't get conflicting values, and
 * when we overwrite the value we have only one place to do it.
 *
 * - Variables are never removed after being added, unless special methods to
 * reset the manager are called (in that case, all the variables are cleared).
 *
 * - To store a variable means to save it inmmediately to the file, and in the
 * runtime structure.  The variable must be present, that is, it only admits to
 * overwrite variables, not to append them.  So when storing a variable, it will
 * override the same value in the same line of the file -- no need to change the
 * layout of the file or touch the commentaries or blank lines, we just
 * substitute a line.
 *
 *
 * These impositions may seem too restrictive, but after the experience of using
 * a similar software components with lots of fancy functionalities, it became
 * clear that the added flexibility doesn't come to help, but to cause subtle
 * bugs: shadowing variables with new values defined in different places, saving
 * variables in different files (which leads to the previous problem), etc.
 * After all, having a few to a few dozen different variables should be enough,
 * and this hardly justifies to have complex schemes that bring headaches way
 * too often.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class ConfigMgr : public Singleton<ConfigMgr>
{
public:
	/** Load the given file, incorporating the key-value pairs found.  Only
	 * one file can be added, read the rationale behind this in the comments
	 * to the class.
	 *
	 * @param file Pathname of the file to be loaded
	 *
	 * @return whether the request could be performed or not.
	 */
	bool loadConfigFile(const std::string& file);

	/** Reset the state of the manager, useful if we need to restart the
	 * aplication or similar situations. */
	void reset();

	/** Get the variable with the given key, returning default value if not
	 * found
	 *
	 * @param key Key of the <key,value> pair
	 * @param defaultValue Default value to return (if the returned string
	 * is the same as defaultValue, the operation could not be performed)
	 *
	 * @return Value associated with key (defaultValue if not found)
	 */
	const std::string& getConfigVar(const std::string& key,
					const std::string& defaultValue) const;

	/** Store the given variable in the file and in the runtime structure,
	 * the variable must be in the file.  Read the comments to the class to
	 * understand the rationale behind this.
	 *
	 * @param key Key of the <key,value> pair
	 * @param value Value of the <key,value> pair
	 *
	 * @return whether the request could be performed or not.
	 */
	bool storeConfigVar(const std::string& key, const std::string& value);

private:
	/** Singleton friend access */
	friend class Singleton<ConfigMgr>;

	/// Name of the config file
	std::string _configFilename;

	/// Variables present
	std::map<std::string, std::string> _config;


	/** Default constructor */
	ConfigMgr();
	/** Destructor */
	~ConfigMgr();

	/** Parse a line.  When returning true, the varName and varValue
	 * parameters contain the values parsed: varName is the left part of the
	 * first '=' (stripping surrounding blanks), varValue is the right side
	 * (stripping surrounding blanks, too).
	 *
	 * @param line Line to parse
	 * @param key String to store the key of the <key,value> pair
	 * @param value String to store the value of the <key,value> pair
	 *
	 * @return true when it could get a <key,value> pair, false otherwise
	 */
	static bool parseLine(const std::string& line, std::string& key, std::string& value);
};


NAMESPACE_END(AmbarMetta);
#endif
