/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tablemgr.hxx"

#include "common/includes.hxx"

#include <cerrno>
#include <cfloat>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>


NAMESPACE_START(AmbarMetta);


static const std::string NOT_FOUND = "<not found>";


/********************************************************************************
 * Table
 *******************************************************************************/
Table::Table()
{
}

Table::Table(const std::vector<std::string>& rowNames,
	     const std::vector<std::string>& columnNames,
	     const std::vector<std::vector<std::string> >& content) :
	_rowNames(rowNames), _columnNames(columnNames), _content(content)
{
}

Table::~Table()
{
}

const std::string& Table::getValue(const std::string& row,
				   const std::string& column) const
{
/*
	const auto rowIt = _content[row];
	if (rowIt != _content.end()) {
		const auto colIt = it[column];
		if (colIt != rowIt->end()) {
			return *colIt;
		}
	}

	return NOT_FOUND;
*/
	return _content[0][0];
}


/********************************************************************************
 * TableMgr
 *******************************************************************************/
template <> TableMgr* Singleton<TableMgr>::INSTANCE = 0;

TableMgr::TableMgr()
{
}

TableMgr::~TableMgr()
{
}

bool TableMgr::loadTable(const std::string& tableName)
{
	std::string tablefilePath = std::string(GAME_TABLES_DIR) + "/" + tableName;

	std::vector<std::string> rowNames;
	std::vector<std::string> columnNames;
	std::vector<std::vector<std::string> > content;

	std::ifstream tablefile(tablefilePath);
	if (tablefile.is_open()) {
		std::string line;
		while (! tablefile.eof()) {
			std::getline(tablefile, line);
			std::vector<std::string> values;
			bool gotParsed = parseLine(line, values);
			if (!gotParsed) {
				continue;
			} else if (columnNames.empty()) {
				// first line: fill column names
				columnNames = values;
			} else {
				// first element: row name
				rowNames.push_back(values[0]);
				// rest of elements: contents
				values.erase(values.begin());
				content.push_back(values);
			}
		}
		tablefile.close();

		// add table to collection
		_tables[tableName]= Table(rowNames, columnNames, content);

		logINFO("Loaded table: '%s'", tableName.c_str());
		return true;
	} else {
		logERROR("Could not load table '%s': '%s'", tableName.c_str(), strerror(errno));
		return false;
	}
}

void TableMgr::reset()
{
	_tables.clear();
}

const std::string& TableMgr::getValue(const std::string& table,
				      const std::string& row,
				      const std::string& column) const
{
	const auto it = _tables.find(table);
	if (it != _tables.end()) {
		return it->second.getValue(row, column);
	} else {
		return NOT_FOUND;
	}
}

int TableMgr::getValueAsInt(const std::string& table,
			    const std::string& row,
			    const std::string& column) const
{
	std::string result = getValue(table, row, column);

	if (result == NOT_FOUND) {
		return INT_MIN;
	} else {
		return atoi(result.c_str());
	}
}

float TableMgr::getValueAsFloat(const std::string& table,
				const std::string& row,
				const std::string& column) const
{
	static const std::string notFound = "<not found>";
	std::string result = getValue(table, row, column);

	if (result == NOT_FOUND) {
		return FLT_MIN;
	} else {
		return atof(result.c_str());
	}
}

bool TableMgr::parseLine(const std::string& line, std::vector<std::string>& values)
{
	if (line[0] == '#') {
		// comment line
		return false;
	} else {
		std::string::size_type lastPos = 0;
		std::string::size_type tabPos = line.find('\t');
		do {
			std::string elem = line.substr(lastPos, tabPos);
			lastPos = tabPos;
			tabPos = line.find('\t');
			values.push_back(elem);
		} while (tabPos != std::string::npos);

		return true;
	}
}


NAMESPACE_END(AmbarMetta);
