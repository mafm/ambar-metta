/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_TABLEMGR_H__
#define __AMBARMETTA_COMMON_TABLEMGR_H__


/** \file tablemgr.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains a manager for tables.
 */


#include "common/patterns/singleton.hxx"

#include <map>
#include <vector>

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** This class represents a table read from a file
 *
 * It is matrix of strings.  Every row and every column has a name, and in this
 * way the elements can be indexed.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class Table
{
public:
	/** Default constructor */
	Table();

	/** Default constructor
	 *
	 * @param rowNames Names of the rows
	 *
	 * @param columnNames Names of the columnss
	 *
	 * @param content Content of the table
	 *
	 */
	Table(const std::vector<std::string>& rowNames,
	      const std::vector<std::string>& columnNames,
	      const std::vector<std::vector<std::string> >& content);

	/** Destructor */
	~Table();

	/** Get the value of the given (row, column)
	 *
	 * @param row The row of the table to get the value from
	 *
	 * @param column The column of the table to get the value from
	 *
	 * @return Value asked for
	 */
	const std::string& getValue(const std::string& row,
				    const std::string& column) const;

private:
	/// Row names
	std::vector<std::string> _rowNames;

	/// Column names
	std::vector<std::string> _columnNames;

	/// Content of the tables
	std::vector<std::vector<std::string> > _content;
};


/** This class reads (and writes) table files
 *
 * Table files have a special syntax, being a matrix of strings.  Every row is
 * separated by a newline character (that is, each line of a table file is a
 * row).  Every column within a row is separated by a tab.
 *
 * The first line contains the name of the column, while the first element of
 * each row is the name of the row.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class TableMgr : public Singleton<TableMgr>
{
public:
	/** Load the given table, incorporating a table with the content found.
	 *
	 * @param file Pathname of the file to be loaded
	 *
	 * @return whether the request could be performed or not.
	 */
	bool loadTable(const std::string& file);

	/** Reset the state of the manager, useful if we need to restart the
	 * aplication or similar situations. */
	void reset();

	/** Get the value of the given (row, column) of a table
	 *
	 * @param table The table to get the value from
	 *
	 * @param row The row of the table to get the value from
	 *
	 * @param column The column of the table to get the value from
	 *
	 * @return Value asked for
	 */
	const std::string& getValue(const std::string& table,
				    const std::string& row,
				    const std::string& column) const;

	/** Get the value of the given (row, column) of a table, as int
	 *
	 * @param table The table to get the value from
	 *
	 * @param row The row of the table to get the value from
	 *
	 * @param column The column of the table to get the value from
	 *
	 * @return Value asked for
	 */
	int getValueAsInt(const std::string& table,
			  const std::string& row,
			  const std::string& column) const;

	/** Get the value of the given (row, column) of a table, as float
	 *
	 * @param table The table to get the value from
	 *
	 * @param row The row of the table to get the value from
	 *
	 * @param column The column of the table to get the value from
	 *
	 * @return Value asked for
	 */
	float getValueAsFloat(const std::string& table,
			      const std::string& row,
			      const std::string& column) const;

private:
	/** Singleton friend access */
	friend class Singleton<TableMgr>;


	/// Variables present
	std::map<std::string, Table> _tables;


	/** Default constructor */
	TableMgr();
	/** Destructor */
	~TableMgr();

	/** Parse a line.  When returning true, the varName and varValue
	 * parameters contain the values parsed: varName is the left part of the
	 * first '=' (stripping surrounding blanks), varValue is the right side
	 * (stripping surrounding blanks, too).
	 *
	 * @param line Line to parse
	 * @param key String to store the key of the <key,value> pair
	 * @param value String to store the value of the <key,value> pair
	 *
	 * @return true when it could get a <key,value> pair, false otherwise
	 */
	static bool parseLine(const std::string& line, std::vector<std::string>& values);
};


NAMESPACE_END(AmbarMetta);
#endif
