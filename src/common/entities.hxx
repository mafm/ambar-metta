/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_ENTITIES_H__
#define __AMBARMETTA_COMMON_ENTITIES_H__


/** \file entities.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * This file contains classes related to entities.
 */


#include <cstdint>

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** Entity ID type, with methods to convert between numerical and string
 * representation.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class EntityID
{
public:
	/// Value type (for numerical representation)
	typedef uint64_t value_type;

	/// Numerical representation
	const value_type id;
	/// String representation
	const std::string idStr;


	/** Constructor for copies and so on */
	EntityID();
	/** Constructor with string representation */
	EntityID(const std::string& i);
	/** Constructor with numerical representation */
	EntityID(value_type i);

	/** Operator to compare to objects of the same class */
	bool operator == (const EntityID& other) const;
	/** Operator to compare to objects of the same class */
	bool operator != (const EntityID& other) const;

	/** String to numerical representation */
	static value_type toNumerical(const std::string& id);
	/** Numerical to string representation */
	static std::string toString(value_type id);
};


/** Representation of an item in the inventory
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class InventoryItem
{
public:
	/** Enum for item inventory type, it might be used for filtering or to
	 * enable different actions when selecting them. */
	enum class InventoryType { Equip, Consum, Raw, Other };


	/// Item identifier
	EntityID::value_type entityID;

	/// Item mesh type
	std::string meshType;
	/// Item mesh subtype
	std::string meshSubtype;

	/// Inventory type
	InventoryType invType;

	/// Load
	float load;


	/** Constructor: empty */
	InventoryItem();
	/** Constructor: initialize values */
	InventoryItem(EntityID::value_type id,
		      const std::string& type,
		      const std::string& subtype,
		      float load);

	/// Set item ID
	void setItemID(const std::string& id);
	/// Set item ID
	void setItemID(EntityID::value_type id);
	/// Get item ID
	EntityID::value_type getItemID() const;
	/// Get item ID as string
	const char* getItemIDAsStr() const;
	/// Set mesh type
	void setType(const std::string& type);
	/// Get mesh type
	const char* getType() const;
	/// Set mesh subtype
	void setSubtype(const std::string& subtype);
	/// Get mesh subtype
	const char* getSubtype() const;
	/// Set load
	void setLoad(float load);
	/// Get load
	float getLoad() const;

	/** Get entity ID */
	EntityID::value_type getEntityID() const;
};


NAMESPACE_END(AmbarMetta);
#endif
