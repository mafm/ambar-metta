/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_COMMAND_H__
#define __AMBARMETTA_COMMON_COMMAND_H__


/** \file command.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains the base for the use of commands and command managers.
 *
 * Typically, programs of the project (e.g. client, server) would derive and
 * instantiate these classes to create their own command managers and commands.
 * Those programs then can set some way to get input and show output (a kind of
 * console) and let the user behind to manipulate the program via these
 * commands.
 *
 * Typical uses for this are to control the server to know about parameters
 * (users connected, state of the system as a whole) or perform actions
 * (e.g. reload configuration, quit remotely without access to the local
 * terminal) without having to re-start the process with a different
 * instructions.
 *
 * In the client this is not as necessary since the control can be made through
 * the GUI, but some of these commands are much easier to implement than GUI
 * counterparts (specially if they're not used often so doesn't pay too much to
 * create GUI for them), provide a method to interact with the client itself
 * using keyboard and not mouse, or perform remote actions on the server
 * (provided that the user has proper permissions to manipulate the server).
 *
 */


#include <cstdarg>
#include <vector>
#include <map>

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** Output for a command
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CommandOutput
{
public:
	/** Append given string to the output of a command (adding "\n")
	 *
	 * @param line Line to be appended to the output
	 */
	void appendLine(const std::string& line);

	/** Get the output of a command
	 *
	 * @return The whole output accumulated so far
	 */
	const std::string& getString() const;

private:
	/** Pool of text to send */
	std::string _textPool;
};



/** A base class of a command.
 *
 *  The concrete class needs to define the execute() method, and also create a
 * constructor to call the constructor of the base class with the appropriate
 * parameters (constructors themselves cannot be virtual).
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class Command
{
public:
	/** The different permission levels the users of the commands, and which
	 * level the command needs to be executed */
	enum PermissionLevel : int { NotSet = 0, Player, Admin };


	/** Constructor with some basics needed when creating any command.
	 *
	 * @param level Level needed to execute the command
	 * @param name Command name
	 * @param descr One-line description of the command
	 */
	Command(PermissionLevel level,
		const char* name,
		const char* descr);

	/** Destructor */
	virtual ~Command() { }

	/** The name of the command */
	const char* getName() const;

	/** Get a one-line description of the command */
	const char* getDescription() const;

	/** Get the name of the arguments
	 *
	 * @return Structure with the argument names
	 */
	const std::vector<std::string>& getArgNames() const;

	/** Check for permissions (true if allowed to execute the command with
	 * the given level)
	 *
	 * @param level Permission level of the user who executes the command
	 *
	 * @return Whether the permission check was successful
	 */
	bool permissionAllowed(PermissionLevel level) const;

	/** Execute the command (fake, calls the private function of the same
	 * name that will be defined in the derived classes, and performs the
	 * permission check and maybe other)
	 *
	 * @param args Arguments for the command
	 * @param level Permission level of the user who executes the command
	 * @param output Place to be added the output that we'll show
	 */
	void execute(std::vector<std::string>& args,
		     PermissionLevel level,
		     CommandOutput& output) const;

protected:
	/** The name of the command */
	std::string _name;

	/** Description, one line only if possible */
	std::string _descr;

	/** The names of the arguments */
	std::vector<std::string> _argNames;

	/** Permission required to use the command */
	PermissionLevel _permNeeded;

private:
	/** Execute the command
	 *
	 * @param args Arguments for the command
	 * @param output Place to be added the output that we'll show
	 */
	virtual void execute(std::vector<std::string>& args,
			     CommandOutput& output) const = 0;
};


/** A command manager, meant to be used in client and server, each one with a
 * unique set of commands.  It has to be completed to be instantiated.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CommandMgr
{
public:
	/** Execute the given command line, with given permission, and add the
	 * result to the given output.
	 *
	 * @param commandName Help about this command in particular
	 * @param level Permission level of the calling user
	 * @param output Place to be added the output that we'll show
	 */
	void execute(const char* commandLine,
		     Command::PermissionLevel level,
		     CommandOutput& output) const;

	/** Show global help
	 *
	 * @param level Permission level of the calling user
	 * @param output Place to be added the output that we'll show
	 */
	void showHelp(Command::PermissionLevel level, CommandOutput& output) const;

	/** Show help about the given command
	 *
	 * @param commandName Help about this command in particular
	 * @param level Permission level of the calling user
	 * @param output Place to be added the output that we'll show
	 */
	void showHelp(const std::string& commandName,
		      Command::PermissionLevel level,
		      CommandOutput& output) const;

protected:
	/** Default constructor */
	CommandMgr();
	/** Destructor */
	virtual ~CommandMgr();

	/** Abstract method which has to be defined in client and server, to
	 * create and register the commands.
	 *
	 * The commands are removed in the destructor of this class, for
	 * convenience.
	 */
	virtual void registerCommands() = 0;

	/** Add a command (accesed by the registerCommands method)
	 *
	 * @param command The command to be added
	 */
	void addCommand(Command* command);

private:
	/** The commands in this set */
	std::map<std::string, Command*> _commandList;


	/** Find a command by name
	 *
	 * @param commandName The name of the command
	 *
	 * @return 0 if not found
	 */
	const Command* findCommand(const std::string& commandName) const;

	/** Parse command line, putting each piece into the list of arguments
	 *
	 * @param commandLine The command line
	 * @param args Array of arguments where to store the parsed ones
	 */
	static void parseCommandLine(const std::string& commandLine,
				     std::vector<std::string>& args);
};


NAMESPACE_END(AmbarMetta);
#endif
