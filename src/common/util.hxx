/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_UTIL_H__
#define __AMBARMETTA_COMMON_UTIL_H__


/** \file util.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains several utility functions, data structures or algorithms,
 * used everywhere in the code and generally unclassifiable under a common
 * class.
 */

#include <exception>

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** Exception base class for the rest of exceptions in the library
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class Exception : public std::exception {
public:
	/** Default constructor */
	Exception() throw() { }

	/** Constructor
	 *
	 * @param message Message to be displayed
	 */
	Exception(const std::string& message) throw() : _message(message) { }

	/** Destructor */
	virtual ~Exception() throw() { }

	/** @see std::exception::what() */
	virtual const char* what() const throw() {
		return _message.c_str();
	}

protected:
	/** Variable to store a message */
	std::string _message;
};


/** Format a string as in s[n]printf, but without limitations (buffer overruns,
 * fixed length...).
 *
 * C++ classes and methods for printing are not as clean as this solution,
 * although they have other advantages; the main purpose of this function is
 * printf-like clarity.
 *
 * \note This takes advantage of the optimization called Return-Value
 * Optimization, RVO (G++ name: elide-constructors), so the string is not
 * constructed twice before returning.  This way the caller can use a simpler
 * syntax and yet have no penalties.
 *
 * @param format Format of the string
 *
 * @return String with all the information included
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
std::string strFmt(const char* format, ...) __attribute__((format(printf, 1, 2)));


/** Trim function (remove blank space from both sides of the string)
 *
 * @param str String to trim
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
void strTrim(std::string& str);


NAMESPACE_END(AmbarMetta);
#endif
