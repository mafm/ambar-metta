/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entities.hxx"

#include "common/includes.hxx"


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * EntityID
 *******************************************************************************/
EntityID::EntityID() :
	id(0)
{
}

EntityID::EntityID(const std::string& i) :
	id(toNumerical(i)), idStr(i)
{
}

EntityID::EntityID(EntityID::value_type i) :
	id(i), idStr(toString(i))
{
}

EntityID::value_type EntityID::toNumerical(const std::string& id)
{
	return strtoull(id.c_str(), NULL, 10);
}

std::string EntityID::toString(EntityID::value_type id)
{
	return strFmt("%llu", id);
}

bool EntityID::operator==(const EntityID& other) const
{
	return (id == other.id);
}

bool EntityID::operator!=(const EntityID& other) const
{
	return (id != other.id);
}


/********************************************************************************
 * InventoryItem
 *******************************************************************************/
InventoryItem::InventoryItem() :
	entityID(0), load(-1.0f)
{
}

InventoryItem::InventoryItem(EntityID::value_type id, const std::string& type, const std::string& subtype, float l) :
	entityID(id), meshType(type), meshSubtype(subtype), load(l)
{
}

EntityID::value_type InventoryItem::getItemID() const
{
	return entityID;
}

const char* InventoryItem::getItemIDAsStr() const
{
	return EntityID::toString(entityID).c_str();
}

void InventoryItem::setItemID(const std::string& id)
{
	entityID = EntityID::toNumerical(id);
}

void InventoryItem::setItemID(EntityID::value_type id)
{
	entityID = id;
}

const char* InventoryItem::getType() const
{
	return meshType.c_str();
}

void InventoryItem::setType(const std::string& type)
{
	meshType = type;
}

const char* InventoryItem::getSubtype() const
{
	return meshSubtype.c_str();
}

void InventoryItem::setSubtype(const std::string& subtype)
{
	meshSubtype = subtype;
}

float InventoryItem::getLoad() const
{
	return load;
}

void InventoryItem::setLoad(float _load)
{
	load = _load;
}

EntityID::value_type InventoryItem::getEntityID() const
{
	return entityID;
}


NAMESPACE_END(AmbarMetta);
