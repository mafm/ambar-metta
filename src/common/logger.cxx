/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "logger.hxx"

#include "common/includes.hxx"

#include <cstdarg>
#include <cstdio>
#include <ctime>


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * LogMgr
 *******************************************************************************/
const size_t TIMESTAMP_LENGTH = sizeof("YYYYmmdd HH:MM:SS");


template <> LogMgr* Singleton<LogMgr>::INSTANCE = 0;

LogMgr::LogMgr() :
	_logLevel(Severity::DEBUG)
{
}

LogMgr::~LogMgr()
{
}

void LogMgr::log(Severity severity, const char* file, int line, const char* format, ...) const
{
	if (severity >= _logLevel) {
		// log as '<timestamp> :: <severity> :: <file>:<line> :: <message>'
		char ts[TIMESTAMP_LENGTH];
		time_t now = time(0);
		strftime(ts, sizeof(ts), "%Y%m%d %H:%M:%S", localtime(&now));

		// print only filename, not full path
		std::string basename = file;
		basename = basename.substr(basename.find_last_of("/")+1);

		fprintf(stderr, "%s :: %s :: %s:%d :: ",
			ts, levelToString(severity), basename.c_str(), line);
		va_list arg;
		va_start(arg, format);
		vfprintf(stderr, format, arg);
		va_end(arg);

		fprintf(stderr, "\n");
		// mafm: is this necessary? anyway, it doesn't harm
		fflush(stderr);
	}
}

void LogMgr::log(Severity severity, const char* file, int line, const std::string& msg) const
{
	log(severity, file, line, msg.c_str());
}

void LogMgr::setLogMsgLevel(Severity level)
{
	_logLevel = level;
}

bool LogMgr::setLogMsgLevel(const std::string& levelStr)
{
	if (levelStr == "DEBUG") {
		_logLevel = Severity::DEBUG;
		return true;
	} else if (levelStr == "INFO") {
		_logLevel = Severity::INFO;
		return true;
	} else if (levelStr == "WARNING") {
		_logLevel = Severity::WARNING;
		return true;
	} else if (levelStr == "ERROR") {
		_logLevel = Severity::ERROR;
		return true;
	} else if (levelStr == "FATAL") {
		_logLevel = Severity::FATAL;
		return true;
	} else {
		return false;
	}
}

const char* LogMgr::levelToString(Severity level)
{
	switch (level) {
	case Severity::DEBUG:
		return "DEBUG";
	case Severity::INFO:
		return "INFO";
	case Severity::WARNING:
		return "WARNING";
	case Severity::ERROR:
		return "ERROR";
	case Severity::FATAL:
		return "FATAL";
	default:
		// this case should not appear
		return "INVALID LOG LEVEL";
	}
}


NAMESPACE_END(AmbarMetta);
