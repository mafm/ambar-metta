/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "observer.hxx"

#include "common/includes.hxx"


NAMESPACE_START(AmbarMetta);


/*******************************************************************************
 * Observer
 ******************************************************************************/
Observer::Observer(const std::string& name) :
	_name(name)
{
}

Observer::~Observer()
{
	logDEBUG("Observer::~Observer() start");

	// Observable::detachObserver() calls back to ::detachedFromObservable()
	// and a race condition is raised accessing the list, so we have to ask
	// for detaching with no call back
	for (auto it = _observables.begin(); it != _observables.end(); ++it) {
		(*it)->detachObserver(this, false); // don't call back
		it = _observables.erase(it);
	}

	logDEBUG("Observer::~Observer() end");
}

void Observer::attachedToObservable(Observable* observable)
{
	_observables.push_back(observable);
}

void Observer::detachedFromObservable(Observable* observable)
{
	_observables.remove(observable);
}


/*******************************************************************************
 * Observable
 ******************************************************************************/
Observable::~Observable()
{
	logDEBUG("Observable::~Observable() start");
	detachAllObservers();
	logDEBUG("Observable::~Observable() end");
}

void Observable::attachObserver(Observer* observer)
{
	observer->attachedToObservable(this);
	_observers.push_back(observer);
	onAttachObserver(observer);
}

void Observable::detachObserver(Observer* observer, bool callback)
{
	if (callback) {
		onDetachObserver(observer);
		observer->detachedFromObservable(this);
	}
	_observers.remove(observer);
}

void Observable::detachAllObservers()
{
	for (auto it = _observers.begin(); it != _observers.end(); ++it) {
		onDetachObserver(*it);
		(*it)->detachedFromObservable(this);
		it = _observers.erase(it);
	}
}

void Observable::notifyObservers(const ObserverEvent& event)
{
	for (auto it = _observers.begin(); it != _observers.end(); ++it) {
		(*it)->updateFromObservable(event);
	}
}


NAMESPACE_END(AmbarMetta);
