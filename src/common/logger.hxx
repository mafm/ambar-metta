/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_LOGGER_H__
#define __AMBARMETTA_COMMON_LOGGER_H__


/** \file logger.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file has all needed elements to write log entries. It might be more
 * elegant to expose log API with a singleton class only; but macros save a lot
 * of typing (it's used everywhere, obviously).
 *
 * The way to write log entries is to call the macros defined below as a
 * printf-like functions (whitout ending newline), which in turn will call
 * LogMgr::log(), but supplying a variety of parameters needed to ease the
 * burden of the programmers.
 *
 * Examples:
 *
 * logDEBUG("Error when parsing file: '%s'", errorMsg);
 * logWARNING("Could not connect to server %s port %d", server, port);
 * logFATAL("Could not allocate memory");
 */


#include "common/patterns/singleton.hxx"

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** Log a debug message */
#define logDEBUG(...)	LogMgr::instance().log(LogMgr::Severity::DEBUG, __FILE__, __LINE__, __VA_ARGS__)
/** Log a info message */
#define logINFO(...)	LogMgr::instance().log(LogMgr::Severity::INFO, __FILE__, __LINE__, __VA_ARGS__)
/** Log a warning message */
#define logWARNING(...)	LogMgr::instance().log(LogMgr::Severity::WARNING, __FILE__, __LINE__, __VA_ARGS__)
/** Log a error message */
#define logERROR(...)	LogMgr::instance().log(LogMgr::Severity::ERROR, __FILE__, __LINE__, __VA_ARGS__)
/** Log a fatal message */
#define logFATAL(...)	LogMgr::instance().log(LogMgr::Severity::FATAL, __FILE__, __LINE__, __VA_ARGS__)


/** This class governs the logging actions.  In principle it shouldn't be called
 * directly, but through the macros acting as shortcuts.
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class LogMgr : public Singleton<LogMgr>
{
public:
	/** The different levels of the log message */
	enum Severity : int { DEBUG = 1, INFO, WARNING, ERROR, FATAL };

	/** Log a message with printf-like syxtax
	 *
	 * @param severity Severity level of the message to be logged
	 * @param file File of the originating call
	 * @param line Line of the originating call
	 * @param format Format message to be logged
	 */
	void log(Severity severity, const char* file, int line, const char* format, ...) const
		__attribute__((format(printf, 5, 6))); // 5 and 6 because 1st parameter is hidden *this

	/** Log a message without special formatting
	 *
	 * @param severity Severity level of the message to be logged
	 * @param file File of the originating call
	 * @param line Line of the originating call
	 * @param msg Formatted message
	 */
	void log(Severity severity, const char* file, int line, const std::string& msg) const;

	/**
	 * Set the level of the log messages to be sent to output
	 *
	 * @param level Lowest severity level to be logged
	 */
	void setLogMsgLevel(Severity level);
	/**
	 * Set the level of the log messages to be sent to output.  This should
	 * usually be done when being set comming from a console or so,
	 * otherwise the non-string version is preferred.
	 *
	 * @param level Lowest severity level to be logged
	 * @return whether the action was successful or not
	 */
	bool setLogMsgLevel(const std::string& levelStr);

private:
	/** Singleton friend access */
	friend class Singleton<LogMgr>;


	/** Log severity level the instance will respect */
	Severity _logLevel;


	/** Default Constructor */
	LogMgr();
	/** Destructor */
	~LogMgr();

	/** Translate log severity level to string, e.g. to print it */
	static const char* levelToString(Severity level);
};


NAMESPACE_END(AmbarMetta);
#endif
