/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "configmgr.hxx"

#include "common/includes.hxx"

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * ConfigMgr
 *******************************************************************************/
template <> ConfigMgr* Singleton<ConfigMgr>::INSTANCE = 0;

ConfigMgr::ConfigMgr()
{
}

ConfigMgr::~ConfigMgr()
{
}

bool ConfigMgr::loadConfigFile(const std::string& file)
{
	// check whether we already have the config loaded
	if (!_config.empty()) {
		logERROR("Config already loaded, refusing to load it again");
		return false;
	}

	_configFilename = file;
	std::ifstream cfgfile(file);
	if (cfgfile.is_open()) {
		std::string line;
		while (! cfgfile.eof()) {
			std::getline(cfgfile, line);
			std::string key, value;
			bool gotParsed = parseLine(line, key, value);
			if (!gotParsed) {
				continue;
			} else {
				// insert into the storage
				if (_config.find(key) == _config.end()) {
					_config[key] = value;
				} else {
					logERROR("Config: key '%s' already present (%s), ignore it",
						 key.c_str(), (*_config.find(key)).second.c_str());
					continue;
				}
			}
		}
		cfgfile.close();
		logINFO("Loaded config file: '%s'", file.c_str());
		return true;
	} else {
		logERROR("Could not load config: '%s'", strerror(errno));
		return false;
	}
}

void ConfigMgr::reset()
{
	_config.clear();
}

const std::string& ConfigMgr::getConfigVar(const std::string& key, const std::string& defaultValue) const
{
	const auto it = _config.find(key);
	if (it != _config.end()) {
		return it->second;
	} else {
		return defaultValue;
	}
}

bool ConfigMgr::storeConfigVar(const std::string& key, const std::string& value)
{
	// check whether the variable is present
	const std::string& result = getConfigVar(key, "<does not exist>");
	if (result == "<does not exist>") {
		logERROR("Couldn't found key '%s' in file (refusing to add new keys, can only overwrite)",
			 key.c_str());
		return false;
	} else if (result == value) {
		// silently avoiding to overwrite
		logDEBUG("Key '%s' already has the intended value '%s' (ignoring unnecessary overwrite)",
			 key.c_str(), value.c_str());
		return true;
	}

	// retrieving the source file, line by line
	std::vector<std::string> sourceLines;
	std::ifstream cfgfile(_configFilename.c_str(), std::ios::in);
	if (cfgfile.is_open()) {
		std::string line;
		while (! cfgfile.eof()) {
			std::getline(cfgfile, line);
			strTrim(line); // trim so we don't get unwanted blanks
			sourceLines.push_back(line);
		}
		cfgfile.close();
	} else {
		logERROR("Couldn't open config to store key: '%s'", strerror(errno));
		return false;
	}

	// substituting the line
	bool substituted = false;
	for (auto it = sourceLines.begin(); it != sourceLines.end(); ++it) {
		// replacing it only when the key is at position zero in the
		// line (i.e., it's not a comment)
		std::string::size_type pos = it->find(key);
		if (pos == 0) {
			logDEBUG("Found '%s' in line '%s', substituting key",
				 key.c_str(), it->c_str());
			(*it) = key + std::string(" = ") + value;
			substituted = true;
			break;
		}
	}
	if (!substituted) {
		logERROR("Couldn't find key '%s' in the config file, despite being in the config",
			 key.c_str());
		return false;
	}

	// writing the updated set of lines back to the file
	std::ofstream cfgfileWrite(_configFilename.c_str(), std::ios::trunc | std::ios::out);
	if (cfgfileWrite.is_open()) {
		for (auto it = sourceLines.begin(); it != sourceLines.end(); ++it) {
			cfgfileWrite << *it << std::endl;
		}
		cfgfileWrite.close();
		return true;
	} else {
		logERROR("Couldn't open config to store a var: %s", strerror(errno));
		return false;
	}
}

bool ConfigMgr::parseLine(const std::string& line, std::string& key, std::string& value)
{
	std::string::size_type eqPosition = line.find('=');
	if (line[0] == '#' || eqPosition == line.npos) {
		// doesn't contain '=' or is a comment (starts with #), ignore
		return false;
	} else {
		// parse key and value parts and store them
		key = line.substr(0, eqPosition);
		value = line.substr(eqPosition+1, line.length());

		//trim
		strTrim(key);
		strTrim(value);

		// logDEBUG("CFG PARSED: key='%s', value='%s'", key.c_str(), value.c_str());
		return true;
	}
}


NAMESPACE_END(AmbarMetta);
