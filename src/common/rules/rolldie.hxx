/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_RULES_ROLLDIE_H__
#define __AMBARMETTA_COMMON_RULES_ROLLDIE_H__


/** \file rolldie.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This file contains classes to perform rolls of dies.
 */


#include "common/patterns/singleton.hxx"
#include "common/util.hxx"

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/**
 * This class performs dice rolls based on randomization, given strings in valid
 * format such as '3d20+4'.
 *
 * The valid string is, in regex: ([1-9][0-9]*)d([1-9][0-9]*)([+-][0-9]+)?
 *
 * The first part is a positive non-zero integer indicating the number of times
 * to roll, followed by a 'd' and another positive non-zero integer to indicate
 * the die size (as is common to denominate them in RPGs, such as d12 or d20),
 * followed by an *optional* modifier (either not present, or +integer, or
 * -integer), that it's to be added to the result.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class RollDie : public Singleton<RollDie>
{
public:
	/** Exception */
	class RollDieException : public Exception {
	public:
		/** @see Exception::Exception() */
		RollDieException(const std::string& dieString,
				 std::string::size_type position,
				 const std::string& message);
	};

	/**
	 * Roll with a given string such as '3d20+4'
	 *
	 * @param dieString a valid dice rolling string
	 *
	 * @return the sum of the dice values once they've been modified
	 */
	int roll(const std::string& dieString) const throw(RollDieException);

private:
	/** Singleton friend access */
	friend class Singleton<RollDie>;

	/** Internal class, forward declaration */
	class RollData;


	/** Default constructor */
	RollDie();
	/** Destructor */
	~RollDie();

	/**
	 * Parse a string of the form '3d20+4'.
	 *
	 * @param dieString the string you want to parse
	 *
	 * @param rollData the object which will contains the 3 parsed elements
	 * found in the dieString
	 *
	 * @return true if the string has been parsed
	 */
	static void parseString(const std::string& dieString, RollData& rollData) throw(RollDieException);
};


NAMESPACE_END(AmbarMetta);
#endif
