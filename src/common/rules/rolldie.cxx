/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rolldie.hxx"

#include "common/includes.hxx"

#include <cstdlib>
#include <sys/time.h>


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * RollDie::RollData
 *******************************************************************************/
/**
 * Structure to hold data representing roll dices including modifiers, such as
 * '3d20+4' (3 times, size 20, modifier +4)
 */
class RollDie::RollData
{
public:
	/** Number of times to roll */
	int times;

	/** Die size */
	int die;

	/** Modifier to the random result */
	int modifier;


	/** Default constructor */
	RollData() : times(0), die(0), modifier(0) { }

	/** Whether it's valid or not */
	bool isValid() const {
		return (times > 0) && (die > 0);
	}

	/** Convert to string representation */
	std::string toString() const {
		std::string s = strFmt("%dd%d", times, die);

		if (modifier > 0) {
			s += strFmt("+%d", modifier);
		} else if (modifier < 0) {
			s += strFmt("%d", modifier); // '-' sign already included
		} else {
			// don't add anything on zero
		}

		return s;
	}
};


/********************************************************************************
 * RollDie::RollDieException
 *******************************************************************************/
RollDie::RollDieException::RollDieException(const std::string& dieString,
					    std::string::size_type position,
					    const std::string& message)
{
	_message = strFmt("RollDieException: '%s' at %zu: %s",
			  dieString.c_str(), position, message.c_str());
}


/********************************************************************************
 * RollDie
 *******************************************************************************/
template <> RollDie* Singleton<RollDie>::INSTANCE = 0;

RollDie::RollDie()
{
	// seed the rng: with seconds is not very trustable, since players can
	// guess it by the uptime of the program
	struct timeval now;
	gettimeofday(&now, 0);
	srandom(now.tv_sec ^ now.tv_usec);
}

RollDie::~RollDie()
{
}

int RollDie::roll(const std::string& dieString) const throw(RollDieException)
{
	// parse the string and get the result
	RollData rollData;
	try {
		parseString(dieString, rollData);
	} catch (RollDieException& e) {
		throw e;
	}

	// calculate
	int count = 0;
	for (int i = 0; i < rollData.times; ++i) {
		count += (random() % rollData.die) + 1;
	}

	return (count + rollData.modifier);
}

void RollDie::parseString(const std::string& dieString, RollData& rollData) throw(RollDieException)
{
	/* mafm: with C++0x regex, tentative implementation

	// example of valid expression: '3d12+5', or digit+ d digit+
	// ([+-]digit+)? ; that is: one or more digits (number of rolls),
	// followed by d, followed again by one or more digits (dice size), and
	// optionaly + or - followed by digits (modifier)
	std::regex valid_regex("([1-9][0-9]*)d([1-9][0-9]*)([+-][0-9]+)?");

	if (!std::regex_match(dieString.begin(), dieString.end(), valid_regex)) {
		// doesn't match the regex, not valid
		return false;
	} else {
		std::cmatch tokens;
		std::regex_search(dieString.c_str(), tokens, valid_regex);
		rollData.times = std::atoi(tokens[1].str().c_str());
		rollData.die = std::atoi(tokens[2].str().c_str());
		rollData.modifier = std::atoi(tokens[3].str().c_str());
	}
	*/

	// position of the first 'd' token in the original string
	std::string::size_type dPos = dieString.find_first_not_of("0123456789");
	if (dieString[dPos] != 'd') {
		throw RollDieException(dieString, dPos,
				       "Couldn't find an integer followed by 'd'");
	}

	// position of the modifier token in the original string (might be
	// string::npos)
	std::string::size_type modifierPos = dieString.find_first_not_of("0123456789", dPos+1);
	if (modifierPos != std::string::npos) {
		if (dieString[modifierPos] != '+' && dieString[modifierPos] != '-') {
			throw RollDieException(dieString, modifierPos,
					       "Couldn't find modifier sign (or not in proper place)");
		}
		// position of extraneous symbols in the string
		std::string::size_type extraPos = dieString.find_first_not_of("0123456789", modifierPos+1);
		if (extraPos != std::string::npos) {
			throw RollDieException(dieString, extraPos,
					       "Found extraneous/forbidden characters");
		}
	}

        // Number of times the dice is beeing rolled: _3_d20+4
	rollData.times = std::atoi(dieString.substr(0, dPos).c_str());
        // Die size: 3d_20_+4
	rollData.die = std::atoi(dieString.substr(dPos+1, modifierPos).c_str());
        // Optional modifier: 3d20_+4_
	if (modifierPos != std::string::npos) {
		rollData.modifier = std::atoi(dieString.substr(modifierPos+1).c_str());
	}

	// sanity check, mostlly that time and dice size numbers were > 0
	if (!rollData.isValid()) {
		throw RollDieException(dieString, dieString.length(),
				       "Parsed data not valid, interpreted rolldie is '" + rollData.toString() + "'");
	}
}


NAMESPACE_END(AmbarMetta);
