/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_DATATYPES_H__
#define __AMBARMETTA_COMMON_DATATYPES_H__


/** \file datatypes.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * This file contains different simple and widely-used datatypes or classes,
 * usually shared between client and server, that we define here for
 * convenience.
 */


#include <cstdint>

#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** Minimal representation of a vector of 3 elements when we don't want to
 * depend on external libraries, e.g. for network messages.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class Vector3
{
public:
	/// Component
	float x;
	/// Component
	float y;
	/// Component
	float z;

	/** Constructor: initialize to zero */
	Vector3() : x(0.0f), y(0.0f), z(0.0f) { }
	/** Constructor: initialize to given values */
	Vector3(float X, float Y, float Z);

	/** Operation */
	Vector3 operator+(const Vector3& u) const;
	/** Operation */
	Vector3 operator-(const Vector3& u) const;
	/** Operation */
	Vector3 operator-() const;
	/** Operation */
	float operator*(const Vector3& u) const;
	/** Operation */
	Vector3 operator/(float f) const;
	/** Operation */
	float magnitude() const;
	/** Operation */
	float distance(const Vector3& u) const;
	/** Operation */
	void normalize();

private:
	/// Error tolerance
	static const float TOLERANCE;

	/** Check limits and correct values.  After each operation, if the
	 * variables are below some sane limits, they are corrected. */
	void checkLimits();
};


/** Name-value pair, simple container
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class NameValuePair
{
public:
	/// First element of the pair, name
	std::string name;
	/// Second element of the pair, value
	std::string value;


	/** Constructor to create an empty structure */
	NameValuePair() { }

	/** Constructor to create an initialized pair */
	NameValuePair(const std::string& n, const std::string& v) :
		name(n), value(v) { }
};


/** Compound type to send info about characters
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class CharacterListEntry
{
public:
	/// Component
	std::string name;
	/// Component
	std::string race;
	/// Component
	std::string gender;
	/// Component
	std::string playerClass;
	/// Component
	std::string area;
};


NAMESPACE_END(AmbarMetta);
#endif
