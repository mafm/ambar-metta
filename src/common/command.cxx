/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "command.hxx"

#include "common/includes.hxx"


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * CommandOutput
 *******************************************************************************/
void CommandOutput::appendLine(const std::string& line)
{
	if (!_textPool.empty()) {
		_textPool.append("\n");
	}
	_textPool.append(line);
}

const std::string& CommandOutput::getString() const
{
	return _textPool;
}


/********************************************************************************
 * Command
 *******************************************************************************/
Command::Command(PermissionLevel level,
		 const char* name,
		 const char* descr) :
	_name(name), _descr(descr), _permNeeded(level)
{
}

const char* Command::getName() const
{
	return _name.c_str();
}

const char* Command::getDescription() const
{
	return _descr.c_str();
}

const std::vector<std::string>& Command::getArgNames() const
{
	return _argNames;
}

bool Command::permissionAllowed(PermissionLevel level) const
{
	return (_permNeeded <= level);
}

void Command::execute(std::vector<std::string>& args,
		      PermissionLevel level,
		      CommandOutput& output) const
{
	// check for permissions
	if (!permissionAllowed(level)) {
		const std::string& msg = strFmt("Insufficient permissions to execute command '%s'.",
						_name.c_str());
		output.appendLine(msg.c_str());
		return;
	}

	// now execute the command, defined in derived classes
	execute(args, output);
}


/********************************************************************************
 * CommandMgr
 *******************************************************************************/
CommandMgr::CommandMgr()
{
}

CommandMgr::~CommandMgr()
{
	for (auto it = _commandList.begin(); it != _commandList.end(); ++it) {
		delete (*it).second;
	}
}

void CommandMgr::addCommand(Command* command)
{
	std::string commandName(command->getName());
	if (findCommand(commandName)) {
		logWARNING("CommandMgr: Adding command to set: '%s' already exists",
			   commandName.c_str());
	} else {
		_commandList[commandName] = command;
	}
}

const Command* CommandMgr::findCommand(const std::string& commandName) const
{
	// the <map> returns the last element when the element with the key is
	// not found, so we have to make this to behave like usual functions and
	// not to do this special check on the caller.
	//
	// IMPORTANT: note that map[] returns the element when it *exists*, but
	// otherwise creates a new entry with an empty value, so be careful with
	// this
	if (_commandList.find(commandName) == _commandList.end()) {
		return 0;
	} else {
		return (*_commandList.find(commandName)).second;
	}
}

void CommandMgr::parseCommandLine(const std::string& commandLine, std::vector<std::string>& args)
{
	std::string cmdLine(commandLine);

	// tokenize the string into arguments
	while (cmdLine.length() > 0) {
		// remove whitespace around
		strTrim(cmdLine);
		//logDEBUG("cL before tokenizing: '%s'", cmdLine.c_str());

		std::string::size_type pos = cmdLine.find_first_of(' ');
		if (pos != std::string::npos) {
			// there's some space: several arguments
			std::string newArg = cmdLine.substr(0, pos);
			args.push_back(newArg);
			cmdLine = cmdLine.substr(pos, cmdLine.length());
			//logDEBUG("Argument recognized: '%s'", newArg.c_str());
		} else {
			// no spaces left, last argument
			args.push_back(cmdLine);
			break;
		}
	}
}

void CommandMgr::execute(const char* commandLine,
			 Command::PermissionLevel level,
			 CommandOutput& output) const
{
	// try to parse the command line into arguments
	std::vector<std::string> args;
	parseCommandLine(commandLine, args);
	if (args.size() == 0) {
		const std::string& msg = strFmt("Cannot parse command line: '%s'", commandLine);
		output.appendLine(msg);
		return;
	}

	// pop the command name
	std::string commandName = args[0];
	args.erase(args.begin());

	// we handle here the /help meta command
	if (commandName == "help") {
		if (args.size() == 0) {
			// general help
			showHelp(level, output);
		} else {
			// help about command, only considering one argument
			showHelp(args[0], level, output);
		}
		return;
	}

	// search for a "real" command
	const Command* command = findCommand(commandName);
	if (!command) {
		const std::string& msg = strFmt("No such command '%s', try 'help' for a list.",
						commandName.c_str());
		output.appendLine(msg);
	} else if (!command->permissionAllowed(level)) {
		const std::string& msg = strFmt("Not allowed to execute command '%s'.",
						commandName.c_str());
		output.appendLine(msg);
	} else {
		// execute the command at last
		command->execute(args, level, output);
	}
}

void CommandMgr::showHelp(Command::PermissionLevel level, CommandOutput& output) const
{
	// header
	const std::string& msg = strFmt("Available commands (%zu total)", _commandList.size());
	output.appendLine(msg);
	// short description of the existing commands (for the given level) in
	// the middle
	for (auto it = _commandList.begin(); it != _commandList.end(); ++it) {
		if ((*it).second->permissionAllowed(level)) {
			showHelp((*it).first, level, output);
		}
	}
	// footer
	output.appendLine("Use 'help <command>' for more info.");
}

void CommandMgr::showHelp(const std::string& commandName,
			  Command::PermissionLevel level,
			  CommandOutput& output) const
{
	// get the command and check if exists
	const Command* command = findCommand(commandName);
	if (!command || !command->permissionAllowed(level)) {
		const std::string& msg = strFmt("No help on '%s'. Try 'help' for a list.",
						commandName.c_str());
		output.appendLine(msg);
		return;
	}

	// add to output name and arguments
	std::string line = "  ";
	line += command->getName();
	const std::vector<std::string>& argNames = command->getArgNames();
	for (size_t i = 0; i < argNames.size(); ++i) {
		line += " <";
		line += argNames[i];
		line += ">";
	}

	// "tab" to column 40 in a line
	while (line.size() < 40) {
		line += " ";
	}

	// add short description
	line += command->getDescription();
	output.appendLine(line);
}


NAMESPACE_END(AmbarMetta);
