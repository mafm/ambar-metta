/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "msgbase.hxx"

#include "common/includes.hxx"

#include <cstring>
#include <cstdlib>


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * MsgType
 *******************************************************************************/
MsgType::MsgType(const char* codeName)
{
	setType(codeName);
}

MsgType::MsgType(uint32_t code)
{
	setType(code);
}

bool MsgType::operator == (const MsgType& other) const
{
	return (_typeID == other._typeID);
}

bool MsgType::operator != (const MsgType& other) const
{
	return ! (_typeID == other._typeID);
}

const char* MsgType::getName() const
{
	return _typeName.c_str();
}

uint32_t MsgType::getID() const
{
	return _typeID;
}

void MsgType::setType(const char* codeName)
{
	if (strlen(codeName) != 4) {
		logERROR("Bad code name '%s', message type won't be valid",
			 codeName);
	}
	_typeName = codeName;
	_typeID = ( (codeName[0] << 24) & 0xff000000 )
		| ( (codeName[1] << 16) & 0x00ff0000 )
		| ( (codeName[2] << 8) & 0x0000ff00 )
		| ( codeName[3] & 0x000000ff );
}

void MsgType::setType(uint32_t type)
{
	_typeName[0] = (type >> 24) & 0xff;
	_typeName[1] = (type >> 16) & 0xff;
	_typeName[2] = (type >> 8) & 0xff;
	_typeName[3] = (type) & 0xff;
	_typeID = type;
}


/********************************************************************************
 * MsgBase
 *******************************************************************************/
MsgBase::MsgBase() :
	_isSerialized(false), _isDeserialized(false)
{
}

void MsgBase::serialize()
{
	if (_isSerialized) {
		logERROR("Message (type '%s', size %u) already serialized, skipping",
		       getType().getName(), _buffer.getSize());
		return;
	}

	// reserve space for size and message type
	uint16_t size = 0;
	write(size);
	uint32_t type = getType().getID();
	write(type);

	// perform the message especific serialization
	serializeData();

	// actually write the size, now that we know it
	size = _buffer.getSize();
	char s0 = static_cast<char>((size >> 8) & 0xff);
	char s1 = static_cast<char>(size & 0xff);
	_buffer.overwritePosition(0, s0);
	_buffer.overwritePosition(1, s1);

	_isSerialized = true;
}

void MsgBase::deserialize(const char* buffer, size_t size)
{
	if (_isDeserialized) {
		logERROR("Message (type '%s', size %u) already deserialized, skipping",
		       getType().getName(), _buffer.getSize());
		return;
	}

	// set up the local buffer with the given data, ignoring header
	size_t headerSize = sizeof(uint16_t) + sizeof(uint32_t);
	CHECK_FATAL(size >= headerSize);
	_buffer.append(buffer+headerSize, size-headerSize);

	// perform the message especific deserialization
	deserializeData();

	// check that the deserialization was complete and so the buffer is
	// empty
	if (_buffer.getSize() != 0) {
		logERROR("Deserializing message (type '%s', size %zu), buffer not empty (%u bytes)",
		       getType().getName(), size, _buffer.getSize());
	}

	_isDeserialized = true;
}

bool MsgBase::bufferGEThan(size_t bytes)
{
	bool bigEnough = _buffer.getSize() >= bytes;
	if (!bigEnough) {
		logERROR("Deserializing msg, trying to read more data (%zu) than the available (%u)",
			bytes, _buffer.getSize());
	}
	return bigEnough;
}

void MsgBase::write(const char* data, size_t size)
{
	_buffer.append(data, size);
}

void MsgBase::write(const std::string& s)
{
	write(s.c_str() + '\0', s.size()+1);
}

void MsgBase::write(char c)
{
	_buffer.append(&c, 1);
}

void MsgBase::write(bool b)
{
	write(static_cast<char>(b));
}

void MsgBase::write(uint64_t i)
{
	char aux[8];
	aux[0] = static_cast<char>((i >> 56) & 0xff);
	aux[1] = static_cast<char>((i >> 48) & 0xff);
	aux[2] = static_cast<char>((i >> 40) & 0xff);
	aux[3] = static_cast<char>((i >> 32) & 0xff);
	aux[4] = static_cast<char>((i >> 24) & 0xff);
	aux[5] = static_cast<char>((i >> 16) & 0xff);
	aux[6] = static_cast<char>((i >> 8) & 0xff);
	aux[7] = static_cast<char>((i >> 0) & 0xff);
	write(aux, 8);
}

void MsgBase::write(uint32_t i)
{
	char aux[4];
	aux[0] = static_cast<char>((i >> 24) & 0xff);
	aux[1] = static_cast<char>((i >> 16) & 0xff);
	aux[2] = static_cast<char>((i >> 8) & 0xff);
	aux[3] = static_cast<char>((i >> 0) & 0xff);
	write(aux, 4);
}

void MsgBase::write(int32_t i)
{
	write(static_cast<uint32_t>(i));
}

void MsgBase::write(uint16_t i)
{
	char aux[2];
	aux[0] = static_cast<char>((i >> 8) & 0xff);
	aux[1] = static_cast<char>((i >> 0) & 0xff);
	write(aux, 2);
}

void MsgBase::write(int16_t i)
{
	write(static_cast<uint16_t>(i));
}

void MsgBase::write(uint8_t i)
{
	write(static_cast<char>(i));
}

void MsgBase::write(int8_t i)
{
	write(static_cast<uint8_t>(i));
}

void MsgBase::write(float f)
{
	write(*(uint32_t*)&f);
}

void MsgBase::write(const Vector3& v3)
{
	write(v3.x);
	write(v3.y);
	write(v3.z);
}

void MsgBase::read(char* data, size_t size)
{
	// check if there's enough data in the buffer
	if (!bufferGEThan(sizeof(char)*size)) {
		return;
	}
	_buffer.extractFront(data, size);
}

void MsgBase::read(std::string& s)
{
	// mafm: due to the interface available in vector (we cannot use
	// std::string for this), we have to iterate considering the front
	// position only, until we find the null character terminating the
	// string, or else we go out of bounds

	// mafm: needed this because we expect the string to be empty, and it
	// might be not empty using dummy initialization values (<none>, etc).
	// The result expected in the rest of the data types is to overwrite the
	// old value if exists, and replace it with the data that we read, so we
	// want to maintain this consistency :)
	s.clear();

	while (bufferGEThan(sizeof(char))) {
		char c = _buffer.popFront();
		if (c == '\0') {
			return;
		} else {
			s.append(1, c);
		}
	}
}

void MsgBase::read(char& c)
{
	// check if there's enough data in the buffer
	if (!bufferGEThan(sizeof(char))) {
		return;
	}
	c = _buffer.popFront();
}

void MsgBase::read(bool& b)
{
	char c;
	read(c);
	b = static_cast<bool>(c);
}

void MsgBase::read(uint64_t& i)
{
	char c0, c1, c2, c3, c4, c5, c6, c7;
	read(c0); read(c1); read(c2); read(c3); read(c4); read(c5); read(c6); read(c7);
	i = ( (static_cast<uint64_t>(c0) << 56) & 0xff00000000000000LL )
		| ( (static_cast<uint64_t>(c1) << 48) & 0x00ff000000000000LL )
		| ( (static_cast<uint64_t>(c2) << 40) & 0x0000ff0000000000LL )
		| ( (static_cast<uint64_t>(c3) << 32) & 0x000000ff00000000LL )
		| ( (static_cast<uint64_t>(c4) << 24) & 0x00000000ff000000LL )
		| ( (static_cast<uint64_t>(c5) << 16) & 0x0000000000ff0000LL )
		| ( (static_cast<uint64_t>(c6) << 8)  & 0x000000000000ff00LL )
		| ( static_cast<uint64_t>(c7) & 0x00000000000000ffLL );
}

void MsgBase::read(uint32_t& i)
{
	char c0, c1, c2, c3;
	read(c0); read(c1); read(c2); read(c3);
	i = ( (static_cast<uint32_t>(c0) << 24) & 0xff000000 )
		| ( (static_cast<uint32_t>(c1) << 16) & 0x00ff0000 )
		| ( (static_cast<uint32_t>(c2) << 8) & 0x0000ff00 )
		| ( static_cast<uint32_t>(c3) & 0x000000ff );
}

void MsgBase::read(int32_t& i)
{
	uint32_t aux;
	read(aux);
	i = static_cast<int32_t>(aux);
}

void MsgBase::read(uint16_t& i)
{
	char c0, c1;
	read(c0); read(c1);
	i = ( (static_cast<uint16_t>(c0) << 8) & 0xff00 )
		| ( static_cast<uint16_t>(c1) & 0x00ff );
}

void MsgBase::read(int16_t& i)
{
	uint16_t aux;
	read(aux);
	i = static_cast<int16_t>(aux);
}

void MsgBase::read(uint8_t& i)
{
	char c;
	read(c);
	i = static_cast<uint8_t>(c);
}

void MsgBase::read(int8_t& i)
{
	char c;
	read(c);
	i = static_cast<int8_t>(c);
}

void MsgBase::read(float& f)
{
	uint32_t aux;
	read(aux);
	f = *(float*)&aux;
}

void MsgBase::read(Vector3& v3)
{
	read(v3.x);
	read(v3.y);
	read(v3.z);
}

uint32_t MsgBase::getLength() const
{
	return _buffer.getSize();
}

const char* MsgBase::getBuffer() const
{
	return _buffer.getBuffer();
}


/********************************************************************************
 * MsgHdlFactory
 *******************************************************************************/
MsgHdlFactory::~MsgHdlFactory()
{
	// delete message objects passed initially to the factory
	while (!_factories.empty()) {
		delete _factories.begin()->second.first;
		delete _factories.begin()->second.second;
		_factories.erase(_factories.begin());
	}
}

void MsgHdlFactory::registerMsgWithHdl(MsgBase* msg, MsgHdlBase* hdl)
{
	uint32_t key = msg->getType().getID();
	auto it = _factories.find(key);
	if (it != _factories.end()) {
		logWARNING("Msg already registered (type: '%s')",
			   msg->getType().getName());
	} else if (msg->getType() != hdl->getMsgType()) {
		logERROR("Msg types mismatch (msg: '%s', hdl: '%s')",
			 msg->getType().getName(),
			 hdl->getMsgType().getName());
	} else {
		// register the message and handler
		_factories[key].first = msg;
		_factories[key].second = hdl;
	}
}

bool MsgHdlFactory::handleStream(Netlink& netlink,
				 uint32_t key,
				 const char* buffer,
				 uint32_t size)
{
	// mafm: see comment in the header about creating instances

	// see if we have the needed msg+hdl
	MsgBase* msg = createMsgInstance(key);
	MsgHdlBase* hdl = getHdl(key);
	if (!msg || !hdl) {
		return false;
	}

	// deserialize and use the message, delete it afterwards
	msg->deserialize(buffer, size);
	hdl->handleMsg(*msg, &netlink);
	delete msg;

	return true;
}

MsgBase* MsgHdlFactory::createMsgInstance(uint32_t key) const
{
	auto it = _factories.find(key);
	if (it == _factories.end()) {
		logERROR("Message not found (type: '%s')",
			 MsgType(key).getName());
		return 0;
	} else {
		return it->second.first->createInstance();
	}
}

MsgHdlBase* MsgHdlFactory::getHdl(uint32_t key) const
{
	auto it = _factories.find(key);
	if (it == _factories.end()) {
		logERROR("Handler not found for message (type: '%s')",
			 MsgType(key).getName());
		return 0;
	} else {
		return it->second.second;
	}
}


NAMESPACE_END(AmbarMetta);
