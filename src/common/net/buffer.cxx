/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "buffer.hxx"

#include "common/includes.hxx"

#include <cstring>
#include <cstdlib>


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * Buffer
 *******************************************************************************/
const uint32_t Buffer::SIZE_LIMIT(32*1024*1024); // limit size to 32 MBs

Buffer::Buffer() :
	_size(0), _data(0)
{
	// mafm: This implementation is not very efficient, but at the time of
	// writing this, files are transferred at up to 2GB (with sleep
	// functions in client and server loops, to avoid using 100% of CPU).
	// This is of course where the client needs more power, and it's very
	// unlikely that neither clients or servers will use those speeds with
	// an "official" game server (unless we have 100K users in the same
	// server or things like that).
}

Buffer::~Buffer()
{
	delete [] _data;
}

void Buffer::append(const char* data, size_t s)
{
	// apply size limit
	if ((_size+s) > SIZE_LIMIT) {
		logERROR("Buffer size limit of %u reached (%u after append)",
			 SIZE_LIMIT, _size + s);
		return;
	}

	if (_data == 0) {
		// empty buffer, append the new one
		_size = s;
		_data = new char[s];
		memcpy(_data, data, s);
	} else {
		// existing buffer, append using a bigger buffer
		char* final = new char[_size+s];
		memcpy(final, _data, _size);
		memcpy(final+_size, data, s);
		_size += s;
		// delete old buffer
		char* aux = _data;
		delete [] aux;
		_data = final;
	}
}

char Buffer::popFront()
{
	char c;
	extractFront(&c, 1);
	return c;
}

void Buffer::extractFront(char* buffer, size_t s)
{
	if (s > _size) {
		logERROR("Requesting %zu but only %u bytes available in the buffer",
		       s, _size);
		buffer = 0;
		return;
	} else {
		// copy data to the given buffer
		memcpy(buffer, _data, s);

		// extract from existing buffer
		if (s == _size) {
			// all of it
			delete [] _data;
			_data = 0;
			_size = 0;
		} else {
			// part of it, only
			char* final = new char[_size-s];
			memcpy(final, _data+s, _size-s);
			_size -= s;
			// delete old buffer
			char* aux = _data;
			delete [] aux;
			_data = final;
		}
	}
}

void Buffer::overwritePosition(size_t index, char c)
{
	if (index > _size) {
		logERROR("Trying to overwrite non-existent position (%zu, size %u), skipping",
		       index, _size);
	} else {
		_data[index] = c;
	}
}

const char* Buffer::getBuffer() const
{
	return _data;
}

uint32_t Buffer::getSize() const
{
	return _size;
}


NAMESPACE_END(AmbarMetta);
