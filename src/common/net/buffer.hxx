/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_NET_BUFFER_H__
#define __AMBARMETTA_COMMON_NET_BUFFER_H__


/** \file buffer.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * Simple data buffer for network communication.
 */


#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/** Simple data buffer for network communication.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class Buffer {
public:
	/** Default constructor */
	Buffer();
	/** Destructor */
	~Buffer();

	/** Append to the buffer */
	void append(const char* data, size_t size);

	/** Pop the front element */
	char popFront();

	/** Extract given size from the front */
	void extractFront(char* buffer, size_t size);

	/** Overwrite given position (to set message size, in example) */
	void overwritePosition(size_t index, char c);

	/** Get the buffer itself */
	const char* getBuffer() const;

	/** Get current size of the buffer */
	uint32_t getSize() const;

private:
	/// Buffer size limit
	static const uint32_t SIZE_LIMIT;

	/// Buffer size
	uint32_t _size;
	/// Pointer to the data
	char* _data;
};


NAMESPACE_END(AmbarMetta);
#endif
