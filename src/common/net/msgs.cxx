/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "msgs.hxx"

#include "common/includes.hxx"


NAMESPACE_START(AmbarMetta);


/********************************************************************************
 * MsgUtils
 *******************************************************************************/
const char* MsgUtils::Errors::getDescription(int code)
{
	switch (code) {
	case SUCCESS:
		return "Success";
	case EBADLOGIN:
		return "Wrong login/password";
	case EALREADYLOGGED:
		return "Already logged";
	case EDATABASE:
		return "Database error (Internal server error)";
	case ECHARCORRUPT:
		return "Characters corrupted (Internal server error)";
	case EUSERALREADYEXIST:
		return "Username already exists";
	case EMAXCHARS:
		return "Max number of characters per account reached";
	case ENEWCHARBADDATA:
		return "Bad data for new character (race, gender, class, points...)";
	case ECREATEFAILED:
		return "Couldn't create player in the world (Internal server error)";
	case EALREADYPLAYING:
		return "Already playing";
	default:
		return "Unknown error code";
	}
}


/********************************************************************************
 * TestDataTypes
 *******************************************************************************/
MsgType MsgTestDataTypes::_type("Test");

MsgBase* MsgTestDataTypes::createInstance() const
{
	return new MsgTestDataTypes;
}

void MsgTestDataTypes::serializeData()
{
	str1 = "String 1"; write(str1);
	uint64_1 = 0xffffffffffffffffLL; write(uint64_1);
	uint64_2 = 0; write(uint64_2);
	uint64_3 = +1234567890123456789LL; write(uint64_3);
	uint32_1 = 0xffffffff; write(uint32_1);
	uint32_2 = 0; write(uint32_2);
	uint32_3 = +1234567890; write(uint32_3);
	uint16_1 = 0xffff; write(uint16_1);
	uint16_2 = 0; write(uint16_2);
	uint16_3 = +12345; write(uint16_3);
	uint8_1 = 0xff; write(uint8_1);
	uint8_2 = 0; write(uint8_2);
	uint8_3 = +123; write(uint8_3);
	str2 = "String 2"; write(str2);
	int32_1 = -123456789; write(int32_1);
	int32_2 = 0; write(int32_2);
	int32_3 = +123456789; write(int32_3);
	int16_1 = -12345; write(int16_1);
	int16_2 = 0; write(int16_2);
	int16_3 = +12345; write(int16_3);
	int8_1 = -123; write(int8_1);
	int8_2 = 0; write(int8_2);
	int8_3 = +123; write(int8_3);
	str3 = "String 3"; write(str3);
	c = 'c'; write(c);
	b = true; write(b);
	f1 = -123456.78f; write(f1);
	f2 = 0.0f; write(f2);
	f3 = +123456.78f; write(f3);
	str4 = "String 4"; write(str4);
}

void MsgTestDataTypes::deserializeData()
{
	read(str1); logDEBUG("String 1: '%s'", str1.c_str());
	read(uint64_1); logDEBUG("0xffffffffffffffff: '%llx'", uint64_1);
	read(uint64_2); logDEBUG("0: '%llu'", uint64_2);
	read(uint64_3); logDEBUG("+1234567890123456789: '%llu'", uint64_3);
	read(uint32_1); logDEBUG("0xffffffff: '%x'", uint32_1);
	read(uint32_2); logDEBUG("0: '%u'", uint32_2);
	read(uint32_3); logDEBUG("+1234567890: '%u'", uint32_3);
	read(uint16_1); logDEBUG("0xffff: '%x'", uint16_1);
	read(uint16_2); logDEBUG("0: '%u'", uint16_2);
	read(uint16_3); logDEBUG("+12345: '%u'", uint16_3);
	read(uint8_1); logDEBUG("0xff: '%x'", uint8_1);
	read(uint8_2); logDEBUG("0: '%u'", uint8_2);
	read(uint8_3); logDEBUG("+123: '%u'", uint8_3);
	read(str2); logDEBUG("String 2: '%s'", str2.c_str());
	read(int32_1); logDEBUG("-123456789: '%d'", int32_1);
	read(int32_2); logDEBUG("0: '%d'", int32_2);
	read(int32_3); logDEBUG("+123456789: '%d'", int32_3);
	read(int16_1); logDEBUG("-12345: '%d'", int16_1);
	read(int16_2); logDEBUG("0: '%d'", int16_2);
	read(int16_3); logDEBUG("+12345: '%d'", int16_3);
	read(int8_1); logDEBUG("-123: '%d'", int8_1);
	read(int8_2); logDEBUG("0: '%d'", int8_2);
	read(int8_3); logDEBUG("+123: '%d'", int8_3);
	read(str3); logDEBUG("String 3: '%s'", str3.c_str());
	read(c); logDEBUG("c: '%c'", c);
	read(b); logDEBUG("1 (bool): '%d'", b);
	read(f1); logDEBUG("-123456.78f: '%8.02f'", f1);
	read(f2); logDEBUG("0: '%g'", f2);
	read(f3); logDEBUG("+123456.78f: '%8.02f'", f3);
	read(str4); logDEBUG("String 4: '%s'", str4.c_str());
}


/********************************************************************************
 * Connection
 *******************************************************************************/
MsgType MsgConnect::_type("Conn");

MsgBase* MsgConnect::createInstance() const
{
	return new MsgConnect;
}

void MsgConnect::serializeData()
{
}

void MsgConnect::deserializeData()
{
}


MsgType MsgConnectReply::_type("ConR");

MsgBase* MsgConnectReply::createInstance() const
{
	return new MsgConnectReply;
}

void MsgConnectReply::serializeData()
{
	write(resultCode);
	write(protocolVersion);
	write(uptime);
	write(totalUsers);
	write(totalChars);
	write(currentPlayers);
}

void MsgConnectReply::deserializeData()
{
	read(resultCode);
	read(protocolVersion);
	read(uptime);
	read(totalUsers);
	read(totalChars);
	read(currentPlayers);
}


MsgType MsgLogin::_type("Logi");

MsgBase* MsgLogin::createInstance() const
{
	return new MsgLogin;
}

void MsgLogin::serializeData()
{
	write(username);
	write(pw_md5sum);
}

void MsgLogin::deserializeData()
{
	read(username);
	read(pw_md5sum);
}


MsgType MsgLoginReply::_type("LogR");

MsgBase* MsgLoginReply::createInstance() const
{
	return new MsgLoginReply;
}

void MsgLoginReply::serializeData()
{
	charNumber = charList.size();
	write(resultCode);
	write(charNumber);
	for (size_t i = 0; i < charNumber; ++i) {
		write(charList[i].name);
		write(charList[i].race);
		write(charList[i].gender);
		write(charList[i].playerClass);
		write(charList[i].area);
	}
}

void MsgLoginReply::deserializeData()
{
	read(resultCode);
	read(charNumber);
	for (size_t i = 0; i < charNumber; ++i) {
		std::string name = "<none>";
		std::string race = "<none>";
		std::string gender = "<none>";
		std::string playerClass = "<none>";
		std::string area = "<none>";
		read(name);
		read(race);
		read(gender);
		read(playerClass);
		read(area);
		addCharacter(name, race, gender, playerClass, area);
	}
}

void MsgLoginReply::addCharacter(std::string& name, std::string& race,
				 std::string& gender, std::string& playerClass, std::string& area)
{
	CharacterListEntry character;
	character.name = name;
	character.race = race;
	character.gender = gender;
	character.playerClass = playerClass;
	character.area = area;
	charList.push_back(character);
}


MsgType MsgNewUser::_type("NUsr");

MsgBase* MsgNewUser::createInstance() const
{
	return new MsgNewUser;
}

void MsgNewUser::serializeData()
{
	write(username);
	write(pw_md5sum);
	write(email);
	write(realname);
}

void MsgNewUser::deserializeData()
{
	read(username);
	read(pw_md5sum);
	read(email);
	read(realname);
}


MsgType MsgNewUserReply::_type("NUsR");

MsgBase* MsgNewUserReply::createInstance() const
{
	return new MsgNewUserReply;
}

void MsgNewUserReply::serializeData()
{
	write(resultCode);
}

void MsgNewUserReply::deserializeData()
{
	read(resultCode);
}


MsgType MsgNewChar::_type("NCha");

MsgBase* MsgNewChar::createInstance() const
{
	return new MsgNewChar;
}

void MsgNewChar::serializeData()
{
	write(charname);
	write(race);
	write(gender);
	write(playerClass);
	write(ab_choice_con);
	write(ab_choice_str);
	write(ab_choice_dex);
	write(ab_choice_int);
	write(ab_choice_wis);
	write(ab_choice_cha);
}

void MsgNewChar::deserializeData()
{
	read(charname);
	read(race);
	read(gender);
	read(playerClass);
	read(ab_choice_con);
	read(ab_choice_str);
	read(ab_choice_dex);
	read(ab_choice_int);
	read(ab_choice_wis);
	read(ab_choice_cha);
}


MsgType MsgNewCharReply::_type("NChR");

MsgBase* MsgNewCharReply::createInstance() const
{
	return new MsgNewCharReply;
}

void MsgNewCharReply::serializeData()
{
	write(resultCode);
	write(character.name);
	write(character.race);
	write(character.gender);
	write(character.playerClass);
	write(character.area);
}

void MsgNewCharReply::deserializeData()
{
	read(resultCode);
	read(character.name);
	read(character.race);
	read(character.gender);
	read(character.playerClass);
	read(character.area);
}


MsgType MsgDelChar::_type("DCha");

MsgBase* MsgDelChar::createInstance() const
{
	return new MsgDelChar;
}

void MsgDelChar::serializeData()
{
	write(charname);
}

void MsgDelChar::deserializeData()
{
	read(charname);
}


MsgType MsgDelCharReply::_type("DChR");

MsgBase* MsgDelCharReply::createInstance() const
{
	return new MsgDelCharReply;
}

void MsgDelCharReply::serializeData()
{
	write(resultCode);
	write(charname);
}

void MsgDelCharReply::deserializeData()
{
	read(resultCode);
	read(charname);
}


MsgType MsgJoin::_type("Join");

MsgBase* MsgJoin::createInstance() const
{
	return new MsgJoin;
}

void MsgJoin::serializeData()
{
	write(charname);
}

void MsgJoin::deserializeData()
{
	read(charname);
}


MsgType MsgJoinReply::_type("JoiR");

MsgBase* MsgJoinReply::createInstance() const
{
	return new MsgJoinReply;
}

void MsgJoinReply::serializeData()
{
	write(resultCode);
}

void MsgJoinReply::deserializeData()
{
	read(resultCode);
}


/********************************************************************************
 * Console
 *******************************************************************************/
MsgType MsgChat::_type("Chat");

MsgBase* MsgChat::createInstance() const
{
	return new MsgChat;
}

void MsgChat::serializeData()
{
	write(origin);
	write(target);
	write(text);
	write(type);
}

void MsgChat::deserializeData()
{
	read(origin);
	read(target);
	read(text);
	read(type);
}


MsgType MsgCommand::_type("Cmmd");

MsgBase* MsgCommand::createInstance() const
{
	return new MsgCommand;
}

void MsgCommand::serializeData()
{
	write(command);
}

void MsgCommand::deserializeData()
{
	read(command);
}


/********************************************************************************
 * Contacts
 *******************************************************************************/
MsgType MsgContactStatus::_type("CtSt");

MsgBase* MsgContactStatus::createInstance() const
{
	return new MsgContactStatus;
}

void MsgContactStatus::serializeData()
{
	write(charname);
	write(type);
	write(status);
	write(lastLogin);
	write(comment);
}

void MsgContactStatus::deserializeData()
{
	read(charname);
	read(type);
	read(status);
	read(lastLogin);
	read(comment);
}


MsgType MsgContactAdd::_type("CtAd");

MsgBase* MsgContactAdd::createInstance() const
{
	return new MsgContactAdd;
}

void MsgContactAdd::serializeData()
{
	write(charname);
	write(type);
	write(comment);
}

void MsgContactAdd::deserializeData()
{
	read(charname);
	read(type);
	read(comment);
}


MsgType MsgContactDel::_type("CtDl");

MsgBase* MsgContactDel::createInstance() const
{
	return new MsgContactDel;
}

void MsgContactDel::serializeData()
{
	write(charname);
}

void MsgContactDel::deserializeData()
{
	read(charname);
}


/********************************************************************************
 * Entities
 *******************************************************************************/
MsgType MsgEntityCreate::_type("EnCr");

MsgBase* MsgEntityCreate::createInstance() const
{
	return new MsgEntityCreate;
}

void MsgEntityCreate::serializeData()
{
	write(entityID);
	write(entityName);
	write(entityClass);
	write(meshType);
	write(meshSubtype);
	write(area);
	write(position);
	write(rot);
}

void MsgEntityCreate::deserializeData()
{
	read(entityID);
	read(entityName);
	read(entityClass);
	read(meshType);
	read(meshSubtype);
	read(area);
	read(position);
	read(rot);
}


MsgType MsgEntityMove::_type("EnMv");

MsgBase* MsgEntityMove::createInstance() const
{
	return new MsgEntityMove;
}

void MsgEntityMove::serializeData()
{
	write(entityID);
	write(area);
	write(position);
	write(direction);
	write(directionSpeed);
	write(rot);
	write(rotSpeed);
	write(mov_fwd);
	write(mov_bwd);
	write(run);
	write(rot_left);
	write(rot_right);
}

void MsgEntityMove::deserializeData()
{
	read(entityID);
	read(area);
	read(position);
	read(direction);
	read(directionSpeed);
	read(rot);
	read(rotSpeed);
	read(mov_fwd);
	read(mov_bwd);
	read(run);
	read(rot_left);
	read(rot_right);
}


MsgType MsgEntityDestroy::_type("EnDt");

MsgBase* MsgEntityDestroy::createInstance() const
{
	return new MsgEntityDestroy;
}

void MsgEntityDestroy::serializeData()
{
	write(entityID);
}

void MsgEntityDestroy::deserializeData()
{
	read(entityID);
}


/********************************************************************************
 * Inventory
 *******************************************************************************/
MsgType MsgInventoryListing::_type("IvLt");

MsgBase* MsgInventoryListing::createInstance() const
{
	return new MsgInventoryListing;
}

void MsgInventoryListing::serializeData()
{
	std::string type,subtype;
	listSize = invListing.size();
	write(listSize);
	for (size_t i = 0; i < listSize; ++i) {
		InventoryItem item = invListing[i];
		type = item.getType();
		subtype = item.getSubtype();
		write(item.getItemID());
		write(type);
		write(subtype);
		write(item.getLoad());
	}
}

void MsgInventoryListing::deserializeData()
{
	read(listSize);
	for (size_t i = 0; i < listSize; ++i) {
		EntityID::value_type itemID;
		std::string meshType, meshSubtype;
		float load;
		read(itemID);
		read(meshType);
		read(meshSubtype);
		read(load);
		invListing.push_back(InventoryItem(itemID, meshType, meshSubtype, load));
	}
}


void MsgInventoryListing::addItem(const InventoryItem* item)
{
	invListing.push_back(*item);
}


MsgType MsgInventoryGet::_type("IvGt");

MsgBase* MsgInventoryGet::createInstance() const
{
	return new MsgInventoryGet;
}

void MsgInventoryGet::serializeData()
{
	write(itemID);
}

void MsgInventoryGet::deserializeData()
{
	read(itemID);
}


MsgType MsgInventoryAdd::_type("IvAd");

MsgBase* MsgInventoryAdd::createInstance() const
{
	return new MsgInventoryAdd;
}

void MsgInventoryAdd::serializeData()
{
	std::string type(item.getType());
	std::string subtype(item.getSubtype());
	write(item.getItemID());
	write(type);
	write(subtype);
	write(item.getLoad());
}

void MsgInventoryAdd::deserializeData()
{
	EntityID::value_type itemID;
	std::string meshType, meshSubtype;
	float load;
	read(itemID);
	read(meshType);
	read(meshSubtype);
	read(load);
	item.setItemID(itemID);
	item.setType(meshType);
	item.setSubtype(meshSubtype);
	item.setLoad(load);
}


MsgType MsgInventoryDrop::_type("IvDp");

MsgBase* MsgInventoryDrop::createInstance() const
{
	return new MsgInventoryDrop;
}

void MsgInventoryDrop::serializeData()
{
	write(itemID);
}

void MsgInventoryDrop::deserializeData()
{
	read(itemID);
}


MsgType MsgInventoryDel::_type("IvDl");

MsgBase* MsgInventoryDel::createInstance() const
{
	return new MsgInventoryDel;
}

void MsgInventoryDel::serializeData()
{
	write(itemID);
}

void MsgInventoryDel::deserializeData()
{
	read(itemID);
}


/********************************************************************************
 * Player data
 *******************************************************************************/
MsgType MsgPlayerData::_type("PlDa");

MsgBase* MsgPlayerData::createInstance() const
{
	return new MsgPlayerData;
}

void MsgPlayerData::serializeData()
{
	write(health_max);
	write(health_cur);
	write(magic_max);
	write(magic_cur);
	write(load_max);
	write(load_cur);
	write(stamina);
	write(gold);
	write(level);
	write(ab_con);
	//write(abe_con);
	write(ab_str);
	//write(abe_str);
	write(ab_dex);
	//write(abe_agi);
	write(ab_int);
	//write(abe_int);
	write(ab_wis);
	//write(abe_wis);
	write(ab_cha);
	//write(abe_cha);
}

void MsgPlayerData::deserializeData()
{
	read(health_max);
	read(health_cur);
	read(magic_max);
	read(magic_cur);
	read(load_max);
	read(load_cur);
	read(stamina);
	read(gold);
	read(level);
	read(ab_con);
	//read(abe_con);
	read(ab_str);
	//read(abe_str);
	read(ab_dex);
	//read(abe_agi);
	read(ab_int);
	//read(abe_int);
	read(ab_wis);
	//read(abe_wis);
	read(ab_cha);
	//read(abe_cha);
}


/********************************************************************************
 * Time
 *******************************************************************************/
MsgType MsgTimeMinute::_type("TmMn");

MsgBase* MsgTimeMinute::createInstance() const
{
	return new MsgTimeMinute;
}

void MsgTimeMinute::serializeData()
{
	write(gametime);
}

void MsgTimeMinute::deserializeData()
{
	read(gametime);
}


NAMESPACE_END(AmbarMetta);
