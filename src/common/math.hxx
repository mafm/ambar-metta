/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_COMMON_MATH_H__
#define __AMBARMETTA_COMMON_MATH_H__


/** \file math.hxx
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 *
 * Common mathematical functions, constants, etc.
 */


#include "config.hxx"


NAMESPACE_START(AmbarMetta);


/// Pi number
#define PI_NUMBER 3.141592f


/** Ensure that the first parameter is less than or equal to the second,
 * otherwise set it as equal.
 *
 * @param target The parameter that we want to check if it's within the limits
 *
 * @param limit The limit
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
template <class T>
inline
void ensureMaximum(T& target, const T& limit)
{
        if (target > limit) {
                target = limit;
        }
}

/** Ensure that the first parameter is greater than or equal to the second,
 * otherwise set it as equal.
 *
 * @param target The parameter that we want to check if it's within the limits
 *
 * @param limit The limit
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
template <class T>
inline
void ensureMinimum(T& target, const T& limit)
{
        if (target < limit) {
                target = limit;
        }
}


/** Power 2, simple shortcut
 *
 * @param number number to raise to the power of 2
 *
 * @return Result of the operation
 *
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
template <typename T>
inline
T power2(T number)
{
        return (number * number);
}


NAMESPACE_END(AmbarMetta);
#endif
