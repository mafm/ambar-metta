/*
 * This file is part of the Ambar-metta project.
 *
 * Copyright (C) 2009-2010 by Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AMBARMETTA_CONFIG_H__
#define __AMBARMETTA_CONFIG_H__


/** \file config
 *
 * This file contains includes and other data that the whole project wants to
 * include, so here's a place to put all of this.
 */


/********************************************************************************
 * Preprocessor Macros
 *******************************************************************************/
/// Macros for namespaces
#define NAMESPACE_START(arg) namespace arg {
/// Macros for namespaces
#define NAMESPACE_END(arg) }

/// Version string
#define VERSION "0.1.0"

/// License string
#define LICENSE \
"This program is free/open-source software, under GNU General Public License\n" \
"version 3 or later (GPLv3+).  Please see COPYING file or\n"	\
"<http://gnu.org/licenses/gpl.html> for more information.\n"

/// Combined Version+License string
#define BANNER_STRING \
	strFmt("Ambar-metta RPG, version %s\n\n%s", VERSION, LICENSE).c_str()

/// Check that will stay there in the final code, and logs as logFATAL and exits
/// the program with failure (similar to assert, but permanent).
///
/// \note This is meant to be used with caution, only in cases where the
/// condition should not happen (rare events of which we can't recover from,
/// etc).
#define CHECK_FATAL(x)							\
	if (!(x)) {							\
		logFATAL("Assertion failed: %s\n", #x);			\
		exit(-1);						\
	}


/********************************************************************************
 * Header includes
 *******************************************************************************/
/// Including <string> so we can use it everywhere as basic type
#include <string>


/********************************************************************************
 * Compiler tricks
 *******************************************************************************/
/// If we're not using GNU C, elide __attribute__
#ifndef __GNUC__
#  define  __attribute__(x)  /*NOTHING*/
#endif


#endif
